<?php

/////// BEGINN - Modell und Menü Eigenschaften aktualisieren ///////

include '../inc/db.php';

// POST Daten bekommen
$post = file_get_contents('php://input') ?? $_POST;

// JSON Daten Dekodieren
$post_obj = json_decode($post, true);

// Menü Daten aus dem Objekt bekommen
$menu = $post_obj['menu'];
$menu_id = $menu['id'];
$menu_slug = $menu['slug'];
$menu_name = $menu['name'];

// Modell für die Datenbank vorbereiten
$model = json_encode($menu, JSON_UNESCAPED_UNICODE);

// Benutzer, die auf das Menü zugreifen können
$users = $post_obj['users'];

// Menü-Daten in der Datenbank aktualisieren
$query = "UPDATE menus SET slug = '$menu_slug', name = '$menu_name', model = '$model' WHERE id = '$menu_id'";
mysqli_query($conn, $query);

// Alle bisherigen Einträge entfernen, um keine doppelten Einträge zu machen
$delete_users = "DELETE FROM admins_menus WHERE menu_id = '$menu_id'";
mysqli_query($conn, $delete_users);

// Schleife durchführen, um alle Einträge in die Datenbank zu speichern
foreach ($users as $user_id) {
  mysqli_query($conn, "INSERT INTO admins_menus SET admin_id = ".intval($user_id).", menu_id = ".intval($menu_id));
}

// Falls alles okay ist

$result = [
  'status' => 'ok'
];

// Zum Testen
echo json_encode($result);

/////// BEGINN - Modell und Menü Eigenschaften aktualisieren ///////

?>
