<?php

// URLs prüfen

// POST Daten bekommen
$post = file_get_contents('php://input') ?? $_POST;

// JSON Daten Dekodieren
$post_obj = json_decode($post, true);

$url = $post_obj['url'];

// Falls die URL angegeben worden ist
if(isset($url)){

  // Headers bekommen
  $result = get_headers($url);

  // Falls es eine Antwort gibt
  if($result){

    echo true;
    exit;
  }

}

echo false;

?>
