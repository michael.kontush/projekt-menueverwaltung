<?php

/////// BEGINN - Menü für Benutzer freigeben und Passwort ändern ///////

include '../inc/db.php';

// POST Daten bekommen
$post = file_get_contents('php://input') ?? $_POST;

// JSON Daten Dekodieren
$post_obj = json_decode($post, true);

// Daten über den Admin aus dem Objekt bekommen
$admin_id = $post_obj['id'];
$password_1 = md5($post_obj['pass']);
$password_2 = md5($post_obj['confpass']);
$admin_level = $post_obj['superadmin'];

// Die IDs der Menüs bekommen
$menus = $post_obj['menus'];

// Alle bisherigen Einträge entfernen, um keine doppelten Einträge zu machen
$delete_menus = "DELETE FROM admins_menus WHERE admin_id = '$admin_id'";
mysqli_query($conn, $delete_menus);

// Schleife durchführen, um alle Einträge in die Datenbank zu speichern
foreach ($menus as $menu_id) {
  mysqli_query($conn, "INSERT INTO admins_menus SET admin_id = ".intval($admin_id).", menu_id = ".intval($menu_id));
}

// Falls die Passwörter nicht übereinstimmen

if ($password_1 != $password_2) {

  $different_passwords = [
    'status' => 'error',
    'title' => "Ein Fehler ist aufgetreten.",
    'message' => "Die Passwörter stimmen nicht über ein. Die Änderungen wurden nicht gespeichert.",
  ];

  echo json_encode($different_passwords);

  die;

}

// Falls wir das Passwort nicht ändern wollen

if (strlen($post_obj['pass']) == 0 && strlen($post_obj['confpass']) == 0) {

  // Die Passwörter in der Datenbank aktualisieren
  mysqli_query($conn,"UPDATE admins SET admin_level = '$admin_level' WHERE id = '$admin_id'");

  $okay = [
    'status' => 'ok',
    'title' => "Alles Okay",
    'message' => "Die Benutzerdaten wurden aktualisiert.",
  ];

  echo json_encode($okay);

}elseif (strlen($post_obj['pass']) >= 6 && strlen($post_obj['pass']) <= 20) {

    // Die Passwörter in der Datenbank aktualisieren
    mysqli_query($conn,"UPDATE admins SET password = '$password_1', admin_level = '$admin_level' WHERE id = '$admin_id'");

    $okay = [
      'status' => 'ok',
      'title' => "Alles Okay",
      'message' => "Die Benutzerdaten wurden aktualisiert.",
    ];

    echo json_encode($okay);

}elseif (strlen($post_obj['pass']) < 6 || strlen($post_obj['pass']) > 20) {

  $password_lenght = [
    'status' => 'error',
    'title' => "Das Passwort ist zu lang oder zu kurz.",
    'message' => "Bitte geben sie ein Passwort zwischen 6 und 20 Zeichen an.",
  ];

  echo json_encode($password_lenght);

  die;

}

/////// ENDE - Menü für Benutzer freigeben und Passwort ändern ///////

?>
