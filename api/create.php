<?php

/////// START - Erstellung eines neues Menüs ///////

session_start();

include '../inc/db.php';

// POST Daten bekommen
$post = file_get_contents('php://input') ?? $_POST;

// JSON Daten Dekodieren
$post_obj = json_decode($post, true);

// Variablen zuweisen
$menu = $post_obj['menu'];
$menu_slug = $menu['slug'];
$menu_name = $menu['name'];

// Datum der Erstellung des Menüs
$creation_date = date("Y-m-d H:i:s");

// Modell für die Datenbank vorbereiten
$model = json_encode($menu, JSON_UNESCAPED_UNICODE);

// Benutzer, die auf das Menü zugreifen können
$users = $post_obj['users'];

// Menü + Modell in die Datenbank eintragen
$query = "INSERT INTO menus (slug, name, model, creation_date)
      VALUES('$menu_slug', '$menu_name', '$model', '$creation_date')";

mysqli_query($conn, $query);

// Wenn alles Okay ist
$result = [
  'status' => 'ok'
];

// Zum Testen
echo json_encode($result);

// ID des letzten eingefügten Menüs abfragen
$last_menu = "SELECT id FROM menus ORDER BY id DESC LIMIT 1";
$result_last_menu = mysqli_query($conn,$last_menu);
$row_last_menu = mysqli_fetch_array($result_last_menu);
$menu_id = $row_last_menu["id"];

// ID des Admins aus der Session bestimmen
$admin_id = $_SESSION["id"];

// Daten des eingeloggten Admins bekommen
$query2 = "SELECT id, admin_level FROM admins WHERE id = '$admin_id'";
$result2 = mysqli_query($conn,$query2);
$row2 = mysqli_fetch_array($result2);

// Dem Admin den Zugriff auf das zuvor von ihm erstellte Menü erlauben, falls er kein Superadmin ist
if (($row2['admin_level']) !== '0'){
  $query_menus = "INSERT INTO admins_menus (admin_id, menu_id)
  VALUES('$admin_id', '$menu_id')";
  mysqli_query($conn, $query_menus);
}

/////// ENDE - Erstellung eines neues Menüs ///////

?>
