<?php

/////// START - Erstellung eines neuen Admins ///////

session_start();

include '../inc/db.php';

// POST Daten bekommen
$post = file_get_contents('php://input') ?? $_POST;

// JSON Daten Dekodieren
$user = json_decode($post, true);

// Variablen zuweisen
$username = $user['username'];
$password_1 = $user['pass'];
$password_2 = $user['confpass'];
$admin_level = $user['superadmin'];
$menu_id = $user['menuId'];

// Datum der Erstellung des Admins
$creation_date = date("Y-m-d H:i:s");

// Leeres Errors Array erstellen
$errors = [];

// Validierung
if (empty($username)) { array_push($errors, "Der Benutzername muss eingegeben werden"); }
if (empty($password_1)) { array_push($errors, "Das Passwort muss eingegeben werden"); }
if ($password_1 != $password_2) {
array_push($errors, "Die Passwörter stimmen nicht überein");
die;
}

// Überprüfen, ob ein User mit dem selben Username existiert
$user_check_query = "SELECT * FROM admins WHERE username='$username' LIMIT 1";
$result_user = mysqli_query($conn, $user_check_query);
$user = mysqli_fetch_assoc($result_user);

if ($user) {
  if ($user['username'] === $username) {
    array_push($errors, "Dieser Benutzername existiert bereits.");

    $result = [
      'status' => 'error',
      'title' => 'Ein Fehler ist aufgetreten',
      'message' => 'Dieser Benutzername existiert bereits.',
    ];

    echo json_encode($result);

    die;
  }
}

// User in die DB hinzufügen wenn es keine Fehler gibt
if (count($errors) == 0) {
  $password = md5($password_1); // Passwort verschlüsseln

  $query = "INSERT INTO admins (username, password, admin_level, creation_date)
        VALUES('$username', '$password', '$admin_level', '$creation_date')";

  mysqli_query($conn, $query);

  // Nachricht für das Modale Fenster vorbereiten

  $result = [
    'status' => 'ok',
    'title' => 'Perfekt!',
    'message' => 'Der Benutzer wurde in die Datenbank hinzugefügt.',
  ];

  echo json_encode($result);

  // Die ID des letzten (den wir eben hinzugefügt haben) Admins abfragen

  $query2 = "SELECT id, admin_level FROM admins ORDER BY id DESC LIMIT 1";
  $result2 = mysqli_query($conn,$query2);
  $row2 = mysqli_fetch_array($result2);

  $admin_id = $row2['id'];

  // Falls der Admin-Level nicht Superadmin ist, Menü Zugriff dem Benutzer erlauben

  if (($row2['admin_level']) !== '0'){

    $query3 = "INSERT INTO admins_menus (admin_id, menu_id)
    VALUES('$admin_id', '$menu_id')";

    mysqli_query($conn, $query3);

  }

}

/////// ENDE - Erstellung eines neuen Admins ///////

?>
