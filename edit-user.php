<?php

session_start();

if(empty($_SESSION["id"])){
  header("location: login.php");
}

include 'inc/db.php';

// Alle Menüs aus der DB bekommen
$result_menu = mysqli_query($conn, "SELECT id, name, slug FROM menus");

if (isset($_GET['id'])){ // Falls es eine ID im URL gibt

  $id = intval($_GET['id']);

  // Alles Daten des ausgewählten Admins bekommen
  $result = mysqli_query($conn,"SELECT * FROM admins WHERE id = $id");
  $row = mysqli_fetch_array($result);
  $admin_level = $row['admin_level'];

  // ID und Name der Menüs bekommen
  $sql_all_menus = "SELECT id, name FROM menus ORDER BY id DESC;";
  $result_all_menus = mysqli_query($conn,$sql_all_menus);

  // ID, Slug und Namen der Menüs bekommen, die der ausgewählte Benutzer bearbeiten darf
  $sql_menus_allowed = "SELECT menus.id, slug, name FROM admins_menus, menus WHERE admin_id = '$id' AND menu_id = menus.id;";
  $result_menus_allowed = mysqli_query($conn,$sql_menus_allowed);

}

?>

<!DOCTYPE html>
<html lang="de">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href="/css/bootstrap.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/bootstrap-icons.css" rel="stylesheet">
  <title>Benutzer bearbeiten</title>
  <style>[v-cloak]{display:none!important;}</style>
  <?php include 'inc/favicons.php'; ?>
</head>
<body>

  <div v-cloak id="app">

    <?php $title="Benutzer bearbeiten"; ?>

    <?php include 'inc/header.php'; ?>

    <div class="content">
      <div class="container">

        <?php

        // Prüfen, ob der Admin ein Superadmin ist
        if($_SESSION["admin_level"] != '0'){

          echo '

          <div class="row py-2">
            <div class="col">
              <div class="p-3 text-center">Diese Seite steht nur Superadmins zur Verfügung.</div>
            </div>
          </div>';

          echo '</div>
          </div>'; // #content und #container

          include 'inc/footer.php';

          echo '</div>'; // #app

          include 'inc/scripts.php';

            die;

        }

         ?>

        <!--

        p-state: Zustand des Komponenten
          showResult: Toast anzeigen
            success - Alles okay
            error - Fehler ist aufgetreten
            choseMenu - sie müssen ein Menü auswählen
          response: Was wir im Toast anzeigen
            title: Überschrift im Toast
            message: Nachricht im Toast
          dropdownActive: Dropdown anzeigen / verstecken
          deleteConfirm: Modal-Fenster anzeigen, wo wir die Entfernung des Admins bestätigen
          superadmin: Admin Level (0 = Superadmin, 1 = Admin Level 1)
          pass: Passwort
          confpass: Passwort bestätigen
          query: Suchanfrage, nach Menüs suchen

        p-data:
          id: ID des Admins
          menus:
            all: Alle Menüs aus der Datenbank
            selected: Ausgewählten Menüs für den ausgewählten Admin

        -->

        <user-edit
          inline-template
          :p-state="{
            showResult: '',
            response: {
              title: '',
              message: '',
            },
            dropdownActive: false,
            deleteConfirm: false,
            superadmin: <?php echo $admin_level; ?>,
            pass: '',
            confpass: '',
            query: '',
          }"
          :p-data="{
            id: '<?php echo $id; ?>',
            menus: {
              all: [
              <?php
                while ($row_all_menus = mysqli_fetch_array($result_all_menus)){
                  echo "{ id: '".$row_all_menus['id']."', name: '".$row_all_menus['name']."' },";
                }
              ?>
              ],
              selected: [
              <?php
                while ($row_menus_allowed = mysqli_fetch_array($result_menus_allowed)){
                  echo "{ id: '".$row_menus_allowed['id']."', name: '".$row_menus_allowed['name']."' },";
                }
              ?>
              ],
            },
          }"
        >

          <div>

            <!-- Text -->
            <div class="row mt-3">
              <div class="col">
                <div class="text-left">Falls sie das Passwort dieses Admins nicht ändern wollen, können sie die Passwort Felder leer lassen.
                </div>
              </div>
            </div>

            <!-- Felder - Benutzername, Passwort, Passwort wiederholen -->

            <div class="row justify-content-center mt-3">
              <div class="col-12 col-lg-3 col-sm-6">
                <div class="input-group mb-3">
                  <input type="text" class="form-control br" aria-label="name" name="username" value="<?php echo $row['username'];?>" readonly>
                </div>
              </div>
              <div class="col-12 col-lg-3 col-sm-6">
                <div class="input-group mb-3">
                  <input
                    type="password"
                    class="form-control br"
                    placeholder="Neues Passwort"
                    v-model="st.pass"
                  >
                  <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="von 6 bis 20 Zeichen"/>
                  <span class="input-group-text ms-1">?</span>
                </div>
              </div>
              <div class="col-12 col-lg-3 col-sm-6">
                <div class="input-group mb-3">

                  <input
                    type="password"
                    class="form-control br"
                    placeholder="Passwort wiederholen"
                    v-model="st.confpass"
                  >
                  <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="von 6 bis 20 Zeichen"/>
                  <span class="input-group-text ms-1">?</span>
                </div>
              </div>
              <div class="col-12 col-lg-3 col-sm-6 mb-2">
                <!-- Placeholder -->
              </div>
            </div>

            <!-- Admin Level, Menü aussuchen -->

            <div class="row">
              <div class="col-12 col-lg-3 col-sm-6 mb-2">
                <small><label for="lang" class="text-secondary">
                  Admin-Level
                </label></small>

                <!-- Den Level des Admins ausgewählen
                  @input="st.menuId = 0" - ausgewähles Menü zurücksetzten, falls wir den Admin Level Superadmin auswählen
                -->
                <select
                  class="form-select"
                  name="admin_level"
                  v-model="st.superadmin"
                  @input="st.menuId = 0"
                >
                  <option :value="1">Admin Level 1</option>
                  <option :value="0">Superadmin</option>
                </select>
              </div>

              <div class="row">

                <!-- Falls Admin Level = 1 und kein Menü ausgewählt worden ist
                Text und den ganzen Button rot markieren
                -->
                <div
                  v-if="st.superadmin"
                  class="col mt-3"
                >
                  <small>
                    <span
                      class="me-2"
                      :class="{ 'text-danger': st.showResult === 'choseMenu' }"
                    >
                      Menü freigeben:
                    </span>
                  </small>

                  <div class="btn-group">
                    <button
                      v-if="fMenus.length"
                      @click.prevent="st.dropdownActive = !st.dropdownActive"
                      class="btn dropdown-toggle"
                      :class="st.showResult === 'choseMenu' ? 'btn-outline-danger' : 'btn-outline-secondary'"
                    >
                      Menü auswählen
                    </button>

                    <!-- Falls alle Menüs ausgewählt worden waren -->
                    <button
                      v-if="!fMenus.length"
                      class="btn btn-outline-secondary"
                      disabled
                    >
                      Sie haben alle Menüs ausgewählt
                    </button>

                    <!-- Dropdown Menüsuche -->
                    <div
                      v-if="st.dropdownActive"
                      class="dropdown-overlay"
                      @click="st.dropdownActive = false"
                    >

                    </div>
                    <ul
                      v-if="st.dropdownActive"
                      class="dropdown-menu show"
                      style="
                      top:40px;
                      min-width:250px;"
                    >
                      <li>
                        <div class="px-3">
                          <input
                            type="text"
                            placeholder="Nach einem Menü suchen..."
                            v-model="st.query"
                            class="ddinput w100p"
                          >
                        </div>
                      </li>
                      <li>
                        <hr class="dropdown-divider">
                      </li>
                      <li
                        v-if="menu.active"
                        v-for="menu in fMenus"
                        :key="menu.id"
                        @click="addMenu(menu)"
                      >
                        <a
                          class="dropdown-item"
                          href="#"
                        >
                          {{ menu.name }}
                        </a>
                      </li>
                      <li v-if="isEmptyFMenus">
                        <div
                          class="dropdown-item disabled"
                        >
                          Es wurde kein Menü gefunden
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>

                <div
                  v-if="!st.superadmin"
                  class="col mb-2 mt-3"
                >
                  <span class="text-secondary">
                    <small>Der Superadmin kann alle Menüs bearbeiten.</small>
                  </span>
                </div>
              </div>

          </div>

            <!-- Ausgabe - Benutzer hat bereits Zugriff -->

            <div
              v-if="st.superadmin"
              class="row justify-content-center"
            >
              <div class="col-12 col-sm-12 mt-3 mb-1">
                <small><span class="me-1">Dieser Benutzer hat bereits Zugriff auf:</span>
                  <span
                    v-for="(item,itemInd) in data.menus.selected"
                    class="me-2"
                  >
                    {{ item.name }}
                    <span
                      class="bi-x-square ms-1 button_remove"
                      @click="removeMenu(itemInd)"
                    ></span>
                  </span>

                </small>

              </div>
            </div>

            <!-- Button - Änderungen speichern, Benutzer löschen -->

            <div class="row justify-content-between py-3">
              <div class="col">
                <button
                  class="btn btn-outline-primary me-3 mb-2"
                  @click="saveData('/api/save-user.php')"
                >
                  Änderungen speichern
                </button>
                <a
                  href="delete-user.php?id=<?php echo $id;?>"
                  class="btn btn-outline-danger mb-2"
                  @click.prevent="st.deleteConfirm = true"
                >
                  Benutzer löschen
                </a>
              </div>
              <div class="col">
                <span class="text-danger"><?php if (isset($different_passwords)) {echo $different_passwords; }?></span>
              </div>
            </div>


            <!-- Modal-Fenster -->
            <transition name="fade">
              <div
                v-if="st.deleteConfirm"
                class="modal modal-overlay"
                @click="st.deleteConfirm = false"
              >
                <div class="modal-dialog" style="display:block;">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">
                        Wollen sie diesen Benutzer wirklich entfernen?
                      </h5>
                      <div
                        class="btn-close"
                        @click="st.deleteConfirm = false"
                      ></div>
                    </div>
                    <div class="modal-footer">
                      <button
                        type="button"
                        class="btn btn-secondary"
                        @click.prevent="st.deleteConfirm = false"
                      >
                        Schließen
                      </button>
                      <button
                        type="button"
                        class="btn btn-primary"
                        @click.prevent="goto('delete-user.php?id=<?php echo $id;?>')"
                      >
                        Entfernen
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </transition>

            <!-- Toast mit Status des Fehlers / Bestätigung ausgeben -->
            <div
              class="toast-container position-fixed top-5 bottom-0 start-0 end-0 p-3"
              style="
                width:100%;
                display: flex;
                align-items: center;
                justify-content: center;
                z-index: 1;
              "
            >
              <transition name="fade" :duration="{ enter: 500, leave: 800 }">
                <div
                  v-show="st.showResult"
                  class="my-toast"
                >
                  <div class="toast-header">
                    <svg
                      class="bd-placeholder-img rounded me-2"
                      width="20"
                      height="20"
                      xmlns="http://www.w3.org/2000/svg"
                      aria-hidden="true"
                      preserveAspectRatio="xMidYMid slice"
                      focusable="false"
                    >
                      <rect
                        v-if="st.showResult === 'success'"
                        width="100%" height="100%" fill="#198754"
                      ></rect>
                      <rect
                        v-if="st.showResult === 'error' || st.showResult === 'choseMenu'"
                        width="100%"
                        height="100%"
                        fill="#dc3545"
                      ></rect>
                    </svg>

                    <strong
                      class="me-auto"
                      :class="{
                        'text-success': st.showResult === 'success',
                        'text-danger': st.showResult === 'error' || st.showResult === 'choseMenu',
                      }"
                    >
                      {{ st.response.title }}
                    </strong>
                    <button
                      class="btn-close"
                      @click="st.showResult = ''"
                    ></button>
                  </div>
                  <div class="toast-body">
                    {{ st.response.message }}
                  </div>
                </div>
              </transition>
            </div>

          </div>

        </user-edit>

      </div>
    </div>

  <?php include 'inc/footer.php';?>

  </div> <!-- #app -->

  <?php include 'inc/scripts.php';?>

</body>
</html>
