<?php

session_start();

// Prüfen, ob Benutzer bereits eingeloggt ist, wenn ja, weiterleiten zur Startseite
if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
    header("location: choose-menu.php");
    exit;
}

include('inc/db.php');

// Variablen definieren und mit leeren Werten initialisieren
$username = $password = $post_password = "";
$username_err = $password_err = $login_err = "";

// Abarbeitung der Daten beim Absenden des POST-Formulars
if($_SERVER["REQUEST_METHOD"] == "POST"){

    // Überprüfen, ob der Benutzername leer ist
    if(empty($_POST["username"])){
        $username_err = "Bitte geben sie den Benutzernamen ein.";
    } else{
        $username = $_POST["username"];
    }

    // Überprüfen, ob das Passwort Feld leer ist
    if(empty($_POST["password"])){
        $password_err = "Bitte geben sie das Passwort ein.";
    } else{
        $post_password = $_POST["password"];
    }

    // Anmeldeinformationen validieren
    if(empty($username_err) && empty($password_err)){

        // SELECT Anweisung vorbereiten
        $sql = "SELECT id, username, password, admin_level FROM admins WHERE username = ?";

        if($stmt = mysqli_prepare($conn, $sql)){
            // Variablen als Parameter an die vorbereitete Anweisung binden
            mysqli_stmt_bind_param($stmt, "s", $param_username);

            // Parameter einstellen
            $param_username = $username;

            // Versuch, die vorbereitete Anweisung auszuführen
            if(mysqli_stmt_execute($stmt)){
                // Ergebnis speichern
                mysqli_stmt_store_result($stmt);

                // Überprüfen, ob der Benutzername existiert, wenn ja, dann Passwort prüfen
                if(mysqli_stmt_num_rows($stmt) == 1){
                    // Ergebnisvariablen binden
                    mysqli_stmt_bind_result($stmt, $id, $username, $db_password, $admin_header);
                    if(mysqli_stmt_fetch($stmt)){
                        if($db_password == md5($post_password)){
                            // Passwort ist korrekt, neue Session starten
                            session_start();

                            // Daten in Session-Variablen speichern
                            $_SESSION["loggedin"] = true;
                            $_SESSION["id"] = $id;
                            $_SESSION["username"] = $username;
                            $_SESSION["admin_level"] = $admin_header;

                            // Benutzer zur Startseite weiterleiten
                            header("location: choose-menu.php");
                        }else{
                            // Passwort ungültig, Fehlermeldung anzeigen
                            $login_err = "Falscher Benutzername oder Passwort.";
                        }
                    }
                } else{
                    // Benutzername existiert nicht, Fehlermeldung anzeigen
                    $login_err = "Falscher Benutzername oder Passwort.";
                }
            } else{ // Falls etwas nicht stimmt, anderen Fehler anzeigen
                echo "Oops! Ein Fehler ist aufgetreten.";
            }

            // Anweisung schließen
            mysqli_stmt_close($stmt);
        }
    }

    // Verbindung schließen
    mysqli_close($conn);
}
?>

<!DOCTYPE html>
<html lang="de">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href="/css/bootstrap.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/bootstrap-icons.css" rel="stylesheet">
  <title>Dashboard-Projekt Login</title>
  <?php include 'inc/favicons.php'; ?>
</head>
<body>

  <div id="app">

    <?php $title="Dashboard-Projekt Login"; ?>

    <?php include 'inc/header.php'; ?>

    <div class="content">
      <div class="container">

        <section class="h-100">
          <div class="container h-100">
            <div class="row justify-content-sm-center h-100">
              <div class="col-xxl-4 col-xl-5 col-lg-5 col-md-7 col-sm-9 mb-5">

                <!-- Falls ein Fehler aufgetreten ist -->
                <div class="text-center my-5">
                  <?php
                  if(!empty($login_err)){
                      echo '<div class="alert alert-danger">' . $login_err . '</div>';
                  }
                  ?>
                </div>


                <div class="card shadow-lg">
                  <div class="card-body p-5">

                    <!-- Form - Beginn -->
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">

                      <!-- Benutzername -->
                      <div class="mb-3">
                        <label class="mb-2 text-muted" for="email">Benutzername</label>
                        <input type="text" name="username" class="form-control <?php echo (!empty($username_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $username; ?>">
                        <div class="invalid-feedback">
                          Bitte geben sie den Benutzernamen ein.
                        </div>
                      </div>

                      <!-- Passwort -->
                      <div class="mb-3">
                        <div class="mb-2 w-100">
                          <label class="text-muted" for="password">Passwort</label>
                        </div>
                        <input type="password" name="password" class="form-control <?php echo (!empty($password_err)) ? 'is-invalid' : ''; ?>">
                          <div class="invalid-feedback">
                            Bitte geben sie das Passwort ein.
                          </div>
                      </div>

                      <!-- Einloggen Button -->
                      <div class="d-flex align-items-center">
                        <button type="submit" class="btn btn-primary ms-auto mt-3">
                          Einloggen
                        </button>
                      </div>
                    </form>
                    <!-- Form - Ende -->

                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>

    <?php include 'inc/footer.php';?>

  </div> <!-- #app -->

<?php include 'inc/scripts.php';?>

</body>
</html>
