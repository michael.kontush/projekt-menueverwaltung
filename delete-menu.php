<?php

session_start();

include('inc/db.php');

// Das Menü nach dem Slug aus der URL bekommen
$chosen_menu = $_GET['slug'];

// Menü löschen
$sql = "DELETE FROM menus WHERE slug = '$chosen_menu'";
if (mysqli_query($conn, $sql)) {
    $menu_deleted = "Das Menü \"".$chosen_menu."\" wurde gelöscht. Wir leiten sie zur Startseite weiter...";
} else {
    $menu_deleted = "Ein Fehler ist aufgetreten.";
}

?>

<!DOCTYPE html>
<html lang="de">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href="/css/bootstrap.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/bootstrap-icons.css" rel="stylesheet">
  <title>Menü <?php echo $chosen_menu; ?> wurde gelöscht</title>
  <?php include 'inc/favicons.php'; ?>
</head>
<body>

  <div id="app">

    <?php $title="Das Menü \"$chosen_menu\" wurde gelöscht"; ?>

    <?php include 'inc/header.php'; ?>

    <div class="content">
      <div class="container">

        <!-- Text und weiterleiten zur Startseite -->
        <div class="row gx-10 py-2">
          <div class="col">
            <div class="p-3 text-center text-danger fw-bold"><?php echo $menu_deleted; header("refresh:5;url=choose-menu.php");?></div>
          </div>
        </div>

      </div>
    </div>

    <?php include 'inc/footer.php';?>

  </div> <!-- #app -->

<?php include 'inc/scripts.php';?>

</body>
</html>
