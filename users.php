<?php

session_start();

if(empty($_SESSION["id"])){
  header("location: login.php");
}

include 'inc/db.php';

$username = "";
$errors = array();

// Die Daten aller Menüs bekommen, um sie in der Dropdownliste der Menüauswahl anzuzeigen
$sql_2 = "SELECT id, name, slug FROM menus ORDER BY id DESC";
$result_2 = mysqli_query($conn,$sql_2);

if (isset($_POST['reg_user'])) { // Benutzer Registrierung

  // Alles aus der Form bekommen
  $username = mysqli_real_escape_string($conn, $_POST['username']);
  $password_1 = mysqli_real_escape_string($conn, $_POST['password_1']);
  $password_2 = mysqli_real_escape_string($conn, $_POST['password_2']);
  $admin_level = mysqli_real_escape_string($conn, $_POST['admin_level']);

  // Falls der Admin kein Superadmin ist
  if ($_POST['admin_level'] !== '0'){
    $menu_id = mysqli_real_escape_string($conn, $_POST['menu_id']);
  }

  // Datum der Hinzugefügung des Admins
  $creation_date = date("Y-m-d H:i:s");

  // Validierung
  if (empty($username)) { array_push($errors, "Der Benutzername muss eingegeben werden"); }
  if (empty($password_1)) { array_push($errors, "Das Passwort muss eingegeben werden"); }
  if ($password_1 != $password_2) {
	array_push($errors, "Die Passwörter stimmen nicht überein");
  die;
  }

  // Überprüfen, ob ein User mit dem selben Username existiert
  $user_check_query = "SELECT * FROM admins WHERE username='$username' LIMIT 1";

  $result_user = mysqli_query($conn, $user_check_query);
  $user = mysqli_fetch_assoc($result_user);

  if ($user) {
    if ($user['username'] === $username) {
      array_push($errors, "Dieser Benutzername existiert bereits.");
    }
  }


  if (count($errors) == 0) { // Wenn es keine Fehler gibt
  	$password = md5($password_1); // Passwort verschlüsseln

    // User in die DB hinzufügen
  	$query = "INSERT INTO admins (username, password, admin_level, creation_date)
  			  VALUES('$username', '$password', '$admin_level', '$creation_date')";

    mysqli_query($conn, $query);

    // Daten des gerade eben hinzugefügten Admin bekommen
    $query2 = "SELECT id, admin_level FROM admins ORDER BY id DESC LIMIT 1";
    $result2 = mysqli_query($conn,$query2);
    $row2 = mysqli_fetch_array($result2);

    $admin_id = $row2['id'];

    // Falls der Admin kein Superadmin ist, Menü Zugriff dem Benutzer erlauben
    if (($row2['admin_level']) !== '0'){

      $query3 = "INSERT INTO admins_menus (admin_id, menu_id)
    	VALUES('$admin_id', '$menu_id')";

      mysqli_query($conn, $query3);

    }

  }
}

?>

<!DOCTYPE html>
<html lang="de">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href="/css/bootstrap.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/bootstrap-icons.css" rel="stylesheet">
  <title>Benutzerverwaltung</title>
  <?php include 'inc/favicons.php'; ?>
  <style>[v-cloak]{display:none!important;}</style>
</head>
<body>

  <div v-cloak id="app">

    <?php $title="Benutzerverwaltung"; ?>

    <?php include 'inc/header.php'; ?>

    <div class="content">
      <div class="container">

        <?php

        // Falls der Admin kein Superadmin ist
        if($_SESSION["admin_level"] != '0'){

          echo '

          <div class="row py-2">
            <div class="col">
              <div class="p-3 text-center">Diese Seite steht nur Superadmins zur Verfügung.</div>
            </div>
          </div>';

          echo '</div>
          </div>'; // #content und #container

          include 'inc/footer.php';

          echo '</div>'; // #app

          include 'inc/scripts.php';

            die;

        }

         ?>

        <!-- Title -->
        <div class="row">
          <div class="col mb-3 mt-3">
            <b>» Neuen Benutzer hinzufügen</b>
          </div>
        </div>

        <!--

        p-state
          username: Benutzername
          pass: Passwort
          confpass: Passwort-Bestätigung
          superadmin: 1 = Admin Level Superadmin
          menuId: ID des Menüs, das wir dem Admin freigeben
          showResult: Toast anzeigen
            success - Alles okay
            error - Fehler ist aufgetreten
            choseMenu - sie müssen ein Menü auswählen
          response: Was wir im Toast anzeigen
            title: Überschrift im Toast
            message: Nachricht im Toast
        p-errors: Fehler

        -->

        <form-validation
          inline-template
          :p-state="{
            username: '',
            pass: '',
            confpass: '',
            superadmin: 1,
            menuId: 0,
            showResult: '',
            response: {
              title: '',
              message: '',
            },
          }"
          :p-errors="{}"
        >
          <div>

            <!-- Input Felder Benutzername, Passwort -->
            <div class="row justify-content-center mt-1">
              <div class="col-12 col-lg-3 col-sm-6">
                <div class="input-group mb-3">
                  <input
                    type="text"
                    class="form-control br"
                    placeholder="Benutzername"
                    v-model="st.username"
                  >
                  <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="von 6 bis 20 Zeichen"/>
                  <span class="input-group-text ms-1">?</span>
                </div>
              </div>
              <div class="col-12 col-lg-3 col-sm-6">
                <div class="input-group mb-3">
                  <input
                    type="password"
                    class="form-control br"
                    placeholder="Neues Passwort"
                    v-model="st.pass"
                  >
                  <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="von 6 bis 20 Zeichen"/>
                  <span class="input-group-text ms-1">?</span>
                </div>
              </div>
              <div class="col-12 col-lg-3 col-sm-6">
                <div class="input-group mb-3">

                  <input
                    type="password"
                    class="form-control br"
                    placeholder="Passwort wiederholen"
                    v-model="st.confpass"
                  >
                  <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="von 6 bis 20 Zeichen"/>
                  <span class="input-group-text ms-1">?</span>
                </div>
              </div>
              <div class="col-12 col-lg-3 col-sm-6 mb-2">
                <!-- Placeholder -->
              </div>
            </div>



            <!-- Admin Level -->
            <div class="row">

              <div class="col-12 col-lg-3 col-sm-6 mb-2">
                <small><label for="lang" class="text-secondary">
                  Admin-Level
                </label></small>
                  <select
                    class="form-select"
                    name="admin_level"
                    v-model="st.superadmin"
                    @input="st.menuId = 0"
                  >
                    <option :value="1">Admin Level 1</option>
                    <option :value="0">Superadmin</option>
                  </select>
              </div>


              <!-- Menü aussuchen im Dropdown falls Admin Level = 1 -->
              <dropdown-list
                v-if="st.superadmin"
                inline-template
                :p-state="{
                  dropdownActive: false,
                  selected: null,
                  query: '',
                }"
                @update-menu-id="setMenuId"
                :p-list="[
                  <?php while ($row_2 = mysqli_fetch_array($result_2)) {
                    echo '{ id: '.$row_2['id'].', name: \''.$row_2['name'].'\' },';
                  } ?>
                ]"
              >
                <div
                  class="col-12 col-lg-3 col-sm-6 mb-2"
                  style="position:relative;"
                >
                    <div class="text-secondary">
                      <small>
                        <span :class="{ 'text-danger': $parent.st.showResult === 'choseMenu' }">
                          Menü zum Bearbeiten freigeben
                        </span>
                      </small>
                    </div>

                    <div class="btn-group">
                      <button
                        v-if="list.length"
                        @click.prevent="st.dropdownActive = !st.dropdownActive"
                        class="btn btn-outline-secondary dropdown-toggle"
                        :class="$parent.st.showResult === 'choseMenu' ? 'btn-outline-danger' : 'btn-outline-secondary'"
                      >
                        <span v-if="st.selected === null">
                          Aus der Liste auswählen
                        </span>
                        <span v-else>
                          {{ list[st.selected].name }}
                        </span>
                      </button>

                      <div
                        v-if="st.dropdownActive"
                        class="dropdown-overlay"
                        @click="st.dropdownActive = false"
                      ></div>
                      <ul
                        v-if="st.dropdownActive"
                        class="dropdown-menu show"
                        style="
                          top:40px;
                          min-width:250px;
                        "
                      >
                        <li>
                          <div class="px-3">
                            <input
                              type="text"
                              placeholder="Nach einem Menü suchen..."
                              v-model="st.query"
                              class="ddinput w100p"
                            >
                          </div>
                        </li>
                        <li>
                          <hr class="dropdown-divider">
                        </li>
                        <li
                          v-for="(item,itemInd) in avList"
                          :key="item.id"
                          @click="selectItem(item.id)"
                        >
                          <a
                            class="dropdown-item"
                            href="#"
                            @click.prevent=""
                          >
                            {{ item.name }}
                          </a>
                        </li>
                        <li v-if="avList.length === 0">
                          <div
                            class="dropdown-item disabled"
                          >
                            Es wurde kein Menü gefunden
                          </div>
                        </li>
                      </ul>
                    </div>
                    <input
                      v-if="st.selected !== null"
                      type="hidden"
                      name="menu_id"
                      :value="list[st.selected].id"
                    >
                </div>
              </dropdown-list>

              <div
                v-if="!st.superadmin"
                class="col-12 col-lg-6 col-sm-6 mb-2"
                style="margin-top: 29px;"
              >
                <span class="text-secondary">
                  <small>Der Superadmin kann alle Menüs bearbeiten.</small>
                </span>
              </div>
            </div>

            <!-- Neuen Admin hinzufügen Button & Errors -->
            <div class="row py-3">
              <div class="col">
                  <button
                    class="btn btn-outline-primary"
                    name="reg_user"
                    @click="submit('/api/create-user.php')"
                  >
                    Neuen Admin hinzufügen
                  </button>
              </div>
            </div>

            <!-- Admin-Liste Title -->
            <div class="row">
              <div class="col mb-2 mt-3">
                <b>» Admin-Liste</b>
              </div>
            </div>


            <!--
            deleteConfirm: Modal Fenster anzeigen
            userId: die ID des zu löschenden Benutzers für das Modal Fenster
            -->
            <users-list
              inline-template
              :p-state="{
                deleteConfirm: false,
                userId: null,
              }"
              :p-users="[]"
            >
              <div class="row">
                <div class="col">
                  <table class="table">
                    <thead>
                      <tr class="mob-flex">
                        <th class="mob-block" scope="col">Benutzername</th>
                        <th class="mob-block" scope="col">Rechte</th>
                        <th class="mob-block" scope="col">Offene Menüs</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php

                      // Alle Daten der Admins aus der Datenbank bekommen
                      $result_show_admin = mysqli_query($conn,"SELECT id, username, admin_level FROM admins ORDER BY id DESC");

                      while ($row_show_admin = mysqli_fetch_array($result_show_admin)) {

                          $id = $row_show_admin['id'];
                          $username = $row_show_admin['username'];

                          $show_admin = $row_show_admin['admin_level'];

                          // Falls $show_admin = 0: Superadmin
                          // Falls $show_admin = 1: Admin Level 1
                          switch($show_admin){
                              case "0":
                                  $show_admin = "<span class=\"text-danger\">Superadmin</span>";
                                  break;
                              case "1":
                                  $show_admin = "Admin Level 1";
                                  break;
                          }

                          $id = $row_show_admin['id'];

                          // Alle Menüs, die dem Admin zur Bearbeitung offen stehen, ausgeben
                          $result_menu_allow = mysqli_query($conn,"SELECT GROUP_CONCAT(name SEPARATOR ', ') AS name FROM admins_menus, menus WHERE admin_id = '$id' AND menu_id = menus.id;");
                          $row_menu_allow = mysqli_fetch_array($result_menu_allow);

                          if (isset($row_menu_allow['name'])){
                            $menu_allow = $row_menu_allow['name'];}
                          else{ // Falls Admin = Superadmin, folgenden Text anzeigen
                            $menu_allow = "Alle Menüs";
                          }

                            echo '
                            <tr class="mob-flex">
                              <td class="mob-block nowrap">
                                <a class="link_grey wrap" href="edit-user.php?id='.$id.'">'.$row_show_admin['username'].'</a>
                                <a
                                  class="bi-x-square ms-1 button_remove"
                                  href="delete-user.php?id='.$row_show_admin['id'].'"
                                  @click.prevent="showDeleteConfirm('.$row_show_admin['id'].')"
                                ></a>
                              </td>
                              <td class="mob-block">'.$show_admin.'</td>
                              <td class="mob-block">'.$menu_allow.'</td>
                            </tr>
                            ';
                        }
                      ?>
                    </tbody>
                  </table>
                </div>

                <!-- Modal Fenster -->
                <transition name="fade">
                  <div
                    v-if="st.deleteConfirm"
                    class="modal modal-overlay"
                    @click="st.deleteConfirm = false"
                  >
                    <div class="modal-dialog" style="display:block;">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title">
                            Wollen sie diesen Benutzer wirklich entfernen?
                          </h5>
                          <div
                            class="btn-close"
                            @click="st.deleteConfirm = false"
                          ></div>
                        </div>
                        <div class="modal-footer">
                          <button
                            type="button"
                            class="btn btn-secondary"
                            @click.prevent="st.deleteConfirm = false"
                          >
                            Abbrechen
                          </button>
                          <button
                            type="button"
                            class="btn btn-primary"
                            @click.prevent="deleteUser"
                          >
                            Entfernen
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </transition>



              </div>
            </users-list>


            <!-- Toast -->
            <div
              class="toast-container position-fixed top-5 bottom-0 start-0 end-0 p-3"
              style="
                width:100%;
                display: flex;
                align-items: center;
                justify-content: center;
                z-index: 1;
              "
            >
              <transition name="fade" :duration="{ enter: 500, leave: 800 }">
                <div
                  v-show="st.showResult"
                  class="my-toast"
                >
                  <div class="toast-header">
                    <svg
                      class="bd-placeholder-img rounded me-2"
                      width="20"
                      height="20"
                      xmlns="http://www.w3.org/2000/svg"
                      aria-hidden="true"
                      preserveAspectRatio="xMidYMid slice"
                      focusable="false"
                    >
                      <rect
                        v-if="st.showResult === 'success'"
                        width="100%" height="100%" fill="#198754"
                      ></rect>
                      <rect
                        v-if="st.showResult === 'error' || st.showResult === 'choseMenu'"
                        width="100%"
                        height="100%"
                        fill="#dc3545"
                      ></rect>
                    </svg>

                    <strong
                      class="me-auto"
                      :class="{
                        'text-success': st.showResult === 'success',
                        'text-danger': st.showResult === 'error' || st.showResult === 'choseMenu',
                      }"
                    >
                      {{ st.response.title }}
                    </strong>
                    <button
                      class="btn-close"
                      @click="st.showMessage = ''"
                    ></button>
                  </div>
                  <div class="toast-body">
                    {{ st.response.message }}
                  </div>
                </div>
              </transition>
            </div>

          </div>
        </form-validation>

    </div>
  </div>

<?php include 'inc/footer.php';?>

</div> <!-- #app -->

<?php include 'inc/scripts.php';?>

</body>
</html>
