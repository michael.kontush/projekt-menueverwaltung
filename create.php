<?php

session_start();

include 'inc/db.php';

if(empty($_SESSION["id"])){
  header("location: login.php");
}

// Alle Slugs bekommen, um zu überprüfen, ob so ein Slug bereits existiert
$sql_slug = "SELECT slug FROM menus ORDER BY slug DESC";
$result_slug = mysqli_query($conn,$sql_slug);

$slugs = '[';
while($row = mysqli_fetch_array($result_slug)){
  $slugs .= '\''.$row['slug'].'\', ';
}
$slugs .= ']';

?>

<!DOCTYPE html>
<html lang="de">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href="/css/bootstrap.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/bootstrap-icons.css" rel="stylesheet">
  <link href="/css/spectrum.min.css" rel="stylesheet">
  <title>Neues Menü erstellen</title>
  <style>[v-cloak]{display:none!important;}</style>
  <?php include 'inc/favicons.php'; ?>
</head>

<body>

<div v-cloak id="app">

  <?php $title="Neues Menü erstellen"; ?>

  <?php include 'inc/header.php'; ?>

  <div class="content">
    <div class="container">

      <!--
      p-dnd: Modell für den Drag&Drop, wird auf edit-menu.php benutzt
      p-state: Zustand der Komponenten
        id: ID des Menüs
        edited: wurden Felder bearbeitet?
        slugs: alle Slugs aus der Datenbank
        curSlug: der Slug der ausgewählten Menüs
      p-users
        all: Alle Benutzer, die in der Datenbank existieren
        selected: Ausgewählte Benutzer, die Zugriff auf das Menü haben (wird auf edit-menu.php benutzt)
      p-menu: Modell mit leeren Daten vorbereiten
     -->

      <menu-edit
        inline-template
        :p-dnd="{}"
        :p-state="{
          id: 0,
          edited: false,
          slugs: <?php echo $slugs; ?>,
          curSlug: '',
        }"
        :p-users="{
          all: [],
          selected: [],
        }"
        :p-menu="{
          'id': 0,
          'slug': '',
          'name': '',
          'bg_1': '',
          'bg_2': '',
          'bg_3': '',
          'font_size': '',
          'font_color': '',
          'special_param': '',
          'list': {
            'de': [],
            'en': [],
          },
        }"
      >
        <div id="createMenu">

          <!-- Input Felder - Anzeigename und Tag -->

          <div class="row mt-3 justify-content-center">
            <div class="col-12 col-lg-3 col-sm-6 mb-2">
              <div class="input-group mb-3">
                <!--
                is-invalid: Falls es Fehler gibt
                clearError: Fehlermeldung löschen falls es Änderungen im Feld gab
               -->
                <input
                  type="text"
                  class="form-control br"
                  :class="{ 'is-invalid': errors && errors.name }"
                  placeholder="Anzeigename*"
                  required
                  @input="clearError('name')"
                  v-model="menu.name"
                >

                <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="z.B: Hauptmenu der Uni"/>
                <span class="input-group-text ms-1">?</span>
              </div>
            </div>
            <div class="col-12 col-lg-3 col-sm-6 mb-2">
              <div class="input-group mb-3">
                <input
                  type="text"
                  class="form-control br"
                  :class="{ 'is-invalid': errors && errors.slug }"
                  placeholder="Tag*"
                  required
                  @input="clearError('slug')"
                  v-model="menu.slug"
                >
                <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="z.B: hauptmenu-2022"/>
                <span class="input-group-text ms-1">?</span>
              </div>
            </div>
            <div class="col-12 col-lg-3 col-sm-6 mb-2">
              <!-- Placeholder -->
            </div>
            <div class="col-12 col-lg-3 col-sm-6 mb-2">
              <!-- Placeholder -->
            </div>
          </div>


          <!-- Input Felder - Hintergrundfarbe, Textfarbe, Schriftgröße -->
          <div class="row">
            <div class="col-12 col-lg-3 col-sm-6 mb-2">
              <div class="input-group mb-3 fwnw">
                <input
                  id="picker_bg_1"
                  class="form-control brr"
                  placeholder="BG Colour (1 LVL)*"
                  :value="menu.bg_1"
                />
                <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="Hintergrundfarbe, z.B: blue oder #ffffff"/>
                <span class="input-group-text ms-1">?</span>
              </div>
            </div>
            <div class="col-12 col-lg-3 col-sm-6 mb-2">
              <div class="input-group mb-3 fwnw">
                <input
                  id="picker_bg_2"
                  class="form-control brr"
                  placeholder="BG Colour (2 LVL)*"
                  :value="menu.bg_2"
                />
                <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="Hintergrundfarbe, z.B: blue oder #ffffff"/>
                <span class="input-group-text ms-1">?</span>
              </div>
            </div>
            <div class="col-12 col-lg-3 col-sm-6 mb-2">
              <div class="input-group mb-3 fwnw">
                <input
                  id="picker_bg_3"
                  class="form-control brr"
                  placeholder="BG Colour (3 LVL)*"
                  :value="menu.bg_3"
                />
                <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="Hintergrundfarbe, z.B: blue oder #ffffff"/>
                <span class="input-group-text ms-1">?</span>
              </div>
            </div>
            <div class="col-12 col-lg-3 col-sm-6 mb-2">
            </div>
          </div>

          <div class="row">
            <div class="col-12 col-lg-3 col-sm-6 mb-2">
              <div class="input-group mb-3 fwnw">
                <input
                  id="picker_font_color"
                  class="form-control brr"
                  placeholder="Textfarbe*"
                  :value="menu.font_color"
                />
                <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="z.B: blue oder #ffffff"/>
                <span class="input-group-text ms-1">?</span>
              </div>
            </div>
            <div class="col-12 col-lg-3 col-sm-6 mb-2">
              <div class="input-group mb-3">
                <input
                  type="text"
                  class="form-control br"
                  :class="{ 'is-invalid': errors && errors.fs }"
                  placeholder="Schriftgröße*"
                  v-model="menu.font_size"
                  @input="clearError('font_size')"
                >
                <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="z.B: von 5 bis 25px, z.B: 12px oder 0.95em" />
                <span class="input-group-text ms-1">?</span>
              </div>
            </div>
            <div class="col-12 col-lg-3 col-sm-6 mb-2">
            </div>
          </div>

          <!-- Input Feld - CSS Styles -->
          <div class="row">
            <div class="col-12 col-sm-12 col-lg-6">
              <div class="input-group">
                <span class="input-group-text">CSS Styles</span>
                <textarea
                  class="form-control"
                  v-model="menu.special_param"
                  style="border-radius:0 .25rem .25rem 0;"
                ></textarea>

                <div style="display:inline-block;">
                  <div class="input-group" style="height:100%;">
                    <input type="hidden">
                    <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="CSS Styles, z.B: font-style: italic;"/>
                    <span class="input-group-text ms-1" style="height: 100%;">?</span>
                  </div>
                </div>

              </div>
            </div>
            <div class="col">
              <!-- Placeholder -->
            </div>
          </div>

          <!-- Button - Menu Erstellen
          saveData: Daten werden auf create.php weitergeleitet
          -->

          <div class="row my-4">
            <div class="col">
              <input
                value="Neues Menü erstellen"
                type="button"
                class="btn btn-outline-primary"
                @click="saveData('/api/create.php')"
              >
            </div>
          </div>

          <!-- Falls es Fehler gibt -->
          <div
            v-if="errors !== null"
            class="text-danger mt-3"
          >
            <b>Folgende Fehler sind aufgetreten:</b>
            <span v-for="err in errors" class="mt-2">
                {{ err }}.
            </span>
          </div>

        </div>
      </menu-edit>

    </div>
  </div>

<?php include 'inc/footer.php';?>

</div> <!-- #app -->

<?php include 'inc/scripts.php';?>

</body>
</html>
