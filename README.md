# Projekt Menüverwaltung

## Abstract
Ziel des Projekts im Rahmen des Projektseminars Systementwicklung war die Erstellung einer
Menüverwaltung zum einfachen Anlegen, Bearbeiten, Verwalten und Testen von hierarchischen
Menüs mit spezifischem Blick auf die Anforderungen der Intra- und Internetpräsenz der Universität
Göttingen.

Die erstellten hierarchischen Menüs müssen es spezifisch erlauben die Probleme von derzeit
fehlendem Direktzugriff auf untere Hierarchieebenen zu minimieren.

Die Aufgabe beinhaltete auch die Anforderung einer Benutzerverwaltung, um kollaboratives
Bearbeiten von Menüs durch mehrere BenutzerInnen zu erlauben und fremde Menüs vor
unerlaubtem Zugriff zu schützen.

Die webbasierte Benutzeroberfläche wurde in zeitgemässem Design erstellt. Drag-and-Drop
Funktionalität und eine Echtzeitvorschau der erstellten Menüs erlaubt die einfache Nutzung der
Menüverwaltung auch durch weniger technikafine BenutzerInnen.

Die Unterstützung von Desktop und Mobilplattformen sollte gleichwertig umgesetzt werden, und
BenutzerInnen, die beide Zugangsmöglichkeiten nutzen sollten von einem maximalen
Wiedererkennungseffekt profitieren.

Die Möglichkeit mehrsprachige Menüs zu unterstützen wurde initial für Deutsch/Englisch umgesetzt,
das Konzept wurde aber bewusst auf Erweiterbarkeit auf weitere Sprachen entworfen.

Die erstellten Menüs können durch den generierten HTML code auf den eigenen Seiten direkt
eingesetzt werden. Der Export als JSON file erlaubt zusätzlich die erstellte Menüstruktur bei Bedarf
in andere Applikationen zu integrieren.

## Installation
Die Vorausetzungen für eine Nutzung unseres Projektes ist Ubuntu 18, PHP 8.0 unter Apache 2.4 und MySQL 5.7.37. 

1. Extrahieren sie alle Dateien in das Root-Verzeichnis (/) des Web-Servers
2. Importieren sie unseren MySQL-Datenbank Dump (dump/msis22db.sql) in die Datenbank Ihres Web-Servers
3. Geben sie die richtigen Zugangsdaten zur Datenbank in der Datei inc/db.php ober belassen sie den gesetzten Default (für lokale Testserver)
4. Öffnen sie die Startseite (für eine lokale Maschine z. B http://127.0.0.1/ )
5. Loggen sie sich als Superadmin ein:
- username: s-admin
- password: 111111

Jetzt können sie unser Menüverwaltungssystem benutzen und an ihre Bedürfnisse anpassen!

## Autoren
Steffen Kober und Michael Kontush
