<?php

// Session initialisieren
session_start();

// Überprüfen, ob der Benutzer angemeldet ist, wenn nicht, zur Login Seite weiterleiten
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

include 'inc/db.php';

// Alle Menüs für die Buttons und die Suche bekommen
if ($_SESSION["admin_level"] == 0){

  // Wenn Superadmin, dann alle Menüs anzeigen
  $sql = "SELECT id, slug, model, name FROM menus ORDER BY id DESC";

  // Test falls es keine Menüs gibt
  // $sql = "SELECT id, slug, model, name FROM menus ORDER BY id DESC LIMIT 0";
  $result = mysqli_query($conn,$sql);

}elseif ($_SESSION["admin_level"] == 1){

  // Wenn Admin Level 1, dann nur die Menüs anzeigen, die dem Admin zur Verfügung stehen
  $sql = "SELECT menus.id, slug, model, name FROM menus, admins_menus WHERE menus.id = admins_menus.menu_id AND admins_menus.admin_id = '".$_SESSION["id"]."' ORDER BY id DESC";
  $result = mysqli_query($conn,$sql);

}

?>

<!DOCTYPE html>
<html lang="de">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href="/css/bootstrap.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/bootstrap-icons.css" rel="stylesheet">
  <title>Dashboard-Projekt</title>
  <?php include 'inc/favicons.php'; ?>
  <style>[v-cloak]{display:none!important;}</style>
</head>
<body>

  <div v-cloak id="app">

    <?php include 'inc/header.php'; ?>

    <div class="content">
      <search-menus
        inline-template
        :p-state="{
          query: '', <!-- Suchanfrage -->
          deleteConfirm: false,
          menuSlug: null,
        }"
        :p-menus="[
          <?php // Alle Parameter der Menüs bekommen und ins Modell einfügen
            while ($row = mysqli_fetch_array($result)){
              $model = json_decode($row['model'], true);

              echo '{
                slug: \''.$row['slug'].'\',
                name: \''.$row['name'].'\',
                bg: \''.$model['bg_1'].'\',
              },';
            }
          ?>
        ]"
      >
      <div class="container">

        <?php

        // Falls es keine Menüs gibt, Text und "Neues Menü erstellen" button anzeigen
        if (mysqli_num_rows($result)==0) {

        echo '

        <div class="row py-2">
          <div class="col">
            <div class="p-3 text-center">Es gibt noch keine Menüs. Sobald es wenigstens ein Menü gibt, wird es hier sichtbar sein.</div>
          </div>
        </div>

        <div class="row pb-4 text-center">
          <div class="col">
              <a href="create.php"><button type="button" class="btn btn-outline-primary fs-6">Neues Menü erstellen</button></a>
          </div>
        </div>

        ';

      }else{ // Text und Suchzeile

        echo '

        <div class="row mt-3">
          <div class="col">
            <div class="text-center">
              Bitte wählen sie ein Menü aus der unteren Liste aus oder klicken sie auf <b>"Neues menü erstellen"</b>, um mit der Fertigung zu beginnen. Unten werden die 5 letzten Menüs angezeigt. Dazu können sie die Suchfunktion benutzten.
            </div>
          </div>
        </div>

        <div class="row justify-content-md-center">

          <div class="col-12 col-xl-3 col-lg-4 col-md-5 col-sm-6 mb-2 mb-2 mt-3 text-center">
            <a
              href="create.php"
              class="btn btn-outline-primary"
            >
              Neues Menü erstellen
            </a>
          </div>

          <div class="col-12 col-xl-3 col-lg-4 col-md-5 col-sm-6 mb-2 mb-2 mt-3">
            <div class="input-group mb-3">
              <input
                type="text"
                class="form-control br"
                placeholder="Nach einem Menü suchen"
                v-model="st.query"
              >
              <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="z.B: hauptmenu-2022"/>
              <span class="input-group-text ms-1">?</span>
            </div>
          </div>

        </div>

        ';

        // v-if wird dafür benutzt, um Menüs aus der Suche zu verstecken, die der Suchanfrage nicht entsprechen
        echo '

        <transition name="fade">
          <div
            v-if="activeMenus === 0"
            class="col text-center mb-2"
          >
            <small>Es wurde kein Menü gefunden.</small>
            <span
              @click="st.query = \'\'"
            >
              <small style="text-decoration:underline; cursor: pointer;">Alle Menüs anzeigen</small>
            </span>

          </div>
        </transition>

        <div class="row">

          <div class="col">

            <div
              class="text-center"
              style="
                display: flex;
                flex-wrap: wrap;
                justify-content: center;
              "
            >

              <transition-group
                name="menu"
                style="
                  display: flex;
                  flex-wrap: wrap;
                  justify-content: center;
                "
              >

                <a
                  v-if="menu.active"
                  v-for="(menu,menuInd) in avMenus"
                  :key="menu.slug"
                  class="btn btn-success me-2 large-button mb-3"
                  :href="\'edit-menu.php?slug=\' + menu.slug"
                  role="button"
                  :style="{ backgroundColor: menu.bg }"
                >

                  <span class="w100p word-wrap">
                    {{ menu.name }}
                    <br>
                    [{{ menu.slug }}]
                  </span>

                </a>
              </transition-group>

            </div>

          </div>

        </div>

        ';

      }

        ?>



        <?php

        // Tabelle mit allen Menüs anzeigen, falls weigstens 1 Menü gefunden worden ist
        if (mysqli_num_rows($result)!==0) {

          echo '

          <div class="row">
            <div class="col">
              <table class="table">
                <thead>
                  <tr class="mob-flex">
                    <th class="mob-block" scope="col">Name des Menüs</th>
                    <th class="mob-block" scope="col">Slug</th>
                    <th class="mob-block" scope="col">Offen für Benutzer</th>
                  </tr>
                </thead>
                <tbody>

          ';

          // Zahl der Menüs, die wir anzeigen pro Seite
          $limit = 4;

          // Aktive Seite anzeigen
          if (isset($_GET["page"])) {

              $page_number = $_GET["page"];

          }else {

            $page_number = 1;

          }

          // Die initiale Seitenzahl bekommen
          $initial_page = ($page_number-1) * $limit;

          // Ausgeben der Daten pro Seite
         if ($_SESSION["admin_level"] == 0){ // Wenn Superadmin, dann alle Menüs anzeigen

           $result_show_menu = mysqli_query($conn,"SELECT id, name, slug FROM menus ORDER BY id DESC LIMIT $initial_page, $limit");

         }elseif ($_SESSION["admin_level"] == 1){ // Wenn Admin Level 1, dann nur die Menüs anzeigen, die dem Admin zur Verfügung stehen

           $result_show_menu = mysqli_query($conn,"SELECT menus.id, name, slug FROM menus, admins_menus WHERE menus.id = admins_menus.menu_id AND admins_menus.admin_id = '".$_SESSION["id"]."' ORDER BY id DESC LIMIT $initial_page, $limit");

         }

         while ($row_show_menu = mysqli_fetch_array($result_show_menu)) {

             $id = $row_show_menu['id'];
             $name = $row_show_menu['name'];
             $slug = $row_show_menu['slug'];

             $menu_id = $row_show_menu['id'];

               // Den Namen und den Slug des Menüs anzeigen

              echo '
              <tr class="mob-flex">
                <td class="mob-block nowrap">
                  <a
                    class="link_grey wrap"
                    href="edit-menu.php?slug='.$slug.'"
                  >'.$name.'</a>

                  <a
                    class="bi-x-square ms-1 button_remove"
                    href="delete-menu.php?slug='.$slug.'"
                    @click.prevent="removeMenu(\''.$slug.'\')"
                  ></a>

                  </td>
                 <td class="mob-block">'.$slug.'</td>
                 <td class="mob-block">
                 ';

                 // Alle Benutzernamen anzeigen, die auf dieses Menü Zugriff haben
                 $sql_menu_allow = "SELECT GROUP_CONCAT(username SEPARATOR ', ') AS username FROM admins, admins_menus WHERE admins.id = admins_menus.admin_id AND menu_id = '$id'";
                 $result_menu_allow = mysqli_query($conn,$sql_menu_allow);

                 while ($row_menu_allow = mysqli_fetch_array($result_menu_allow)){
                   if (isset($row_menu_allow['username'])){ // Wenn es 1 oder mehr Benutzer gibt, die auf das Menü zugreifen können
                     $allowed_users = $row_menu_allow['username'];}
                   else{ // Wenn es keinen Benutzer gibt, der zugewiesen ist, dann kann das Menü nur durch Superadmins bearbeitet werden
                     $allowed_users = "<span class=\"text-danger\">Nur Superadmins</span>";
                   }
                   echo $allowed_users;
                 }

               echo '
                 </td>
               </tr>
               ';


           }

        echo '

                    </tbody>
                  </table>
                </div>
              </div>

            ';

        // Pagination erstellen
        echo '

              <div class="row">
                <div class="col ms-2">
                  <ul class="pagination">

        ';

          if ($_SESSION["admin_level"] == 0){ // Falls User ein Superadmin ist

            // Die Anzahl der Menüs, die dem Superadmin zur Verfügung stehen anzeigen (alle Menüs)
            $count_query = "SELECT COUNT(*) FROM menus";
            $count_result = mysqli_query($conn, $count_query);
            $count_row = mysqli_fetch_row($count_result);

            $total_rows = $count_row[0];

          }elseif ($_SESSION["admin_level"] == 1){ // Falls User ein Admin-Level 1 Admin ist

            // Die Anzahl der Menüs, die dem eingeloggten Admin zur Verfügung stehen anzeigen
            $count_query = "SELECT COUNT(*) FROM menus, admins_menus WHERE menus.id = admins_menus.menu_id AND admins_menus.admin_id = '".$_SESSION["id"]."'";
            $count_result = mysqli_query($conn, $count_query);
            $count_row = mysqli_fetch_row($count_result);

            $total_rows = $count_row[0];

          }

          // Erforderliche Anzahl von Seiten bekommen
          $total_pages = ceil($total_rows / $limit);
          $pageURL = "";

          if($page_number >= 2){

              // Wenn die künftige Seite > 2 ist, dann Button zurück anzeigen
              echo '<li class="page-item">
                <a href="choose-menu.php?page='.($page_number-1).'" class="page-link">Zurück</span>
              </li>';

          }

          for ($i=1; $i<=$total_pages; $i++) {
            if ($i == $page_number) {

                // Die ausgewählte Zahl als active markieren, ohne Link
                $pageURL .= '<li class="page-item active" aria-current="page">
                  <span class="page-link">'.$i.'</span>
                </li>';

            }else{

                // Andere Seitenzahlen anzeigen mit Links
                $pageURL .= '<li class="page-item"><a class="page-link" href="choose-menu.php?page='.$i.'">'.$i.'</a></li>';

            }

          };

          echo $pageURL;

          if($page_number<$total_pages){

              // Wenn wir uns nicht auf der letzten Seite befinden, Vorwärts Knopf hinzufügen
              echo '<li class="page-item">
                <a class="page-link" href="choose-menu.php?page='.($page_number+1).'">Vorwärts</a>
              </li>';

          }

        echo '

              </ul>
            </div>
          </div>

        ';

        }

        ?>

        <!-- Modal Beginn -->
        <transition name="fade">
          <div
            v-if="st.deleteConfirm"
            class="modal modal-overlay"
            @click="st.deleteConfirm = false"
          >
            <div class="modal-dialog" style="display:block;">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">
                    Wollen sie das Menü wirklich entfernen?
                  </h5>
                  <div
                    class="btn-close"
                    @click="st.deleteConfirm = false"
                  ></div>
                </div>
                <div class="modal-footer">
                  <button
                    type="button"
                    class="btn btn-secondary"
                    @click.prevent="st.deleteConfirm = false"
                  >
                    Abbrechen
                  </button>
                  <button
                    v-if="st.menuSlug"
                    type="button"
                    class="btn btn-primary"
                    @click.prevent="goto('delete-menu.php?slug=' + st.menuSlug)"
                  >
                    Entfernen
                  </button>
                </div>
              </div>
            </div>
          </div>
        </transition>
        <!-- Modal Ende -->

      </div>
    </search-menus>
  </div>

<?php include 'inc/footer.php';?>

</div> <!-- #app -->

<?php include 'inc/scripts.php';?>

</body>
</html>
