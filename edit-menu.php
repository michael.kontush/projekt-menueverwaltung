<?php

session_start();

include('inc/db.php');

// Slug aus URL bekommen
$chosen_menu = $_GET['slug'];

// ID, Modell, Name und Slug des Menüs auswählen
$sql = "SELECT id, model, name, slug FROM menus WHERE slug = '$chosen_menu'";
$result = mysqli_query($conn,$sql);
$row = mysqli_fetch_array($result);
$menu_id = $row['id'];

// Im Modell " mit ' ersetzen
$final_model = str_replace("\"","'", $row['model']);

// Alle Benutzer aus der DB auswählen
$sql_all_users = "SELECT id, username FROM admins ORDER BY id DESC";
$result_all_users = mysqli_query($conn,$sql_all_users);

// Benutzer auswählen, die bereits Zugriff auf Menü haben
$sql_menu_allow = "SELECT admins.id, username FROM admins, admins_menus WHERE admins.id = admins_menus.admin_id AND menu_id = '$menu_id'";
$result_menu_allow = mysqli_query($conn,$sql_menu_allow);

// Variable die uns ermöglicht, die Buttons "HTML Code anzeigen" und "Modell anzeigen" zu verstecken
$show_btns = false;

$sql_slug = "SELECT slug FROM menus ORDER BY slug DESC";
$result_slug = mysqli_query($conn,$sql_slug);

$slugs = '[';
while($row_slug = mysqli_fetch_array($result_slug)){
  $slugs .= '\''.$row_slug['slug'].'\', ';
}
$slugs .= ']';

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href="/css/default.min.css" rel="stylesheet">
  <link href="/css/bootstrap.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/bootstrap-icons.css" rel="stylesheet">
  <link href="/css/spectrum.min.css" rel="stylesheet">
  <title><?php echo "Menü \"$chosen_menu\" bearbeiten"?></title>
  <style>[v-cloak]{display:none!important;}</style>
  <?php include 'inc/favicons.php'; ?>
</head>
<body>

  <div id="app">

    <?php $title="Bearbeitung des Menüs \"$chosen_menu\""; ?>

    <?php include 'inc/header.php'; ?>

    <div class="content">
      <div class="container">

        <?php

            // Prüfen, ob es einen Eintrag für den eingeloggten Admin und das ausgewählte Menü gibt
            if (!empty($_SESSION["id"])){

              $query_check = "SELECT id, admin_id, menu_id FROM admins_menus WHERE menu_id = '".$row["id"]."' AND admin_id = '".$_SESSION["id"]."'";
              $result_check = mysqli_query($conn,$query_check);
              $row_check = mysqli_fetch_array($result_check);

              if (!empty($row_check['admin_id'])){

                $found = $row_check['admin_id'];

              }

              // Falls Admin Level 1 und es keinen Eintrag gibt
              if($_SESSION["admin_level"] == '1' && empty($found)){

                echo '

                <div class="row py-2">
                  <div class="col">
                    <div class="p-3 text-center">Sie haben keine Rechte diese Seite anzusehen.</div>
                  </div>
                </div>';

                echo '</div>
                </div>'; // #content und #container

                include 'inc/footer.php';

                echo '</div>'; // #app

                include 'inc/scripts.php';

                die;

              }

            }else{ // Bitte loggen sie sich ein Text ausgeben

              echo '

              <div class="row py-2">
                <div class="col">
                  <div class="p-3 text-center">Diese Seite steht ihnen nicht zur Verfügung. Bitte <a href="login.php" class="text-dark">loggen sie sich ein</a>.</div>
                </div>
              </div>';

              echo '</div>
              </div>'; // #content und #container

              include 'inc/footer.php';

              echo '</div>'; // #app

              include 'inc/scripts.php';

              die;

            }

         ?>

         <svg xmlns="http://www.w3.org/2000/svg" style="display:none;">
           <symbol id="external-link" viewBox="0 0 16 16">
             <path fill-rule="evenodd" d="M8.636 3.5a.5.5 0 0 0-.5-.5H1.5A1.5 1.5 0 0 0 0 4.5v10A1.5 1.5 0 0 0 1.5 16h10a1.5 1.5 0 0 0 1.5-1.5V7.864a.5.5 0 0 0-1 0V14.5a.5.5 0 0 1-.5.5h-10a.5.5 0 0 1-.5-.5v-10a.5.5 0 0 1 .5-.5h6.636a.5.5 0 0 0 .5-.5z"/>
             <path fill-rule="evenodd" d="M16 .5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h3.793L6.146 9.146a.5.5 0 1 0 .708.708L15 1.707V5.5a.5.5 0 0 0 1 0v-5z"/>
           </symbol>
           <symbol id="expand" viewBox="0 0 16 16">
             <path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
           </symbol>

          <!-- class="bi bi-link-45deg" -->
           <symbol id="internal-link" viewBox="0 0 16 16">
             <path d="M4.715 6.542 3.343 7.914a3 3 0 1 0 4.243 4.243l1.828-1.829A3 3 0 0 0 8.586 5.5L8 6.086a1.002 1.002 0 0 0-.154.199 2 2 0 0 1 .861 3.337L6.88 11.45a2 2 0 1 1-2.83-2.83l.793-.792a4.018 4.018 0 0 1-.128-1.287z"/>
             <path d="M6.586 4.672A3 3 0 0 0 7.414 9.5l.775-.776a2 2 0 0 1-.896-3.346L9.12 3.55a2 2 0 1 1 2.83 2.83l-.793.792c.112.42.155.855.128 1.287l1.372-1.372a3 3 0 1 0-4.243-4.243L6.586 4.672z"/>
           </symbol>
         </svg>

        <!-- Modell -->

        <!--

        p-dnd: Modell für den Drag&Drop
          path: Wird dafür gebraucht, um den Pfad zu einem bestimmten Punkt zu bestimmen,
            z.B: 4-3-5 = Punkt 5 im 3 Level, von Punkt 3 im 2 Level von Punkt 4 im 1 Level
          copy: Variable, die zum vorübergehenden Speichern des DOM-Elementes verwendet wird,
            der während eines Drag eines Menüpunktes erstellt wird
          selected: id des ausgewählten Menüpunktes zum Bearbeiten
          hovered: id des Elements, wo wir unser Drag&Drop einfügen wollen
          level: Menülevel in dem sich der ausgewählte Menüpunkt befindet (1 - Level 1, 2 - Level 2, 3 - Level 3)
          hideLastLevel: Level 3 verstecken, damit wir Level 2 einfacher Drag&Droppen können

        p-state: Zustand der Komponente
          menuActive: Menü öffnen / verstecken, in der mobilen Version
          item1selected: welcher Punkt des 1. Levels ausgewählt ist
          item2selected: welcher Punkt des 2. Levels ausgewählt ist
          timeoutId: wurde benutzt, um bei einem Mouseover über die rechte Seite des 2. Level, nicht direkt den 3. Level anzuzeigen.
                     wir haben uns mit den Betreuern entschieden, den mouseover mit einem click zu ersetzten
          delay: zusammen mit timeoutID, wird nicht mehr benutzt
          showMenuModel: Das Modell des Menüs als Textfeld anzeigen
          showHTMLCode: Den HTML Code des Menüs anzeigen
          id: ID des Menüs in der Datenbank
          lang: Sprache (als default DE)
          editProps: Tabs schalten: Eigenschaften bearbeiten / Content bearbeiten,
          item1edit: id des Menüpunktes im Level 1, der zur Zeit bearbeitet wird
          item2edit: id des Menüpunktes im Level 2, der zur Zeit bearbeitet wird
          item3edit: id des Menüpunktes im Level 3, der zur Zeit bearbeitet wird
          dropdownActive: Auswahl der Benutzer
          showMessage: Toast-Fenster anzeigen/verstecken
          deleteConfirm: Modal-Fenster anzeigen/verstecken
          edited: Falls edited=true, dann geben wir eine Message, dass Änderungen vorgenommen worden sind und verstecken den Test-Button
          query: Benutzer suchen
          menus: Liste der Sprachen des Menüs

        p-menu: enthält alle Menüpunkte und Menüeigenschaften im JSON Format

        -->

        <menu-edit
          inline-template
          :p-dnd="{
            path: [null, null, null],
            copy: null,
            selected: null,
            hovered: null,
            level: null,
            hideLastLevel: false,
          }"
          :p-state="{
            checkingUrls: false,
            menuActive: false,
            item1selected: 0,
            item2selected: null,
            timeoutId: null,
            delay: 500,
            showMenuModel: false,
            showHTMLCode: false,
            id: '<?php echo $menu_id; ?>',
            lang: 'de',
            editProps: true,
            item1edit: 0, <!-- Null by default -->
            item2edit: null,
            item3edit: null,
            dropdownActive: false,
            showMessage: false,
            deleteConfirm: false,
            edited: false,
            slugs: <?php echo $slugs;  ?>,
            curSlug: '',
            query: '',
            menus: [
              {
                key: 'de',
                name: 'Deutsches Menu',
              },
              {
                key: 'en',
                name: 'Englisches Menu',
              },
            ],
          }"
          :p-users="{
            all: [
            <?php // Alle Benutzer aus der DB auswählen
              while ($row_all_users = mysqli_fetch_array($result_all_users)){
                echo "{ id: ".$row_all_users['id'].", name: '".$row_all_users['username']."' },";
              }
            ?>
            ],
            selected: [
            <?php // Ausgewählte Benutzer, die auf das Menü Zugriff haben / den wir den Zugriff erlauben wollen
              while ($row_menu_allow = mysqli_fetch_array($result_menu_allow)){
                echo "{ id: ".$row_menu_allow['id'].", name: '".$row_menu_allow['username']."' },";
              }
            ?>
            ],
          }"
          :p-menu="<?php echo $final_model; ?>"
        >

          <div class="" v-cloak>

            <!-- Content / Eigenschaften bearbeiten und Menü Testen Buttons -->
            <div class="row mt-3">
              <div class="col mb-3">
                <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                  <a
                    class="btn"
                    :class="!st.editProps ? 'btn-outline-primary' : 'btn-primary'"
                    href="#"
                    role="button"
                    @click.prevent="setEditProps(true)"
                  >
                    Eigenschaften bearbeiten
                  </a>
                  <a
                    class="btn"
                    :class="st.editProps ? 'btn-outline-primary' : 'btn-primary'"
                    href="#"
                    role="button"
                    @click.prevent="setEditProps(false)"
                  >
                    Content bearbeiten
                  </a>
                </div>
              </div>
              <div class="col text-end">
                <transition name="fade">
                  <a
                    v-if="!st.edited"
                    :href="'preview.php?slug=' + menu.slug"
                    class="btn btn-outline-dark mb-2"
                  >
                  Jetzt testen
                  </a>
                </transition>
                <a
                  href="#"
                  @click.prevent="saveData('/api/save.php')"
                  class="btn btn-outline-primary mb-2"
                >
                  Menü speichern
                </a>
              </div>
            </div>

            <!-- Eigenschaften bearbeiten anzeigen -->
            <div
              v-show="st.editProps === true"
              class=""
            >

              <?php include 'inc/edit-properties.php';?>

            </div>

            <!-- Content bearbeiten anzeigen -->
            <div
              v-show="st.editProps === false"
              class=""
            >

              <?php include 'inc/edit-content.php';?>

            </div>

            <!-- Menü speichern, testen, entfernen Buttons, Fehler anzeigen, Änderungen vorgenommen Text -->
            <div class="row mt-2 mb-1">

              <div class="col-8">
                <a
                  href="#"
                  @click.prevent="saveData('/api/save.php')"
                  class="btn btn-outline-primary mt-1"
                >
                  Menü speichern
                </a>

                <transition name="fade">
                  <a
                    v-if="!st.edited"
                    :href="'preview.php?slug=' + menu.slug"
                    class="btn btn-outline-dark mt-1"
                  >
                    Jetzt testen
                  </a>
                </transition>
                <div
                  v-if="errors !== null"
                  class="text-danger mt-3"
                >
                  <b>Folgende Fehler sind aufgetreten:</b>
                  <span v-for="err in errors" class="mt-2">
                      {{ err }}.
                  </span>
                </div>
              </div>
              <div class="col text-end">
                <a
                  :href="'delete-menu.php?slug=' + menu.slug"
                  class="btn btn-outline-danger mt-1"
                  @click.prevent="st.deleteConfirm = true"
                >
                  Menü entfernen
                </a>
              </div>
              <transition name="fade">
                <div
                  v-if="st.edited"
                  class="text-danger ms-2 ts mt-1"
                >
                  <small>Änderungen wurden vorgenommen. Speichern sie das Menü, um es zu testen.</small>
                </div>
              </transition>

            </div>

              <div
                class="toast-container position-fixed top-5 bottom-0 start-0 end-0 p-3"
                style="
                  width:100%;
                  display: flex;
                  align-items: center;
                  justify-content: center;
                  z-index: 1;
                "
              >
                <!-- Toast Beginn -->
                <transition name="fade" :duration="{ enter: 500, leave: 800 }">
                  <div
                    v-show="st.showMessage"
                    class="my-toast"
                  >
                    <div class="toast-header">
                      <svg class="bd-placeholder-img rounded me-2" width="20" height="20" xmlns="http://www.w3.org/2000/svg" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"><rect width="100%" height="100%" fill="#198754"></rect></svg>

                      <strong class="me-auto text-success">Das Menü wurde gespeichert</strong>
                      <button
                        class="btn-close"
                        @click="st.showMessage = false"
                      ></button>
                    </div>
                    <div class="toast-body">
                      Die Eigenschaften und der Content des Menüs wurden gespeichert. Sie können es
                      <a
                        class="a-grey"
                        :href="'preview.php?slug=' + menu.slug"
                      >
                        jetzt testen!
                      </a>
                    </div>
                  </div>
                </transition>
                <!-- Toast Ende -->
              </div>

            <!-- Modal Beginn -->
            <transition name="fade">
              <div
                v-if="st.deleteConfirm"
                class="modal modal-overlay"
                @click="st.deleteConfirm = false"
              >
                <div class="modal-dialog" style="display:block;">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">
                        Wollen sie das Menü wirklich entfernen?
                      </h5>
                      <div
                        class="btn-close"
                        @click="st.deleteConfirm = false"
                      ></div>
                    </div>
                    <div class="modal-footer">
                      <button
                        type="button"
                        class="btn btn-secondary"
                        @click.prevent="st.deleteConfirm = false"
                      >
                        Abbrechen
                      </button>
                      <button
                        type="button"
                        class="btn btn-primary"
                        @click.prevent="goto('delete-menu.php?slug=' + menu.slug)"
                      >
                        Entfernen
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </transition>
            <!-- Modal Ende -->

            <div class="row">
              <div class="col mb-2 mt-3">
                <strong>» Live-Vorschau</strong>
              </div>
            </div>

            <!--

            p-state: Zustand der aktuellen Komponente, der von der übergeordneten Komponente vererbt wird
            p-menu: Liste aller Menüpunkte, die von der übergeordneten Komponenten vererbt werden

            -->

            <menu-block
              inline-template
              :p-state="st"
              :p-menu="menu"
            >

            <!-- CSS Styles anwenden, die in den Menüeigenschaften angegeben wurden -->

            <div
              v-cloak
              id="test"
              :style="menu.special_param"
            >

              <?php include('inc/preview-menu-block.php'); ?>

            </div>

            </menu-block>

          </div>

      </menu-edit>

    </div>
  </div>

<?php include 'inc/footer.php';?>

</div> <!-- #app -->

<?php include 'inc/scripts.php';?>

</body>
</html>
