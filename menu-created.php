<?php

session_start();

include('inc/db.php');

if(empty($_SESSION["id"])){
  header("location: login.php");
}

 ?>

<!DOCTYPE html>
<html lang="de">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href="/css/bootstrap.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/bootstrap-icons.css" rel="stylesheet">
  <title>Menü wurde erstellt</title>
  <?php include 'inc/favicons.php'; ?>
</head>
<body>

  <div id="app">

    <?php $title="Das Menü wurde erstellt"; ?>

    <?php include 'inc/header.php'; ?>

    <div class="content">
      <div class="container">

        <!-- Text - Ihr Menü wurde erstellt -->

        <div class="row py-2">
          <div class="col">
            <div class="p-3 text-center">Das Menu

              <?php

                // Den Namen des gerade hinzugefügten Menüs erfragen
                $sql = "SELECT name, slug FROM menus ORDER BY id DESC LIMIT 1";
                $result = mysqli_query($conn,$sql);

              	while ($row = mysqli_fetch_array($result)) {
              		echo '<b>"' . $row['name'] . '"</b>';

              ?>

              wurde erstellt. Sie können es jetzt bearbeiten.</div>

          </div>
        </div>

        <!-- Button - Menu Content bearbeiten -->

        <div class="row text-center mb-5">
          <div class="col">
            <a
                href="edit-menu.php?slug=<?php echo $row['slug'];}?>&content=true"
                class="btn btn-outline-primary"
              >
                Menü Content bearbeiten
              </a>
          </div>
        </div>

      </div>
    </div>

    <?php include 'inc/footer.php';?>

  </div> <!-- #app -->

<?php include 'inc/scripts.php';?>

</body>
</html>
