<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href="http://127.0.0.1:8000/css/default.min.css" rel="stylesheet">
  <link href="http://127.0.0.1:8000/css/bootstrap.css" rel="stylesheet">
  <link href="http://127.0.0.1:8000/css/bootstrap-icons.css" rel="stylesheet">
  <link href="http://127.0.0.1:8000/css/style.css" rel="stylesheet">
  <title>Menü-Vorschau - super-menu</title>
  <style>[v-cloak]{display:none!important;}</style>
  <link rel="apple-touch-icon" sizes="180x180" href="http://127.0.0.1:8000/images/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="http://127.0.0.1:8000/images/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="http://127.0.0.1:8000/images/favicon-16x16.png">
  <link rel="manifest" href="http://127.0.0.1:8000/images/site.webmanifest">
  <link rel="mask-icon" href="http://127.0.0.1:8000/images/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="http://127.0.0.1:8000/images/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="http://127.0.0.1:8000/images/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
</head>
<body>


  <div id="app">




    <div class="content">
      <div class="container">


        <svg xmlns="http://www.w3.org/2000/svg" style="display:none;">
          <symbol id="external-link" viewBox="0 0 16 16">
            <path fill-rule="evenodd" d="M8.636 3.5a.5.5 0 0 0-.5-.5H1.5A1.5 1.5 0 0 0 0 4.5v10A1.5 1.5 0 0 0 1.5 16h10a1.5 1.5 0 0 0 1.5-1.5V7.864a.5.5 0 0 0-1 0V14.5a.5.5 0 0 1-.5.5h-10a.5.5 0 0 1-.5-.5v-10a.5.5 0 0 1 .5-.5h6.636a.5.5 0 0 0 .5-.5z"/>
            <path fill-rule="evenodd" d="M16 .5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h3.793L6.146 9.146a.5.5 0 1 0 .708.708L15 1.707V5.5a.5.5 0 0 0 1 0v-5z"/>
          </symbol>
          <symbol id="expand" viewBox="0 0 16 16">
            <path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
          </symbol>
        </svg>


        <menu-block
          inline-template
          :p-state="{
            menuActive: false,
            lang: 'en',
            item1selected: 0,
            item2selected: null,
            timeoutId: null,
            delay: 500,
            showMenuModel: false,
            showHTMLCode: false,
            menus: [
              {
                key: 'de',
                name: 'Deutsches Menu',
              },
              {
                key: 'en',
                name: 'Englisches Menu',
              },
            ],
          }"
          :p-menu="{'id':'154','slug':'super-menu','name':'Super Menü','bg_1':'#00bcd4','bg_2':'','bg_3':'whitesmoke','font_size':'15px','font_color':'black','special_param':'123','list':{'de':[{'name':'Cooler Punkt Neu','subtitle':'Beschreibung Level 1 International','url':'','style':'','color':'','font_weight':'','bg':'','list':[{'name':'2. Level Punkt','subtitle':'Überschrift Lvl 3 (im 2. Lvl bearbeiten)','desc':'Beschreibung...','color':'','font_weight':'','bg':'','url':'','list':[]}]},{'name':'International','subtitle':'Fliegen sie ins Ausland heutea','url':'/en/main/second/int','style':'','color':'white','font_weight':'','bg':'','list':[{'name':'USA ist interessant','subtitle':'USA 3 Level','desc':'Kurzbeschreibung von USA','style':'','color':'','font_weight':'','bg':'','url':'http://google.com','list':[]},{'name':'Paris und heute','subtitle':'Paris ist gestern','desc':'Kurzbeschreibung aus Paris','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/pruefung','list':[]},{'name':'Costa Rica ganz neu','subtitle':'3 Level Title Costa','desc':'Kurzbeschreibung Costaaa','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge','list':[]},{'name':'Hanover ist bereit','subtitle':'Alles über Hanover','desc':'Kurzbeschreibung aus Hanover','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/pruefung','list':[{'name':'Afrika','style':'','color':'','font_weight':'','bg':'','url':'international/andere/japan'},{'name':'Amerika','style':'','color':'','font_weight':'','bg':'','url':'international/andere/neuseeland'},{'name':'Asien','style':'','color':'','font_weight':'','bg':'','url':'international/andere/asien'},{'name':'Kanada','style':'','color':'','font_weight':'','bg':'','url':'international/andere/kanada'},{'name':'Lettland','style':'','color':'','font_weight':'','bg':'','url':'international/andere/russland'},{'name':'Europa','style':'','color':'','font_weight':'','bg':'','url':'international/andere/china'},{'name':'Die Türkei','style':'','color':'','font_weight':'','bg':'','url':'international/andere/beste'}]},{'name':'Kanada ist schön','subtitle':'Kanada 3 Level','desc':'Kurzbeschreibung von Kanada','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/aktuelles','list':[]},{'name':'Andere Länder sind da','subtitle':'Übersicht der anderen Länder','desc':'Kurzbeschreibung der Länder','style':'','color':'','font_weight':'','bg':'','url':'https://fakultaet/studiengaenge','list':[{'name':'Italien','style':'','color':'','font_weight':'','bg':'','url':'international/andere/moldawien'},{'name':'Japan','style':'','color':'','font_weight':'','bg':'','url':'international/andere/japan'},{'name':'China','style':'','color':'','font_weight':'','bg':'','url':'international/andere/china'},{'name':'Australien','style':'','color':'','font_weight':'','bg':'','url':'international/andere/australien'},{'name':'Neuseeland','style':'','color':'','font_weight':'','bg':'','url':'international/andere/neuseeland'},{'name':'Russland','style':'','color':'','font_weight':'','bg':'','url':'international/andere/russland'},{'name':'Das beste Land der Welt','style':'','color':'','font_weight':'','bg':'','url':'international/andere/beste'}]},{'name':'Hamburg und weiter','subtitle':'Alles über Hamburg','desc':'Kurzbeschreibung aus HH','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/international','list':[]}]},{'name':'Studierende','subtitle':'Beschreibung Level 1 Studierende','url':'studierende','style':'','color':'','font_weight':'','bg':'','list':[]},{'name':'Super Fak','subtitle':'Wirtschaftswissenschaftliche Fakultät','url':'fakultaet','style':'','color':'','font_weight':'','bg':'','list':[{'name':'Aktuelles','subtitle':'3 Level Aktuelles','desc':'Kurzbeschreibung des Punktes Aktuelles','style':'','color':'','font_weight':'','bg':'','url':'','list':[]},{'name':'Forschung','subtitle':'3 Level Title Forschung','desc':'Kurzbeschreibung des Punktes Forschung','style':'','color':'','font_weight':'','bg':'','url':'','list':[]},{'name':'Professorinen','subtitle':'3 Level Professorinen','desc':'Kurzbeschreibung des Punktes Professorinen','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/professorinen','list':[]},{'name':'Studiengänge','subtitle':'Übersicht der Studiengänge','desc':'Kurzbeschreibung des Punktes Studiengänge','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge','list':[{'name':'Betriebswirtschaftslehre','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge/bwl'},{'name':'Volkswirtschaftslehre','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge/vwl'},{'name':'Wirtschaftsinformatik','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge/wi'},{'name':'Wirtschaftspädagogik','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge/wp'},{'name':'Promotionsstudiengang Wirtschaftswissenschaften','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge/pws'},{'name':'History of Global Markets','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge/hgm'},{'name':'Master of Science in Information Systems','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge/msis'}]},{'name':'International','subtitle':'Alles über Inernational','desc':'Kurzbeschreibung des Punktes International','style':'','color':'','font_weight':'','bg':'','url':'','list':[{'name':'Test 1','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge/bwl'},{'name':'Test 2','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge/vwl'},{'name':'Test 3','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge/wi'},{'name':'Test 4','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge/wp'},{'name':'Test 5','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge/hgm'},{'name':'Test 6','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge/msis'},{'name':'Test 7','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge/pws'}]},{'name':'Prüfungs und Studienordnungen','subtitle':'Alles über PFSO','desc':'Kurzbeschreibung des Punktes PFSO','style':'','color':'','font_weight':'','bg':'','url':'','list':[]},{'name':'Karriere und Alumni','subtitle':'Alles über Karriere','desc':'Kurzbeschreibung des Punktes Karriere','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/pruefung','list':[]},{'name':'Dekanat der Fakultät','subtitle':'Alles über den Dekanat','desc':'Kurzbeschreibung des Punktes Dekanat','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/pruefung','list':[]}]},{'name':'Studieninteressierte','subtitle':'Beschreibung Level 1 Studierende','url':'studierende','style':'','color':'','font_weight':'','bg':'','list':[]},{'name':'Die Universität','subtitle':'Übersicht der Universität','url':'uni','style':'','color':'','font_weight':'','bg':'','list':[]},{'name':'Weitere Punkte','subtitle':'Beschreibung Level 1 weiteres','url':'weiteres','style':'','color':'','font_weight':'','bg':'','list':[]},{'name':'Bewerbung','subtitle':'Beschreibung Level 1 Bewerbung','url':'','style':'','color':'','font_weight':'','bg':'','list':[]}],'en':[{'name':'International EN','subtitle':'Fliegen sie ins Ausland heute','url':'/en/main/second/int','style':'','color':'white','font_weight':'700','bg':'red','list':[{'name':'USA ist interessant','subtitle':'USA 3 Level','desc':'Kurzbeschreibung von USA','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/aktuelles','list':[]},{'name':'Paris und heute','subtitle':'Paris ist gestern','desc':'Kurzbeschreibung aus Paris','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/pruefung','list':[]},{'name':'Costa Rica ganz neu','subtitle':'3 Level Title Costa','desc':'Kurzbeschreibung Costaaa','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge','list':[]},{'name':'Hanover ist bereit','subtitle':'Alles über Hanover','desc':'Kurzbeschreibung aus Hanover','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/pruefung','list':[{'name':'Afrika','style':'','color':'','font_weight':'','bg':'','url':'international/andere/japan'},{'name':'Amerika','style':'','color':'','font_weight':'','bg':'','url':'international/andere/neuseeland'},{'name':'Asien','style':'','color':'','font_weight':'','bg':'','url':'international/andere/asien'},{'name':'Kanada','style':'','color':'','font_weight':'','bg':'','url':'international/andere/kanada'},{'name':'Lettland','style':'','color':'','font_weight':'','bg':'','url':'international/andere/russland'},{'name':'Europa','style':'','color':'','font_weight':'','bg':'','url':'international/andere/china'},{'name':'Die Türkei','style':'','color':'','font_weight':'','bg':'','url':'international/andere/beste'}]},{'name':'Kanada ist schön','subtitle':'Kanada 3 Level','desc':'Kurzbeschreibung von Kanada','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/aktuelles','list':[]},{'name':'Andere Länder sind da','subtitle':'Übersicht der anderen Länder','desc':'Kurzbeschreibung der Länder','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge','list':[{'name':'Italien','style':'','color':'','font_weight':'','bg':'','url':'international/andere/moldawien'},{'name':'Japan','style':'','color':'','font_weight':'','bg':'','url':'international/andere/japan'},{'name':'China','style':'','color':'','font_weight':'','bg':'','url':'international/andere/china'},{'name':'Australien','style':'','color':'','font_weight':'','bg':'','url':'international/andere/australien'},{'name':'Neuseeland','style':'','color':'','font_weight':'','bg':'','url':'international/andere/neuseeland'},{'name':'Russland','style':'','color':'','font_weight':'','bg':'','url':'international/andere/russland'},{'name':'Das beste Land der Welt','style':'','color':'','font_weight':'','bg':'','url':'international/andere/beste'}]},{'name':'Hamburg und weiter','subtitle':'Alles über Hamburg','desc':'Kurzbeschreibung aus HH','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/international','list':[]}]},{'name':'Cooler Punkt EN','subtitle':'Beschreibung Level 1 International','url':'','style':'','color':'','font_weight':'','bg':'','list':[{'name':'2. Level Punkt','subtitle':'Überschrift Lvl 3 (im 2. Lvl bearbeiten)','desc':'Beschreibung...','color':'','font_weight':'','bg':'','url':'','list':[]}]},{'name':'Studierende EN','subtitle':'Beschreibung Level 1 Studierende','url':'studierende','style':'','color':'','font_weight':'','bg':'','list':[]},{'name':'Super Fak EN','subtitle':'Wirtschaftswissenschaftliche Fakultät','url':'fakultaet','style':'','color':'','font_weight':'','bg':'','list':[{'name':'Aktuelles','subtitle':'3 Level Aktuelles','desc':'Kurzbeschreibung des Punktes Aktuelles','style':'','color':'','font_weight':'','bg':'','url':'','list':[]},{'name':'Forschung','subtitle':'3 Level Title Forschung','desc':'Kurzbeschreibung des Punktes Forschung','style':'','color':'','font_weight':'','bg':'','url':'','list':[]},{'name':'Professorinen','subtitle':'3 Level Professorinen','desc':'Kurzbeschreibung des Punktes Professorinen','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/professorinen','list':[]},{'name':'Studiengänge','subtitle':'Übersicht der Studiengänge','desc':'Kurzbeschreibung des Punktes Studiengänge','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge','list':[{'name':'Betriebswirtschaftslehre','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge/bwl'},{'name':'Volkswirtschaftslehre','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge/vwl'},{'name':'Wirtschaftsinformatik','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge/wi'},{'name':'Wirtschaftspädagogik','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge/wp'},{'name':'Promotionsstudiengang Wirtschaftswissenschaften','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge/pws'},{'name':'History of Global Markets','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge/hgm'},{'name':'Master of Science in Information Systems','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge/msis'}]},{'name':'International','subtitle':'Alles über Inernational','desc':'Kurzbeschreibung des Punktes International','style':'','color':'','font_weight':'','bg':'','url':'','list':[{'name':'Test 1','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge/bwl'},{'name':'Test 2','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge/vwl'},{'name':'Test 3','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge/wi'},{'name':'Test 4','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge/wp'},{'name':'Test 5','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge/hgm'},{'name':'Test 6','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge/msis'},{'name':'Test 7','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/studiengaenge/pws'}]},{'name':'Prüfungs und Studienordnungen','subtitle':'Alles über PFSO','desc':'Kurzbeschreibung des Punktes PFSO','style':'','color':'','font_weight':'','bg':'','url':'','list':[]},{'name':'Karriere und Alumni','subtitle':'Alles über Karriere','desc':'Kurzbeschreibung des Punktes Karriere','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/pruefung','list':[]},{'name':'Dekanat der Fakultät','subtitle':'Alles über den Dekanat','desc':'Kurzbeschreibung des Punktes Dekanat','style':'','color':'','font_weight':'','bg':'','url':'fakultaet/pruefung','list':[]}]},{'name':'Studieninteressierte','subtitle':'Beschreibung Level 1 Studierende','url':'studierende','style':'','color':'','font_weight':'','bg':'','list':[]},{'name':'Die Universität EN','subtitle':'Übersicht der Universität','url':'uni','style':'','color':'','font_weight':'','bg':'','list':[]},{'name':'Weitere Punkte','subtitle':'Beschreibung Level 1 weiteres','url':'weiteres','style':'','color':'','font_weight':'','bg':'','list':[]},{'name':'Bewerbung','subtitle':'Beschreibung Level 1 Bewerbung','url':'','style':'','color':'','font_weight':'','bg':'','list':[]}]}}"
        >

          <div
            v-cloak
            id="test"
            :style="menu.special_param"
          >

            <div class="row">


            </div>

            <!-- Beginn 1 Level Punkte -->

<div class="row">
  <div class="col mx-1">
    <nav
      class="navbar navbar-expand-lg navbar-dark"
      :style="{ backgroundColor: cssBg1Color }"
    >
      <div class="container-fluid">
        <button
          class="navbar-toggler"
          type="button"
          @click="menuToggle"
        >
          <span class="navbar-toggler-icon"></span>
        </button>
        <div
          class="navbar-collapse"
          :class="st.menuActive ? 'show' : 'collapse'"
        >
          <!-- id="navbarSupportedContent" -->
          <ul class="navbar-nav me-auto mb-2 mb-lg-0 flex-wrap top-space">
            <li
              v-for="(item_1,itemInd) in menu.list[st.lang]"
              class="nav-item level_1 nowrap"
              :class="{ active: st.item1selected === itemInd }"
              :style="{
                backgroundColor: item_1.bg,
                fontWeight: item_1.font_weight
              }"
            >
              <a
                class="nav-link"
                aria-current="page"
                :href="item_1.url"
                :style="{
                  color: item_1.color,
                  fontSize: menu.font_size
                }"
                style="
                  display:flex;
                  alignt-items:center;
                "
                @mouseover="selectFirstLevel(itemInd)"
                @click.prevent="selectFirstLevel(itemInd,item_1)"
              >
                {{ item_1.name }}
                <div v-if="item_1.list.length">
                  <svg
                    viewBox="0 0 16 16"
                    height="12"
                    width="12"
                    fill="currentColor"
                    class="ico-exp-mob"
                    :class="{ active: st.item1selected === itemInd }"
                  >
                    <use href="#expand"/>
                  </svg>

                </div>

                <span class="ico-ext-link-mob">
                  <svg
                    v-if="item_1.url"
                    viewBox="0 0 16 16"
                    height="12"
                    width="12"
                    fill="currentColor"
                  >
                    <use href="#external-link"/>
                  </svg>
                </span>
              </a>
              <ul
                v-if="itemInd === st.item1selected"
                class="menu_mob_2"
              >
              <!-- :style="{ borderColor: cssBg1Color }"
              style="
              border-style: solid;
              border-width: 1px 0 0 0;
              /* top right bottom left */
              " -->
                <li v-for="(row,rowInd) in selectedMenuItem.list">
                  <a
                    class="level_2_mob"
                    :href="row.url"
                    @click.prevent.stop="selectSecondLevel(rowInd, false, row)"
                    style="
                      display:flex;
                      white-space:normal;
                    "
                  >
                    {{ row.name }}
                    <span v-if="row.list.length">

                      <svg
                        viewBox="0 0 16 16"
                        height="12"
                        width="12"
                        fill="currentColor"
                        class="ico-exp-mob-2"
                        :class="{ active: st.item2selected === rowInd }"
                      >
                        <use href="#expand"/>
                      </svg>
                    </span>
                    <span
                      @click.stop="goto(row.url)"
                      style="
                        margin-left:auto;
                        padding-left:15px;
                      "
                    >
                      <svg
                        viewBox="0 0 16 16"
                        height="12"
                        width="12"
                        fill="currentColor"
                        class="ico-ext-link-mob-2"
                      >
                        <use href="#external-link"/>
                      </svg>
                    </span>
                  </a>

                  <div
                    class="mob-bg"
                    v-if="rowInd === st.item2selected"
                    data-type="mobile 2nd level"
                  >
                    <!-- style="
                    border-style: solid;
                    border-width: 1px 0 0 0;
                    " -->
                    <!-- :style="{
                      borderColor: row.list.length ? menu.bg_1 : 'transparent'
                    }" -->
                    <div
                      v-if="row.list.length"
                      class="menu_mob_3_descr"
                    >
                      {{ row.subtitle }}
                    </div>
                    <ul
                      class="menu_mob_3"
                    >
                      <li v-for="item in row.list">
                        <a
                          class="level_3_mob"
                          :href="item.url"
                          style="display:flex;"
                        >
                          {{ item.name }}
                          <span
                            style="
                              margin-left:auto;
                              padding-left:15px;
                            "
                          >
                            <svg
                              viewBox="0 0 16 16"
                              height="12"
                              width="12"
                              fill="currentColor"
                              class="ico-ext-link-mob-3"
                            >
                              <use href="#external-link"/>
                            </svg>
                          </span>
                        </a>
                      </li>
                    </ul>
                  </div>

                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </div>
</div>

<!-- Ende 1 Level Punkte -->

<!-- selectedMenuItem -->
<div
  class="row menu_border mx-1 justify-content-between submenu"
  :style="{ backgroundColor: menu.bg_2 }"
>

  <!-- Beginn 2 Level Punkte -->
  <div class="col-12 col-lg-8 col-md-12 col-sm-12 my-2 mb-4">
    <div class="level_2_big ms-3">
     <a
      :href="selectedMenuItem.url"
      :style="{
        color: menu.font_color,
        fontSize: menu.font_size
      }"
    >
      {{ selectedMenuItem.subtitle }}
    </a>
    </div>

    <div
      v-for="(row,rowInd) in secondLevel"
      class="row mb-1"
    >

      <div
        v-for="(item_2,itemInd) in row"
        class="col ms-3 mt-3"
      >
        <div
          v-if="item_2"
          class="vl"
          :class="{ active: st.item2selected === rowInd * 2 + itemInd }"
        >
          <a
            :href="item_2.url"
            class="level_2_link"
            :class="{ 'cursor-default': !item_2.url }"
            :style="{ fontSize: menu.font_size }"
            @mouseover="selectSecondLevel(rowInd * 2 + itemInd, true)"
            @mouseleave="unselectSecondLevel(rowInd * 2 + itemInd)"
          >
            <div
              class="level_2_title ms-2"
              :style="{
                color: item_2.color || menu.font_color,
                backgroundColor: item_2.bg,
                fontWeight: item_2.font_weight
              }"
            >
              {{ item_2.name }}

              <svg
                v-if="item_2.url"
                viewBox="0 0 16 16"
                height="12"
                width="12"
                fill="currentColor"
                class="ico-ext-link"
              >
                <use href="#external-link"/>
              </svg>
            </div>
            <div
              class="level_2_desc ms-2"
              :style="{ color: item_2.color || menu.font_color }"
            >
              {{ item_2.desc }}
            </div>
          </a>
        </div>
      </div>
    </div>

  </div>
  <!-- Ende 2 Level Punkte -->

  <!-- Beginn 3 Level Punkte -->
  <div
    v-if="selectedSubmenuItem"
    class="col-12 col-lg-4 col-md-12 col-sm-12"
    :style="{
      backgroundColor: menu.bg_3,
      fontSize: menu.font_size
    }"
  >
    <div class="mt-2">
      <div
        class="level_3_big ms-1"
      >
        <a
          :href="selectedSubmenuItem.url"
          :style="{ color: menu.font_color }"
          style="font-size:1em;"
        >
          {{ selectedSubmenuItem.subtitle }}
        </a>
      </div>
    </div>
    <div
      v-for="item_3 in selectedSubmenuItem.list"
      class="ms-3 mt-2"
    >
      <a
        class="level_3_title"
        :href="item_3.url"
        :style="{
          color: item_3.color || menu.font_color,
          backgroundColor: item_3.bg,
          fontWeight: item_3.font_weight
        }"
        style="font-size:.92em;"
      >
        {{ item_3.name }}
      </a>
    </div>
    <br>
    <!-- <div class="ms-3 mt-2"></div> -->
  </div>
  <!-- Ende 3 Level Punkte -->

</div>



<div class="row">
  <div class="col mb-4">

  </div>
</div>

          </div>

        </menu-block>

      </div>
    </div>


  </div>

    <!-- SCRIPTS -->

  <script src="http://127.0.0.1:8000/js/highlight.min.js"></script>
  <script src="http://127.0.0.1:8000/js/axios.min.js"></script>

  <script src="http://127.0.0.1:8000/js/vue.js"></script>
  <script src="http://127.0.0.1:8000/js/popper.min.js"></script>
  <script src="http://127.0.0.1:8000/js/bootstrap.bundle.js"></script>
  <script src="http://127.0.0.1:8000/js/app.js"></script>

  <script src="http://127.0.0.1:8000/js/jquery-3.5.1.min.js"></script>

</body>
</html>
