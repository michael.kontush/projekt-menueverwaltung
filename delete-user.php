<?php

session_start();

include 'inc/db.php';

// ID aus der URL bekommen und Benutzer löschen 
$sql = "DELETE FROM admins WHERE id='". $_GET["id"] ."'";
if (mysqli_query($conn, $sql)) {
    $user_deleted = "Wir leiten Sie zur Benutzerverwaltung weiter...";
} else {
    $user_deleted = "Es ist ein Fehler aufgetreten.";
}

?>

<!DOCTYPE html>
<html lang="de">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href="/css/bootstrap.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/bootstrap-icons.css" rel="stylesheet">
  <title>Benutzer wurde gelöscht</title>
  <?php include 'inc/favicons.php'; ?>
</head>
<body>

  <div id="app">

    <?php $title="Der Benutzer wurde gelöscht"; ?>

    <?php include 'inc/header.php'; ?>

    <div class="content">
      <div class="container">

        <!-- Text und weiterleiten zur Startseite -->
        <div class="row gx-10 py-2">
          <div class="col">
            <div class="p-3 text-center text-danger fw-bold"><?php echo $user_deleted; header("refresh:5;url=users.php");?></div>
          </div>
        </div>

      </div>
    </div>

    <?php include 'inc/footer.php';?>

  </div> <!-- #app -->

<?php include 'inc/scripts.php';?>
</body>
</html>
