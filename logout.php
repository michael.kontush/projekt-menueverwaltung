<?php
// Session initialisieren
session_start();

// Alle Sitzungsvariablen zurücksetzten
$_SESSION = array();

// Session löschen
session_destroy();

// Weiterleiten zur Login Seite
header("location: login.php");
exit;
?>
