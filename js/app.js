///// Form Validation und Bearbeitung /////
Vue.component('formValidation', {
  props: ['p-state','p-errors'],
  data: function(){
    return {
      st: this.pState,
      errors: this.pErrors,
    }
  },
  methods: {
    setMenuId(menuId){
      // MenuId aus der DropDown Liste auswählen
      // console.log('%c setMenuId', 'color:gold;', menuId)
      this.st.menuId = menuId
      // console.log('this.st.menuId:', this.st.menuId)
    },
    async submit(url){
      // Validation der Form, um die Daten anschließend an den Server zu senden
      // console.log('%c submit', 'font-size:36px;')

      // Die Länge des Usernames prüfen
      if(this.st.username.length < 6 || this.st.username.length > 20){
        this.st.showResult = 'error'
        this.st.response.title = 'Ein Fehler ist aufgetreten'
        this.st.response.message = 'Der Benutzername muss zwischen 6 und 20 Zeichen lang sein.'
        setTimeout(() => {
          this.st.showResult = ''
        }, 3000)

        return
      }

      // Prüfen, ob die Passwörter übereinstimmen
      if(this.st.pass !== this.st.confpass){
        this.st.showResult = 'error'
        this.st.response.title = 'Ein Fehler ist aufgetreten'
        this.st.response.message = 'Die Passwörter stimmen nicht über ein.'
        setTimeout(() => {
          this.st.showResult = ''
        }, 3000)

        return
      }

      // Die Länge des Passworts prüfen
      if(this.st.pass.length < 6 || this.st.pass.length > 20){
        this.st.showResult = 'error'
        this.st.response.title = 'Ein Fehler ist aufgetreten'
        this.st.response.message = 'Das Passwort muss zwischen 6 und 20 Zeichen lang sein.'
        setTimeout(() => {
          this.st.showResult = ''
        }, 3000)

        return
      }

      if(this.st.superadmin && this.st.menuId === 0){
        // Wenn Superadmin ausgewählt worden ist und kein Menü ausgewählt ist,
        // müssen wir eine Fehlermeldung anzeigen
        this.st.showResult = 'choseMenu'
        this.st.response.title = 'Ein Fehler ist aufgetreten'
        this.st.response.message = 'Um die Benutzerdaten speichern zu können, müssen sie mindestens ein Menü auswählen oder den Admin als Superadmin einstufen.'
        setTimeout(() => {
          this.st.showResult = ''
        }, 3000)

        return
      }

      // Daten vorbereiten
      const data = {
        username: this.st.username,
        pass: this.st.pass,
        confpass: this.st.confpass,

        superadmin: this.st.superadmin,
        menuId: this.st.superadmin ? this.st.menuId : 0,
      }

      // console.log('%c saveData(%s)', 'color:magenta;font-size:36px;', url, JSON.stringify(data));

      // Daten an den Server senden
      const result = await axios.post(url, data)

      // console.log('RESULT:', result)
      if(result.data.status === 'ok') {
        // Alles Okay
        this.st.showResult = 'success'
        setTimeout(() => {
          window.location.reload()
        }, 2000)
      }
      if(result.data.status === 'error') {
        // Fehler
        this.st.showResult = 'error'
      }

      // Title und Nachricht für den Toast
      this.st.response.title = result.data.title
      this.st.response.message = result.data.message

      // console.log('%c RESULT: %s', 'color:gold;', JSON.stringify(result.data, null, 4))

      setTimeout(() => {
        this.st.showResult = ''
      }, 3000)

    },
  },
})


///// Liste der Benutzer /////
Vue.component('usersList', {
  props: ['p-state', 'p-users'],
  data: function(){
    return {
      st: this.pState,
      users: this.pUsers,
    }
  },
  methods: {

    // Modal-Fenster der Löschbestätigung anzeigen
    showDeleteConfirm(id){
      // console.log('showDeleteConfirm', id)
      this.st.deleteConfirm = true; // Modal-Fenster anzeigen
      this.st.userId = id; // Die zu löschende Benutzer-ID angeben
    },

    // Falls wir auf Löschen im Modal-Fenster klicken
    deleteUser(){
      this.st.deleteConfirm = false;

      if (this.st.userId !== null) {
        window.location = 'delete-user.php?id=' + this.st.userId
      }
    },
  },
})


///// Menüs dem Benutzer freigeben /////
Vue.component('dropdownList', {
  props: ['p-state', 'p-list'],
  data: function(){
    return {
      st: this.pState,
      list: this.pList,
    }
  },
  computed: {

    // Liste, die wir ausgeben wollen, unter berücksichtigung der Suchanfrage
    avList(){
      const query = this.st.query.trim()
      if (query.length === 0) return this.list.slice(0,5);

      return this.list.filter(item => {
        return item.name.toLowerCase().includes(query.toLowerCase())
      })
    },
  },
  methods: {

    // ELement aus der DropDown Liste auswählen
    selectItem(id){
      // console.log('%c selectItem(%s)', 'color:lime;', id)
      this.st.dropdownActive = false
      const index = this.list.findIndex(item => item.id === id)
      this.st.selected = index < 0 ? null : index
      this.st.query = ''
      // Das Ereignis mit unseren Daten (hier mit der ID) auf die übergeordnete Komponente senden
      this.$emit('update-menu-id', id)
    },
  },
})


///// Benutzer-Bearbeitung /////
Vue.component('userEdit', {
  props: ['p-data', 'p-state'],
  data: function(){
    return {
      st: this.pState,
      data: this.pData,
    }
  },
  computed: {

    // Alle Menüs in der DropDown Liste anzeigen, die nicht ausgwählt worden sind
    avMenus(){
      // console.log('%c %s', 'color:gold;', JSON.stringify(this.data, null, 4))
      return this.data.menus.all.filter(menu => {
        return !this.data.menus.selected.find(item => {
          return menu.id === item.id
        })
      })
    },

    // Aus der Liste avMenus, nur die Menüs anzeigen, die der Suchanfrage entsprechen
    fMenus(){
      if(this.st.query.trim() === '') return this.avMenus.map((item,index) => {
        return {
          ...item,
          active: index < 5
        }
      })

      const query = this.st.query.trim().toLowerCase()
      return this.avMenus.map(item => {
        const name = item.name.toLowerCase()
        return {
          ...item,
          active: name.includes(query),
        }
      })
    },

    // Falls die Suchanfrage keine Ergebnisse anzeigt
    isEmptyFMenus(){
      return !this.fMenus.find(item => item.active)
    },
  },
  methods: {
    // Zur URL rübergehen
    goto(url){
      window.location = url;
    },

    // Menü hinzufügen
    addMenu(menu){
      console.log('%c addMenu(%s)', 'color:lime;', JSON.stringify(menu))
      this.st.dropdownActive = false
      this.data.menus.selected.push(menu)
      this.st.query = ''
    },

    // Menü entfernen
    removeMenu(menuInd){
      console.log('%c removeMenu(%s)', 'color:orangered;', menuInd)
      this.data.menus.selected.splice(menuInd,1)
    },

    // Daten an den Server senden
    async saveData(url){
      if(this.st.superadmin && this.data.menus.selected.length === 0){
        // Wenn Superadmin ausgewählt worden ist und kein Menü ausgewählt ist,
        // müssen wir eine Fehlermeldung anzeigen
        this.st.showResult = 'choseMenu'
        this.st.response.title = 'Ein Fehler ist aufgetreten'
        this.st.response.message = 'Um die Benutzerdaten speichern zu können, müssen sie mindestens ein Menü auswählen oder den Admin als Superadmin einstufen.'
        setTimeout(() => {
          this.st.showResult = ''
        }, 3000)

        return
      }

      // Daten vorbereiten
      const data = {
        id: this.data.id,
        superadmin: this.st.superadmin,
        menus: this.st.superadmin ? this.data.menus.selected.map(item => item.id) : [],
        pass: this.st.pass,
        confpass: this.st.confpass,
      }

      // console.log('%c saveData(%s)', 'color:magenta;font-size:36px;', url, JSON.stringify(data));

      // Daten an den Server senden
      const result = await axios.post(url, data)

      if(result.data.status === 'ok') {
        // Alles Okay
        this.st.showResult = 'success'
      }
      if(result.data.status === 'error') {
        // Fehler
        this.st.showResult = 'error'
      }

      // Title und Nachricht für den Toast
      this.st.response.title = result.data.title
      this.st.response.message = result.data.message

      // console.log('%c RESULT: %s', 'color:gold;', JSON.stringify(result.data, null, 4))

      setTimeout(() => {
        this.st.showResult = ''
      }, 3000)
    },
  },

})


///// Menü-Suche /////
Vue.component('searchMenus', {
  props: ['p-state','p-menus'],
  data: function(){
    return {
      st: this.pState,
      menus: this.pMenus,
    }
  },
  computed: {
    // Liste der Menüs, die einen "active" Flag haben
    // wird gebraucht, damit die transition richtig funktioniert
    activeMenus(){
      return this.avMenus.filter(item => item.active).length
    },

    // Falls nichts ausgewählt worden ist, dann 5 Menüs anzeigen
    // Falls es eine Suchanfrage gibt, "active" den Menüs zuweisen, die der Suchanfrage entsprchen
    avMenus(){
      if(this.st.query === '') return this.menus.map((item,index) => {
        return {
          ...item,
          active: index < 5
        }
      })

      const query = this.st.query.trim().toLowerCase()
      return this.menus.map(item => {
        const slug = item.slug.toLowerCase()
        const name = item.name.toLowerCase()
        return {
          ...item,
          active: slug.includes(query) || name.includes(query),
        }
      })
    },
  },
  methods: {
    goto(url){
      window.location = url
    },

    // Modal-Fenster anzeigen, die die Entfernung des Menüs bestätigt
    removeMenu(slug){
      this.st.menuSlug = slug
      this.st.deleteConfirm = true
    },
  },
  mounted(){
    // console.log('%c', 'color:aqua;', JSON.stringify(this.menus, null, 4))
  },
})


///// Menü erstellen und bearbeiten  /////
Vue.component('menuEdit', {
  props: ['p-menu','p-state','p-dnd','p-users'], // p-dnd = Drag&Drop
  data: function(){
    return {
      menu: this.pMenu,
      st: this.pState,
      dnd: this.pDnd,
      users: this.pUsers,
      valid: false,
      errors: null,
      fields: [],
      tmpList: null,
    }
  },
  computed: {

    // Flag, um den Button "Unterpunkte hinzufügen" anzuzeigen
    showAddSubmenu(){
      if(this.st.item1edit === null) return false;
      return this.menu.list[this.st.lang][this.st.item1edit].list.length === 0
    },

    // Flag, der bestimmt, ob wir die 2. und 3. Level Punkte anzeigen sollen
    isSubmenu(){
      if(this.st.item1edit === null) return false;
      return this.menu.list[this.st.lang][this.st.item1edit].list.length > 0
    },

    // Benutzer, die nicht ausgewählt sind
    avUsers(){
      return this.users.all.filter(user => {
        return !this.users.selected.find(item => {
          return user.id === item.id
        })
      })
    },

    // Benutzer, die nach der  Suchanfrage nicht ausgewählt worden sind
    fUsers(){
      if(this.st.query === '') return this.avUsers.map((item,index) => {
        return {
          ...item,
          active: index < 5
        }
      })

      const query = this.st.query.trim().toLowerCase()
      return this.avUsers.map(item => {
        const name = item.name.toLowerCase()
        return {
          ...item,
          active: name.includes(query),
        }
      })
    },

    // Flag, der angibt, das bei der Suchanfrage nichts gefunden wurde
    isEmptyFUsers(){
      return !this.fUsers.find(item => item.active)
    },
  },

  methods: {

    // Links prüfen
    async checkMenuUrls(){
      if (!this.st.lang) return

      this.st.checkingUrls = true

      console.log('%c checkMenuUrls()', 'color:lime;font-size:36px;')
      // Alle Menüebenen rekursiv durchlaufen

      for(const lang of ['de', 'en']){
        // 1. Level
        // for(const item1 of this.menu.list[this.st.lang]){
        for(const item1 of this.menu.list[lang]){
          const valid1 = await this.isUrlValid(item1)
          item1.valid = valid1 ? 1 : -1

          // 2. Level
          for(const item2 of item1.list){
            const valid2 = await this.isUrlValid(item2)
            item2.valid = valid2 ? 1 : -1

            // 3. Level
            for(const item3 of item2.list){
              const valid3 = await this.isUrlValid(item3)
              item3.valid = valid3 ? 1 : -1
            }
          }
        }
      }

      this.st.checkingUrls = false
    },

    async isUrlValid(item){
      const url = item.url

      // Wenn es keinen Link gibt, die URL nicht prüfen
      if(!url) return true

      // Falls der Link mit HTTP anfängt
      if(url.startsWith('http')) {
        try {
          const res = await axios.post('/api/check-url.php', { url: url })
          // console.log('%c *************************************', 'color:lime;')
          // console.log(`${url}: ${res.data}`)
          // console.log('%c *************************************', 'color:lime;')
          return res.data === 1
        } catch (err) {
          return false
        }
      } else {
        try {
          const res = await axios.get(url)
          // console.log('%c OK %s:', 'color:gold;font-size:18px;', url, res)
          return true
        } catch (err) {
          // console.log('%c ERROR %s:', 'color:red;', url, err)
          return false
        }
      }
    },

    // Popover-Initialisierung, die ausgelöst wird, wenn sich etwas auf der Seite ändert
    popoverInit(){
      // console.log('%c POPOVER INIT', 'color:aqua;font-size:18px;')

      setTimeout(() => {
        const popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'))
        const popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
          return new bootstrap.Popover(popoverTriggerEl)
        })
      }, 50)
    },

    goto(url){
      window.location = url;
    },

    // Sprache wechseln
    setLang(lng){
      if(this.st.lang === lng) return

      // console.log('%c setLang(%s)', 'color:lime;font-size:32px;', lng)
      this.st.item3edit = null
      this.st.item2edit = null
      this.st.item1edit = null

      this.st.lang = lng

      // console.log('%c showMenuModel %s', 'color:lime;', this.st.showMenuModel)
      this.st.showMenuModel = false
      // console.log('%c showMenuModel %s', 'color:gold;', this.st.showMenuModel)

      // HTML Code ändern, falls eine andere Sprache ausgewählt wird
      // this.createHTML('setLang')

      // this.checkMenuUrls()
    },

    // Benutzer aus Dropdown hinzufügen
    addUser(user){
      // console.log('%c addUser(%s)', 'color:lime;', JSON.stringify(user))
      this.st.dropdownActive = false
      this.users.selected.push(user)
      this.st.query = ''
    },

    // Benutzer aus den ausgewählten Benutzern entfernen
    removeUser(userInd){
      // console.log('%c removeUser(%s)', 'color:orangered;', userInd)
      this.users.selected.splice(userInd,1)
    },

    // Fehler entfernen für das Feld, wo es Änderungen gegeben hat
    clearError(key){

      this.st.edited = true

      if(this.errors === null) return

      delete this.errors[key]
    },

    // Form prüfen
    checkData(){
      // console.log('%c checkData', 'color:magenta;font-size:18px;')
      this.errors = {}
      this.valid = true

      // Prüfen ob Felder nicht leer sind
      const fields = ['slug','name','bg_1','bg_2','bg_3','font_color']
      for(let field of fields){
        // console.log('%s Feld prüfen', field)
        if(this.menu[field].length === 0){
          this.errors[field] = `Das Feld ${field} ist leer`
          this.valid = false
        }
      }

      // Slug prüfen
      const re = /^([a-zA-Z0-9-_]*)$/
      if(!re.test(this.menu.slug)){
        console.log('Fehler im slug')
        this.errors.slug = 'Es gibt unzulässige Zeichen im Slug'
        this.valid = false
      }

      // Gibt es den Slug in der Liste der bereits erstellten Menüs
      if(this.st.slugs.includes(this.menu.slug)){

        if (this.st.curSlug === '' || this.st.curSlug !== this.menu.slug) {
          console.log('Dieser Slug existiert bereits')
          this.errors.slug = 'Dieser Slug existiert bereits'
          this.valid = false
        }

      }

      // Font Size prüfen
      const fs = parseInt(this.menu.font_size)
      if(Number.isInteger(fs)){
        if(this.menu.font_size.includes('px')){
          console.log('Es gibt px, prüfen...')
          if(fs < 5){
            console.log('Die minimale Textgröße ist 5px')
            this.errors.fs = 'Die minimale Textgröße ist 5px'
            this.valid = false
          }
          if(fs > 25){
            console.log('Die maximale Textgröße ist 25px')
            this.errors.fs = 'Die maximale Textgröße ist 25px'
            this.valid = false
          }
        }
      }else{
        console.log('Prüfen sie die Eingabe der Textgröße')
        this.errors.fs = 'Prüfen sie die Eingabe der Textgröße und versuchen sie nochmals auf "Neues Menü erstellen" zu klicken'
        this.valid = false
      }

      return this.valid
    },

    // Falls this.valid = true, dann alle Fehler zurücksetzen
    async saveData(url){
      // if(this.checkData()){
      //   this.errors = null
      // }else{
      //   return
      // }

      if(!this.checkData()) return

      this.errors = null

      // Daten vorbereiten
      const data = {
        menu: this.menu,
        users: this.users.selected.map(user => user.id)
      }

      // console.log('%c USERS: %s', 'color:gold;font-size:18px;', JSON.stringify(data.users))

      // Daten an den Server senden
      await axios.post(url, data)

      this.st.showMessage = true

      // Die Möglichkeit geben, auf den "Jetzt Testen" Button zu klicken, um das Menü zu testen
      this.st.edited = false

      window.history.replaceState( {} , '', location.pathname + '?slug=' + this.menu.slug)

      setTimeout(() => {
        this.st.showMessage = false
      }, 5000)

      // console.log('%c saveData', 'color:red;font-size:36px;', this.menu)

      // Falls wir das Menü erstellt haben
      if(url === '/api/create.php'){
        // window.location = '/edit-menu.php?slug=' + this.menu.slug + '&content=true';
        window.location = '/menu-created.php';
      }
    },

    // Menü vom Deutschen ins Englische clonen
    cloneMenu(){
      this.menu.list.en = JSON.parse(JSON.stringify(this.menu.list.de))
    },

    // Zwischen Content bearbeiten und Eigenschaften bearbeiten schalten
    setEditProps(val){
      // console.log('%c setEditProps(val: %s)', 'color:lime;', val)
      this.st.editProps = val
    },

    // Level 1 Punkt zum Bearbeiten auswählen
    selectMenu1Item(id){
      // console.log('%c selectMenu1Item(%s)', 'color:lime;', id);
      this.st.item3edit = null;
      this.st.item2edit = null;
      this.st.item1edit = id;
      this.popoverInit()
    },

    // Level 2 Punkt zum Bearbeiten auswählen
    selectMenu2Item(id){
      // console.log('%c selectMenu2Item(%s)', 'color:gold;', id);
      this.st.item3edit = null;
      this.st.item2edit = id;
      this.gotoEdit()
      this.popoverInit()
    },

    // Level 3 Punkt zum Bearbeiten auswählen
    selectMenu3Item(rowInd,id){
      // console.log('%c selectMenu3Item(%s, %s)', 'color:magenta;', rowInd, id);
      this.st.item2edit = rowInd;
      this.st.item3edit = id;
      this.gotoEdit()
      this.popoverInit()
    },

    // Zum Input scrollen
    gotoEdit(){
      var el = document.getElementById('edit-2-3')
      if(el){
        window.scrollBy({
          top: el.getBoundingClientRect().top - 10,
          behavior: 'smooth',
        });
      }
    },

    ////// Mobile Drag&Drop - START //////
    touchCalculateFields(level){
      let ref = null
      if (level === 1) {
        ref = this.$refs.items1
        // console.log('%c ref 1', 'color:orangered;font-size:24px;', ref)
      }
      if (level === 2) {
        ref = this.$refs.items2
        // console.log('%c ref 2', 'color:orangered;font-size:24px;', ref)
      }
      if (level === 3) {
        ref = this.$refs.items3
        // console.log('%c ref 3', 'color:orangered;font-size:24px;', ref)
      }

      if (ref === null) return

      console.log('REF:', ref)

      this.fields = ref.map(item => {
        const el = item.children['0']
        const x1 = el.offsetLeft
        const y1 = el.offsetTop
        return {
          x1: x1,
          x2: x1 + el.clientWidth,
          y1: y1,
          y2: y1 + el.clientHeight,
        }
      })

      // console.log(JSON.stringify(this.fields, null, 4))
      // this.fields = this.$refs.items1.map(item => {
      //   const el = item.children['0']
      //   const x1 = el.offsetLeft
      //   const y1 = el.offsetTop
      //   return {
      //     x1: x1,
      //     x2: x1 + el.clientWidth,
      //     y1: y1,
      //     y2: y1 + el.clientHeight,
      //   }
      // })
    },
    touchStart(data){
      console.clear()
      // console.log('%c touchStart', 'color:lime;', data.event);
      console.log('%c touchStart data.list.length', 'color:lime;', data.list.length)

      // Die aktuelle Liste der Elemente an die temporäre tmpList weiterleiten
      this.tmpList = data.list

      this.dnd.level = data.level
      this.dnd.path[0] = data.path[0]
      this.dnd.path[1] = data.path[1]
      this.dnd.path[2] = data.path[2]


      console.log(
        '%c DATA: %s dnd.level: %s path: %s',
        'color:lime;font-size:32px;',
        data.level,
        this.dnd.level,
        JSON.stringify(this.dnd.path)
      )

      this.touchCalculateFields(this.dnd.level)

      const target = data.event.target // Element bekommen auf den wir geklickt haben
      console.log('TARGET:', target)
      this.dnd.copy = target.cloneNode(true) // Aktuelles Element klonen
      this.dnd.copy.style.position = 'absolute' // Element absolut positionieren für Drag&Drop mit der Maus
      const coords = [
        // data.event.pageX, // DESKTOP
        // data.event.pageY // DESKTOP
        data.event.touches[0].pageX,
        data.event.touches[0].pageY
      ] // Dem Element X und Y der Mouse bestimmen

      this.dnd.selected = data.id // Die ID des ausgewählten anzeigen
      this.dnd.hovered = data.id // Den ausgewählten Element highliten

      // Listener hinzufügen, um die Bewegung der Maus zu registrieren
      // document.addEventListener('mousemove', this.onMouseMove)
      document.addEventListener('touchmove', this.touchMove)
      // Listener hinzufügen, um zu wissen, wann ein Mouseup passiert
      // ???????????????????????????????
      document.addEventListener('touchend', this.touchUp)
      // ???????????????????????????????

      // Dem Element Koordinaten bestimmen
      this.touchMoveTo(coords)

      console.log('%c # # # COORDS: %s', 'color:lime;', JSON.stringify(coords, null, 4))
      console.log(`%c item1edit: %s item2edit: %s item3edit: %s`, 'color:gold;', this.st.item1edit, this.st.item2edit, this.st.item3edit)

      // Element auf der Seite anzeigen
      document.body.append(this.dnd.copy)
    },

    touchMove($event){
      console.log('%c touchMove', 'color:gold;')
      // console.log(this.tmpList)
      // Wenn die Mouse sich bewegt, dann die Koordinaten des Elementes speichern
      this.touchMoveTo([
        $event.touches[0].pageX,
        $event.touches[0].pageY
      ])
    },

    touchMoveTo(coords){
      this.dnd.copy.style.left = coords[0] + 'px'
      this.dnd.copy.style.top = coords[1] + 'px'

      // Zusätzlich prüfen wir, ob das ELement sich in einer der Zonen aus this.fields befindet
      const x = coords[0]
      const y = coords[1]
      const index = this.fields.findIndex((item, index) => {
        // x,y - Koordinaten des Zeigers
        return ((x > item.x1 && x < item.x2) && (y > item.y1 && y < item.y2))
      })
      console.log('%c INDEX: %s', 'color:lime;font-size:12px;', index)
      if(index >= 0){
        this.touchOver(index, this.tmpList)
      }
    },

    // Sich virtuell über ein Element bewegen
    touchOver(id,list){
      // Bei Mouseover auf ein Element
      console.log('%c touchEnd(selected: %s, hovered: %s)', 'color:aqua;', this.dnd.selected, this.dnd.hovered)

      // Damit wir keine Elemente highliten (falls kein Element ausgewählt worden ist),
      // oder wenn ein Element bereits highlitet worden ist (wenn ein mouseover passiert)
      if(this.dnd.hovered === null || this.dnd.hovered === id) return

      // ID des Elementes speichern, auf dem die Mouse war
      this.dnd.hovered = id

      // Menü bilden
      this.rebuildList(list)

      if(this.dnd.level === 1){
        // Für Elemente des 1 Levels
        this.st.item1edit = id
      }
      if(this.dnd.level === 2){
        // Für Elemente des 2 Levels
        this.st.item2edit = id
      }
      if(this.dnd.level === 3){
        // Für Elemente des 3 Levels
        this.st.item2edit = this.dnd.path[1]
        this.st.item3edit = id
      }

      // this.tmpList = null
      // Feldlayout neu berechnen
      this.touchCalculateFields(this.dnd.level)
    },
    touchUp(){
      // Nach dem Ende des Drag&Drops, oder falls ein Mouseup registriert wird, müssen wir die Listener entfernen
      document.removeEventListener('touchmove', this.touchMove)
      document.removeEventListener('touchend', this.touchUp)

      console.log('%c touchUp','color:orangered;')
      // Die IDs des hovered und ausgewählten Elements zurücksetzten
      this.dnd.hovered = null
      this.dnd.selected = null
      // Die Kopie des Elementes aus dem DOM entfernen
      this.dnd.copy.remove()

      this.dnd.level = null
    },

    ////// Mobile Drag&Drop - ENDE //////

    ////////// Start des Drag&Drops //////////
    mouseDown(data){
      this.dnd.level = data.level
      this.dnd.path[0] = data.path[0]
      this.dnd.path[1] = data.path[1]
      this.dnd.path[2] = data.path[2]

      // console.log(
      //   '%c DATA: %s dnd.level: %s path: %s',
      //   'color:lime;font-size:32px;',
      //   data.level,
      //   this.dnd.level,
      //   JSON.stringify(this.dnd.path)
      // )
      // console.log(data.event)

      const target = data.event.target // Element bekommen auf den wir geklickt haben
      this.dnd.copy = target.cloneNode(true) // Aktuelles Element klonen
      this.dnd.copy.style.position = 'absolute' // Element absolut positionieren für Drag&Drop mit der Maus
      const coords = [data.event.pageX, data.event.pageY] // Dem Element X und Y Koordinaten der Mouse bestimmen

      this.dnd.selected = data.id
      this.dnd.hovered = data.id

      // Listener hinzufügen, um die Bewegung der Maus zu registrieren
      document.addEventListener('mousemove', this.onMouseMove)

      // Listener hinzufügen, um zu wissen, wann ein Mouseup passiert
      document.addEventListener('mouseup', this.mouseUp)

      // Dem Element Koordinaten bestimmen
      this.moveTo(coords)

      // Vorläufiges Element auf die Seite hinzufügen
      document.body.append(this.dnd.copy)

    },

    // Wenn die Mouse sich bewegt, dann die Koordinaten des Elementes speichern
    onMouseMove($event){
      this.moveTo([$event.pageX, $event.pageY])
    },

    // Neue Koordinaten definieren
    moveTo(coords){
      this.dnd.copy.style.left = coords[0] + 'px'
      this.dnd.copy.style.top = coords[1] + 'px'
    },

    // Bei Mouseover auf ein Element
    mouseOver(id,list){

      // console.log('%c mouseOver(selected: %s, hovered: %s)', 'color:aqua;', this.dnd.selected, this.dnd.hovered)

      // Damit wir keine Elemente highliten (falls kein Element ausgewählt worden ist),
      // oder wenn ein Element bereits highlitet worden ist (wenn ein mouseover passiert)
      if(this.dnd.hovered === null || this.dnd.hovered === id) return

      // ID des Elementes speichern, auf dem die Mouse war
      this.dnd.hovered = id

      // Menü rebilden
      this.rebuildList(list)

      if(this.dnd.level === 1){
        // Für Elemente des 1 Levels
        this.st.item1edit = id
      }
      if(this.dnd.level === 2){
        // Für Elemente des 2 Levels
        this.st.item2edit = id
      }
      if(this.dnd.level === 3){
        // Für Elemente des 3 Levels
        this.st.item2edit = this.dnd.path[1]
        this.st.item3edit = id
      }
    },

    // Menü rebilden
    rebuildList(list){

      console.log('%c rebuildList(from: %s, to: %s)', 'color:gold;font-size:14px;', this.dnd.selected, this.dnd.hovered)
      console.log('%c LEVEL: %s', 'color:lime;font-size:12px;', this.dnd.level, JSON.stringify(this.dnd, null, 4))
      console.log('%c LIST:', 'color:red;font-size:18px;', list)

      // Von
      const from = this.dnd.selected
      // Bis
      const to = this.dnd.hovered

      // Wenn wir von unten nach oben rüberziehen
      if(from < to){
        list.splice(to + 1, 0, list[from])
        // console.l'%c %s', 'color:lime;', og(JSON.stringify(list, null, 4))
        list.splice(from, 1)
        // console.l'%c %s', 'color:gold;', og(JSON.stringify(list, null, 4))

      // Wenn wir von oben nach unten rüberziehen
      }else{
        list.splice(to, 0, list[from])
        // console.log('%c %s', 'color:lime;', JSON.stringify(list, null, 4))
        list.splice(from + 1, 1)
        // console.log('%c %s', 'color:gold;', JSON.stringify(list, null, 4))
      }

      // Bearbeitung des 2. und 3. Levels zurücksetzen, weil es möglich ist, dass der 1. Level keine Sub-Punkte hat
      if(this.dnd.level === 1){
        this.st.item2edit = null
        this.st.item3edit = null
        this.dnd.selected = to
      }

      // Bearbeitung des 3. Levels zurücksetzen, weil es möglich ist, dass der 2. Level keine Sub-Punkte hat
      if(this.dnd.level === 2){
        this.st.item3edit = null
        this.dnd.selected = to
      }

      if(this.dnd.level === 3){
        this.dnd.selected = to
      }

      this.st.edited = true
    },

    // Nach dem Ende des Drag&Drops, oder falls ein Mouseup registriert wird, müssen wir die Listener entfernen
    mouseUp(){
      document.removeEventListener('mousemove', this.onMouseMove)
      document.removeEventListener('mouseup', this.mouseUp)

      // console.log('%c mouseUp','color:orangered;')

      // Die IDs des hovered und ausgewählten Elements zurücksetzten
      this.dnd.hovered = null
      this.dnd.selected = null

      // Die Kopie des Elementes aus dem DOM entfernen
      this.dnd.copy.remove()

      this.dnd.level = null

    },
    ////////// Endes des Drag&Drops //////////

    // Neuen Menüpunkt hinzufügen
    addNewItem(list, level){
      console.log('%c addNewItem(level: %s)', 'color:magenta;font-size:18px;', level)
      console.log(
        '%c item1: %s, item2: %s, item3: %s lang: %s', 'color:lime;font-size:18px;',
        this.st.item1edit, this.st.item2edit, this.st.item3edit, this.st.lang)
      console.log('LIST:', list)

      // Falls wir im Level 1 sind
      if(level === 1){
        const newItem = {
          name: 'Neuer Menü-Punkt',
          subtitle: '',
          url: '',
          valid: 0,
          // style: '',
          color: '',
          font_weight: '',
          bg: '',
          list: [],
        }
        this.st.item3edit = null;
        this.st.item2edit = null;
        list.push(newItem);
        this.st.item1edit = list.length - 1;

        const inp = document.getElementById('item-level-1');
        if(inp){
          inp.focus();
        }
      }

      // Falls wir im Level 2 sind
      if(level === 2){
        const newItem = {
          name: '2. Level Punkt',
          subtitle: 'Überschrift LVL 3',
          desc: 'Beschreibung...',
          // style: '',
          color: '',
          font_weight: '',
          bg: '',
          url: '',
          valid: 0,
          list: [],
        };
        this.st.item3edit = null;
        list.push(newItem);
        this.st.item2edit = list.length - 1;

        const inp = document.getElementById('item-level-2');
        if(inp){
          inp.focus();
        }
      }

      // Falls wir im Level 3 sind
      if(level === 3){
        const newItem = {
          name: '3. Level Punkt',
          // style: '',
          color: '',
          font_weight: '',
          bg: '',
          url: '',
          valid: 0,
        }
        list.push(newItem);
      }

    },

    // Menüpunkt entfernen
    removeItem(id, list, level){
      // level = 1, 2, 3 => Menü des 1. 2. 3. Level
      if(level === 1){
        this.st.item1edit = null;
        this.st.item2edit = null;
        this.st.item3edit = null;
      }

      if(level === 2){
        this.st.item2edit = null;
        this.st.item3edit = null;
      }

      if(level === 3){
        this.st.item3edit = null;
      }
      list.splice(id, 1)
    },
  },

  // Komponent wurde erstellt (es besteht Zugriff auf das Modell)
  created(){
    this.menu.id = this.st.id;
    this.st.curSlug = this.menu.slug;
  },

  // Wenn wir content=true in der URL haben, dann editProps = false setzten (Schaltung zwischen Content / Eigenschaften bearbeiten)
  mounted(){
    if(window.location.search.includes('content=true')){
      this.st.editProps = false
    }

    // Alle URL automatisch bei Aufruf der Seite checken
    this.checkMenuUrls()

    const keys = ['#picker_bg_1', '#picker_bg_2', '#picker_bg_3', '#picker_font_color']
    keys.forEach(itemKey => {
      const key = itemKey.split('picker_')[1]
      $(itemKey).spectrum({
        type: 'component',
        locale: 'de',
        move: (color) => {
          if (color) {
            this.menu[key] = color.toHexString()
          } else {
            this.menu[key] = ''
          }
          this.clearError(key)
        },
        change: (color) => {
          if (color) {
            this.menu[key] = color.toHexString()
          } else {
            this.menu[key] = ''
          }
          this.clearError(key)
        },
      })
    })

  }, // DOM-Rendering
})

///// Menü anzeigen (Preview) /////
Vue.component('menuBlock', {
  props: ['p-menu','p-state'],
  data: function(){
    return {
      menu: this.pMenu,
      st: this.pState,
    }
  },

  computed: {

    // Die Hintergrundfarbe mit Hilfe einer CSS Variable ändern
    cssBg1Color() {
      const r = document.querySelector(':root')
      r.style.setProperty('--border-color', this.menu.bg_1)
      this.createHTML('cssBg1Color');
    },

    // Submenü anzeigen, bei der Auswahl der Menüpunkte des 1. Levels
    selectedMenuItem() {
      // console.log('%c selectedMenuItem(%s, %s, %s)', 'color:aqua;font-size:18px;', this.st.lang, this.menu.list[this.st.lang].length, this.st.item1selected)
      // if(this.st.item1selected === null) return { list: [] }
      if(this.menu.list[this.st.lang].length === 0) return { list: [] }

      return this.menu.list[this.st.lang][this.st.item1selected]
    },

    // Submenü anzeigen, bei der Auswahl der Menüpunkte des 2. Levels
    selectedSubmenuItem() {
      return this.selectedMenuItem.list[this.st.item2selected]
    },

    // Menü-Modell für die ausgewählte Sprache anzeigen
    jsonMenu(){
      return JSON.stringify(this.menu.list[this.st.lang], null, 4)
    },

    jsonMenuSettings(){
      const data = {
        bg_1: this.menu.bg_1,
        bg_2: this.menu.bg_2,
        bg_3: this.menu.bg_3,
        font_color: this.menu.font_color,
        font_size: this.menu.font_size,
        id: this.menu.id,
        name: this.menu.name,
        slug: this.menu.slug,
        special_param: this.menu.special_param,
      }
      return JSON.stringify(data, null, 4)
    },

    // Menü in dem 2. Level richtig rendern, Anzahl der Zellen bestimmen
    secondLevel() {
      const menu = this.selectedMenuItem.list

      const len = Math.ceil(menu.length / 2)
      const result = []

      for(let i = 0; i < len; i++){
        const left = menu[i * 2]
        const right = menu[i * 2 + 1]
        result.push([left,right])
      }

      return result
    },
  },
  methods: {
    showLinkIco(url, extFlag){
      // External Link: isExt === true
      // Internatl Link: isExt === false
      if (!url) return false

      let isExt = url.startsWith('http')

      if(url.includes('uni-goettingen.de')) {
        isExt = false
      }

      return isExt && extFlag || !isExt && !extFlag
    },
    // HTML Code in den Buffer speichern
    async copyCode(){
      console.log('%c copyCode', 'color:lime;font-size:28px;')
      const text = this.st.HTML

      if(window.clipboardData && window.clipboardData.setData){
        return clipboardData.setData("Text", text);
      }else if(document.queryCommandSupported && document.queryCommandSupported("copy")) {
        var textarea = document.createElement("textarea");
        textarea.textContent = text;
        textarea.style.position = "fixed";
        document.body.appendChild(textarea);
        textarea.select();

        try{
          // return document.execCommand("copy");
          document.execCommand("copy");
          this.st.copied = true
        }catch(ex){
          console.warn("Copy to clipboard failed.", ex);
          return false;
        }finally{
          document.body.removeChild(textarea);
        }
      }
    },

    // Bei Änderung der Sprache, den HTML Code ändern
    changeLang(lang){
      this.st.lang = lang;
      this.st.showMenuModel = false;
      this.createHTML('changeLang');
    },

    // Modell mit HighlighJS anzeigen
    showModel(action){
      this.st.showMenuModel = action
      if(action){
        setTimeout(() => {
          hljs.highlightAll()
        },100)
      }
    },

    // Generierten HTML Code runterladen
    async downloadHTML(){
      // console.log('%c downloadHTML', 'color:lime;font-size:32px;')

      const result = await axios({
        url: `/preview.php?slug=${this.menu.slug}&preview&lang=${this.st.lang}`,
        method: 'GET',
        responseType: 'blob',
      })

      // console.log('RESULT:', result.data)

      const FILE = window.URL.createObjectURL(new Blob([result.data]))
      const docUrl = document.createElement('a')
      docUrl.href = FILE
      docUrl.setAttribute('download', 'result.html')
      document.body.appendChild(docUrl)
      // console.log('A:', docUrl)
      docUrl.click()

    },

    // HTML Code des Menüs bekommen
    async createHTML(from){
      // console.log('%c createHTML %s', 'color:lime;font-size:32px;', from)
      const url = `/preview.php?slug=${this.menu.slug}&preview&lang=${this.st.lang}`;
      const result = await axios.get(url)
      this.st.HTML = result.data
      this.st.showHTMLCode = false
    },

    // HTML Code anzeigen / verstecken
    toggleHTMLCode(action){
      this.st.showHTMLCode = action
      // this.st.showMenuModel = false
      if(action){
        setTimeout(() => {
          hljs.highlightAll()
        },100)
      }
    },

    goto(url){
      window.location = url;
    },

    // In der mobilen Version - Menü anzeigen / verstecken, bei klick auf den Hamburger
    menuToggle() {
      this.st.menuActive = !this.st.menuActive
      this.st.item1selected = 0
      this.st.item2selected = 0
    },

    // Was bei Klick auf Level 1 Punkte passiert
    selectFirstLevel(id, item) {
      if(item && item.url && item.list.length === 0){
        window.location = item.url;
        return;
      }

      if(this.st.item1selected === id) return;

      this.st.item1selected = id;
      this.st.item2selected = 0;
      // console.log(`%c selectFirstLevel(${id})`, 'color:lime;')
    },

    // Was bei Klick auf Level 2 Punkte passiert
    selectSecondLevel(id, delay, item) {
      // console.log(`%c selectSecondLevel(id: ${id}, delay: ${delay}:${this.st.delay})`, 'color:orangered;')
      //  console.log('ITEM:',item);

      if(item && item.url && item.list.length === 0){
        window.location = item.url;
        return;
      }

      if(delay){
        // Schalten zwischen Menüpunkten mit Verzögerung
        this.st.timeoutId = setTimeout(() => {
          // Nach der angegebenen Zeit schalten wir den Menüpunkt um
          this.st.item2selected = id;
        },this.st.delay);
      }else{
        // Sofort umschalten
        this.st.item2selected = id;
      }
    },
    unselectSecondLevel(id){
      // Wenn der Schaltvorgang zuvor gestartet wurde und noch nicht abgeschlossen ist, Timer zurücksetzen
      if(this.st.timeoutId){
        clearTimeout(this.st.timeoutId);
      }
    },
  },

  // Nach dem Rendern der Seite die Variable für die Styles bestimmen
  mounted() {
    const r = document.querySelector(':root')
    r.style.setProperty('--border-color', this.menu.bg_1)
    this.createHTML('mounted');
  },
})

///// Benutzerliste  /////
Vue.component('userList', {
  props: ['list'],
  data: function(){
    return {
      list: this.pList,
    }
  },
})

// App erstellen
new Vue({ el: '#app' })

///// Bootstrap /////
function initPage() {
  var popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'))
  var popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
    return new bootstrap.Popover(popoverTriggerEl)
  })
}

document.addEventListener('DOMContentLoaded', initPage)
