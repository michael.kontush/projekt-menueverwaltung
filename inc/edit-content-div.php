<!-- 2 und 3 Level Punkte im Content bearbeiten, auf der edit-menu.php Seite -->

<div
  data-tag="table"
  v-if="st.item1edit !== null"
  class="table table-bordered"
>
  <strong
    class="btn btn-primary btn-sm mb-3"
    @click="dnd.hideLastLevel = !dnd.hideLastLevel"
  >
    <span v-if="dnd.hideLastLevel">
      3. Level anzeigen
    </span>
    <span v-else>
      3. Level verstecken
    </span>
  </strong>

  <div
    class=""
    data-tag="tbody"
    style="
      border: none;
      display: grid;
      grid-template-columns: auto auto;
      justify-items: stretch;
    "
  >
    <template
      data-tag="tr"
      v-for="(row, rowInd) in menu.list[st.lang][st.item1edit].list"
    >
      <!-- BEGIN 1 -->
      <div class="grid-item">
        <div
          class="nowrap"
          ref="items2"
        >
          <a
            data-level="2"
            :href="row.url"
            class="btn btn-outline-primary--- btn-sm"
            :class="st.item2edit === rowInd ? 'btn-primary' : 'btn-outline-primary'"
            @mousedown.prevent="mouseDown(
              {
                path: [st.item1edit, rowInd, null],
                level: 2,
                event: $event,
                item: row,
                id: rowInd,
                list: menu.list[st.lang][st.item1edit].list
              }
            )"
            @mouseover="mouseOver(rowInd, menu.list[st.lang][st.item1edit].list)"
            @touchstart.prevent="touchStart(
              {
                path: [st.item1edit, rowInd, null],
                level: 2,
                event: $event,
                item: row,
                id: rowInd,
                list: menu.list[st.lang][st.item1edit].list
              }
            )"
            @click.prevent="selectMenu2Item(rowInd)"
          >
            {{ row.name }}
            <span v-if="row.valid === -1">[!]</span>
          </a>
          <span
            class="bi-x-square ms-1 button_remove"
            @click="removeItem(
              rowInd,
              menu.list[st.lang][st.item1edit].list,
              2
            )"
          ></span>
          <span
            class="bi-pencil-square ms-2 button_edit mob-inline-block"
            @click.stop="selectMenu2Item(rowInd)"
          ></span>
        </div>
      </div>
      <!-- END 1 -->
      <!-- .................... -->
      <!-- BEGIN 2 -->
      <div class="grid-item">
        <div style="margin: -5px 0 -15px 0;">
          <!-- margin: top right bottom left; -->
          <small>
          {{ row.subtitle }}
          </small>
        </div>
        <div v-show="!dnd.hideLastLevel">
          <div
            v-for="(item, itemInd) in row.list"
            ref="items3"
            class="nowrap"
            :key="itemInd"
          >
            <a
              data-level="3"
              :href="item.url"
              class="btn btn-outline-info--- btn-sm mt-1"
              :class="[
                { 'dnd_hovered': dnd.path[1] === rowInd && itemInd === dnd.hovered },
                dnd.path[1] === rowInd && st.item3edit === itemInd ? 'btn-primary' : 'btn-outline-primary'
              ]"
              @mousedown.prevent="mouseDown(
                {
                  path: [st.item1edit, rowInd, itemInd],
                  level: 3,
                  event: $event,
                  item: item,
                  id: itemInd,
                  list: row.list
                }
              )"
              @mouseover="mouseOver(itemInd, row.list)"
              @touchstart.prevent="touchStart(
                {
                  path: [st.item1edit, rowInd, itemInd],
                  level: 3,
                  event: $event,
                  item: item,
                  id: itemInd,
                  list: row.list
                }
              )"
              @click.stop.prevent="selectMenu3Item(rowInd, itemInd)"
            >
              {{ item.name }}
              <span v-if="item.valid === -1">[!]</span>
            </a>
            <span
              class="bi-x-square ms-1 button_remove"
              @click="removeItem(
                itemInd,
                row.list,
                3
              )"
            ></span>
            <span
              class="bi-pencil-square ms-2 button_edit mob-inline-block"
              @click.stop="selectMenu3Item(rowInd, itemInd)"
            ></span>
          </div>
          <a
            href="#"
            class="btn btn-outline-primary btn-sm mt-2"
            @click.prevent="addNewItem(row.list, 3)"
          >
            Neuen 3. Level Punkt hinzufügen
          </a>
        </div>
      </div>
      <!-- END 2 -->
    </template>
  </div>

</div><!-- table -->
