<?php

  // Die Variable $full erlaubt uns Teile des Codes für den HTML Export Code zu verstecken

  if($full) {

  echo '

  <div class="footer">
    <div class="container pt-2 pb-2 text-center my-3"><small>
      <span class="text-secondary"><strong>© Projektseminar Q1-Q2 2022</strong></span>
      <div class="me-1 mt-2 text-secondary">
        <span class="me-2 ms-1"><a href="create.php" class="footer-link">Menü&nbsp;erstellen</a></span>
        <span class="me-2"><a href="edit-menu.php?slug=super-menu" class="footer-link">Menü&nbsp;bearbeiten</a></span>
        <span class="me-2"><a href="preview.php?slug=super-menu" class="footer-link">Menü&nbsp;Vorschau</a></span>
        <span class="me-2"><a href="users.php" class="footer-link">Benutzerverwaltung</a></span>
        <span class="me-2"><a href="edit-user.php?id=80" class="footer-link">Benutzer&nbsp;bearbeiten</a></span>
      </div>
      <div class="me-1 mt-1">
        <span class="me-2"><a href="export.php" class="footer-link">HTML Code Export Test</a></span>
        <span class="me-2"><a href="login.php" class="footer-link">Benutzer Login</a></span>
        <span class="me-2"><a href="edit-password.php" class="footer-link">Passwort&nbsp;ändern</a></span>
        <span class="me-2"><a href="menu-created.php" class="footer-link">Menü&nbsp;erstellt</a></span>
        <span class="me-2"><a href="choose-menu.php" class="footer-link">Startseite</a></span>
      </div>
    </small></div>
  </div>

  ';

}

?>
