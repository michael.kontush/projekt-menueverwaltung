<?php

include('../inc/db.php');

// Slug aus der URL bekommen
$chosen_menu = $_GET['slug'];

// Als default ist die Deutsche Sprache ausgewählt
// Falls wir "en" im GET bekommen, dann die Englische Version anzeigen
$lang = 'de';
if($_GET['lang'] === 'en'){
  $lang = 'en';
}

// Daten des Menüs nach Slug bekommen
$sql = "SELECT model, name, slug FROM menus WHERE slug = '$chosen_menu'";
$result = mysqli_query($conn,$sql);
$row = mysqli_fetch_array($result);

// Model vorbereiten
$final_model = str_replace("\"","'", $row['model']);

?>

<!--

p-state:
  menuActive: Menü öffnen / verstecken, in der mobilen Version
  lang: Sprache
  item1selected: Ausgewählter 1. Level Punkt
  item2selected: Ausgewählter 2. Level Punkt
  timeoutId: wurde benutzt, um bei einem Mouseover über die rechte Seite des 2. Level, nicht direkt den 3. Level anzuzeigen.
             wir haben uns mit den Betreuern entschieden, den mouseover mit einem click zu ersetzten
  delay: zusammen mit timeoutID, wird nicht mehr benutzt
  copied: HTML Code kopiert
  showMenuModel: Menü Modell anzeigen
  showHTMLCode: HTML Code anzeigen
  menus: Sprachen
p-menu: Modell

-->

<menu-block
  inline-template
  :p-state="{
    menuActive: false,
    lang: '<?php echo $lang; ?>',
    item1selected: 0,
    item2selected: null,
    timeoutId: null,
    delay: 500,
    showMenuModel: false,
    showHTMLCode: false,
    menus: [
      {
        key: 'de',
        name: 'Deutsches Menu',
      },
      {
        key: 'en',
        name: 'Englisches Menu',
      },
    ],
  }"
  :p-menu="<?php echo $final_model; ?>"
  >

          <div id="test" v-cloak>

            <!-- 1 Level Punkte -->

            <div class="row">
              <div class="col mx-1">
                <nav class="navbar navbar-expand-lg navbar-dark">
                  <div class="container-fluid">
                    <button
                      class="navbar-toggler"
                      type="button"
                      @click="menuToggle"
                    >
                      <span class="navbar-toggler-icon"></span>
                    </button>
                    <div
                      class="navbar-collapse"
                      :class="st.menuActive ? 'show' : 'collapse'"
                    >
                      <ul class="navbar-nav me-auto mb-2 mb-lg-0 flex-wrap top-space">
                        <li
                          v-for="(item_1,itemInd) in menu.list[st.lang]"
                          class="nav-item level_1 nowrap"
                          :class="{ active: st.item1selected === itemInd }"
                          :style="{
                            backgroundColor: item_1.bg,
                            fontWeight: item_1.font_weight
                          }"
                        >
                          <a
                            class="nav-link"
                            aria-current="page"
                            :href="item_1.url"
                            :style="{
                              color: item_1.color,
                              fontSize: menu.font_size
                            }"
                            style="
                              display:flex;
                              align-items:center;
                            "
                            @mouseover="selectFirstLevel(itemInd)"
                            @click.prevent="selectFirstLevel(itemInd,item_1)"
                          >
                            {{ item_1.name }}
                            <div
                              v-if="item_1.list.length"
                            >
                              <svg
                                viewBox="0 0 16 16"
                                height="12"
                                width="12"
                                fill="currentColor"
                                class="ico-exp-mob"
                                :class="{ active: st.item1selected === itemInd }"
                              >
                                <use href="#expand"/>
                              </svg>

                            </div>

                            <span
                              class="ico-ext-link-mob"
                              @click="goto(item_1.url)"
                            >
                              <svg
                                v-if="item_1.url"
                                viewBox="0 0 16 16"
                                height="12"
                                width="12"
                                fill="currentColor"
                              >
                                <use href="#external-link"/>
                              </svg>
                            </span>
                          </a>
                          <ul
                            v-if="itemInd === st.item1selected"
                            class="menu_mob_2"
                            :style="{ borderColor: menu.bg_1 }"
                            style="
                              border-style: solid;
                              border-width: 1px 0 0 0;
                            "
                          >
                            <li v-for="(row,rowInd) in selectedMenuItem.list">
                              <a
                                class="level_2_mob"
                                :href="row.url"
                                @click.prevent.stop="selectSecondLevel(rowInd, false, row)"
                                style="
                                  display:flex;
                                  white-space:normal;
                                "
                              >
                                {{ row.name }}
                                <span v-if="row.list.length">

                                  <svg
                                    viewBox="0 0 16 16"
                                    height="12"
                                    width="12"
                                    fill="currentColor"
                                    class="ico-exp-mob-2"
                                    :class="{ active: st.item2selected === rowInd }"
                                  >
                                    <use href="#expand"/>
                                  </svg>
                                </span>
                                <span
                                  v-if="row.url"
                                  @click.stop="goto(row.url)"
                                  style="
                                    margin-left:auto;
                                    padding-left:15px;
                                  "
                                >
                                  <svg
                                    viewBox="0 0 16 16"
                                    height="12"
                                    width="12"
                                    fill="currentColor"
                                    class="ico-exp-mob-2"
                                  >
                                    <use href="#external-link"/>
                                  </svg>
                                </span>
                              </a>

                              <div
                                v-if="rowInd === st.item2selected"
                                class=""
                                :style="{
                                  borderColor: row.list.length ? menu.bg_1 : 'transparent'
                                }"
                                style="
                                  border-style: solid;
                                  border-width: 1px 0 0 0;
                                  /* top right bottom left */
                                "
                              >
                                <div
                                  v-if="row.list.length"
                                  class="menu_mob_3_descr"
                                >
                                  {{ row.subtitle }}
                                </div>
                                <ul
                                  class="menu_mob_3"
                                >
                                  <li v-for="item in row.list">
                                    <a
                                      class="level_3_mob"
                                      :href="item.url"
                                    >
                                      {{ item.name }}
                                    </a>
                                  </li>
                                </ul>
                              </div>

                            </li>
                          </ul>
                        </li>
                      </ul>
                    </div>
                  </div>
                </nav>
              </div>
            </div>

            <!-- Ende 1 Level Punkte -->

            <!-- selectedMenuItem -->
            <div
              class="row menu_border mx-1 justify-content-between submenu"
              :style="{ backgroundColor: menu.bg_2 }"
            >

              <!-- Beginn 2 Level Punkte -->
              <div class="col-12 col-lg-8 col-md-12 col-sm-12 my-2 mb-4">
                <div class="level_2_big ms-3">
                 <a
                  :href="selectedMenuItem.url"
                  :style="{
                    color: menu.font_color,
                    fontSize: menu.font_size
                  }"
                >
                  {{ selectedMenuItem.subtitle }}
                </a>
                </div>

                <div
                  v-for="(row,rowInd) in secondLevel"
                  class="row mb-1"
                >

                  <div
                    v-for="(item_2,itemInd) in row"
                    class="col ms-3 mt-3"
                  >
                    <div
                      v-if="item_2"
                      class="vl"
                      :class="{ active: st.item2selected === rowInd * 2 + itemInd }"
                    >
                      <a
                        :href="item_2.url"
                        class="level_2_link"
                        :class="{ 'cursor-default': !item_2.url }"
                        :style="{ fontSize: menu.font_size }"
                        @mouseover="selectSecondLevel(rowInd * 2 + itemInd, true)"
                        @mouseleave="unselectSecondLevel(rowInd * 2 + itemInd)"
                      >
                        <div
                          class="level_2_title ms-2"
                          :style="{
                            color: item_2.color || menu.font_color,
                            backgroundColor: item_2.bg,
                            fontWeight: item_2.font_weight
                          }"
                        >
                          {{ item_2.name }}

                          <svg
                            v-if="item_2.url"
                            viewBox="0 0 16 16"
                            height="12"
                            width="12"
                            fill="currentColor"
                            class="ico-ext-link"
                          >
                            <use href="#external-link"/>
                          </svg>
                        </div>
                        <div
                          class="level_2_desc ms-2"
                          :style="{ color: item_2.color || menu.font_color }"
                        >
                          {{ item_2.desc }}
                        </div>
                      </a>
                    </div>
                  </div>
                </div>

              </div>
              <!-- Ende 2 Level Punkte -->

              <!-- Beginn 3 Level Punkte -->
              <div
                v-if="selectedSubmenuItem"
                class="col-12 col-lg-4 col-md-12 col-sm-12"
                :style="{
                  backgroundColor: menu.bg_3,
                  fontSize: menu.font_size
                }"
              >
                <div class="mt-2">
                  <div
                    class="level_3_big ms-1"
                  >
                    <a
                      :href="selectedSubmenuItem.url"
                      :style="{ color: menu.font_color }"
                      style="font-size:.92em;"
                    >
                      {{ selectedSubmenuItem.subtitle }}
                    </a>
                  </div>
                </div>
                <div
                  v-for="item_3 in selectedSubmenuItem.list"
                  class="ms-3 mt-2"
                >
                  <a
                    class="level_3_title"
                    :href="item_3.url"
                    :style="{
                      color: item_3.color || menu.font_color,
                      backgroundColor: item_3.bg,
                      fontWeight: item_3.font_weight
                    }"
                    style="font-size:.92em;"
                  >
                    {{ item_3.name }}
                  </a>
                </div>
                <br>
              </div>
              <!-- Ende 3 Level Punkte -->

            </div>

  </div>
</menu-block>
