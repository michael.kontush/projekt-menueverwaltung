<!-- Content bearbeiten, auf der edit-menu.php Seite -->

<div class="row">
  <div class="col">
    <div class="btn-group" role="group" aria-label="Basic outlined example">
      <button
        v-for="item in st.menus"
        type="button"
        class="btn"
        :class="st.lang === item.key ? 'btn-dark' : 'btn-outline-dark'"
        @click="setLang(item.key)"
      >
        {{ item.name }}
      </button>
    </div>
    <div
      v-if="st.checkingUrls"
      class="text-danger ts ms-2 mt-1"
    >
      <small>Die Links werden geprüft...</small>
    </div>
    <div
      v-else
      class="text-success ts ms-2 mt-1">
      <small>Alle Links wurden geprüft.</small>
    </div>
    <transition name="fade">
      <div
        v-if="st.edited"
        class="text-danger ts ms-2 mt-1"
      >
        <small>Änderungen wurden vorgenommen. Speichern sie das Menü, um es zu testen.</small>
      </div>
    </transition>
  </div>
</div>

<hr/>


<!-- 1. Level Punkte falls es keine Punkte gibt (Neuer Punkt Button) -->

<div
  v-if="menu.list[st.lang].length === 0 && st.lang === 'de'"
  class=""
>
  <div class="row">
    <div class="col mb-2">
      <strong>» 1. Level Menüpunkte: </strong> Erstellen sie einen neuen 1 Level Punkt!
    </div>
  </div>
  <div class="row">
    <div class="col-12 col-lg-3 col-md-4 col-sm-4 my-2">
      <a
        href="#"
        class="btn btn-outline-primary"
        @click.prevent="addNewItem(menu.list[st.lang], 1)"
      >
        Neuer Punkt +
      </a>
    </div>
  </div>
</div>

<!-- 1. Level Punkte falls es bereits Punkte gibt -->

<div
  v-if="menu.list[st.lang].length"
  class=""
>

  <div class="row">
    <div class="col mb-2">
      <strong>» 1. Level Menüpunkte:</strong> Klicken sie auf "Unterpunkte hinzufügen", um 2 und 3 Level Punkte zu erstellen.
    </div>
  </div>

  <div class="row">
    <div
      data-level="1"
      v-for="(item, itemInd) in menu.list[st.lang]"
      ref="items1"
      :key="itemInd"
      class="col-12 col-lg-3 col-md-4 col-sm-6 my-2"
      style="white-space:nowrap;"
      @click="selectMenu1Item(itemInd)"
    >
      <a
        :href="item.url"
        class="btn"
        style="white-space:normal;"
        :class="st.item1edit === itemInd ? 'btn-primary btn-active' : 'btn-outline-primary btn-outline-active'"
        @mousedown.prevent="mouseDown(
          {
            path: [st.item1edit, null, null],
            level: 1,
            event: $event,
            item: item,
            id: itemInd,
            list: menu.list[st.lang]
          }
        )"
        @mouseover="mouseOver(itemInd, menu.list[st.lang])"
        @touchstart.prevent="touchStart(
          {
            path: [st.item1edit, null, null],
            level: 1,
            event: $event,
            item: item,
            id: itemInd,
            list: menu.list[st.lang]
          }
        )"
        @click.prevent=""
      >
        {{ item.name }}
        <span v-if="item.valid === -1">[!]</span>
      </a>
      <span
        class="bi-x-square ms-2 button_remove"
        @click.stop="removeItem(
          itemInd,
          menu.list[st.lang],
          1
        )"
      ></span>
      <span
        class="bi-pencil-square ms-2 button_edit mob-inline-block"
        @click.stop="selectMenu1Item(itemInd)"
      ></span>
    </div>

    <div class="col-12 col-lg-3 col-md-4 col-sm-4 my-2">
      <a
        href="#"
        class="btn btn-outline-primary btn-outline-active"
        @click.prevent="addNewItem(menu.list[st.lang], 1)"
      >
        Neuer Punkt +
      </a>
    </div>

  </div>

  <!-- 1. Level Punkte bearbeiten -->

  <?php include 'inc/edit-content-lvl-1.php';?>

  <!-- 2-3 Level Punkte hinzufügen -->

  <div class="row">
    <div class="col">
      <a
        v-if="showAddSubmenu"
        href="#"
        class="btn btn-outline-primary"
        @click.prevent="addNewItem(menu.list[st.lang][st.item1edit].list, 2)"
      >
        Unterpunkte hinzufügen
      </a>
    </div>
  </div>

  <!-- 2. und 3. Level Punkte -->

  <div v-if="isSubmenu">
    <div class="row">
      <div class="col mb-2 mt-3">
        <strong>» 2. und 3. Level Menüpunkte</strong>
      </div>
    </div>

    <?php include 'inc/edit-content-div.php';?>

    <a
      v-if="st.item1edit !== null"
      href="#"
      class="btn btn-outline-primary btn-sm"
      @click.prevent="addNewItem(menu.list[st.lang][st.item1edit].list, 2)"
    >
      Neuen 2. Level Punkt hinzufügen
    </a>

    <?php include 'inc/edit-content-lvl-2-3.php';?>

  </div>

  <hr>

</div>

<!-- Deutsches Menü auf Englisch Clonen -->

<div
  v-if="!menu.list[st.lang].length && st.lang === 'en'"
  class=""
  style="
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  "
>
  <h6>Es gibt noch kein Englisches Menü.</h6>

  <button
    type="button"
    class="btn btn-outline-primary mt-2"
    @click="cloneMenu"
  >
    Englisches Menü erstellen
  </button>
</div>
