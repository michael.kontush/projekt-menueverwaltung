<?php

// Admin-Level des Benutzers bestimmen

if(!empty($_SESSION["username"])){

  $admin_header = $_SESSION["admin_level"];

  switch($admin_header){
      case "0":
          $admin_header = "Superadmin";
          break;
      case "1":
          $admin_header = "Admin Level 1";
          break;
  }

}

// START - Active Seite im Sub-Menu markieren

// Den Teil der URL nach dem "/" bestimmen
$url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$pos = strrpos($url, '/');
$slug_end = $pos === false ? $url : substr($url, $pos + 1);

$users_active = "";
$create_active = "";
$choose_menu_active = "";

if($slug_end == 'choose-menu.php'){
  $choose_menu_active = " active";
  $create_active = "";
  $users_active = "";
}

if($slug_end == 'create.php'){
  $create_active = " active";
  $choose_menu_active = "";
  $users_active = "";
}

if($slug_end == 'users.php'){
  $users_active = " active";
  $create_active = "";
  $choose_menu_active = "";
}

// ENDE - Active Seite im Sub-Menu markieren

// Variable definieren, die uns erlaubt Teile des Codes für den HTML Export zu verstecken

$full = isset($_GET['preview']) ? 0 : 1;

if($full){

  // Header Titel und User Login

  echo '

  <div class="header mb-1">
    <div class="container pt-1 pb-1">

      <div class="row mb-1" style="display:flex; align-items: center;">
        <div class="col text-center--- mt-3---">
          <a href="choose-menu.php">
            <img src="images/ps.png" alt="" width="204" height="63" class="d-inline-block align-text-top">
          </a>
        </div>
        <div class="col-12 col-sm-12 col-md-6 col-lg-6">
          
        </div>
        <div class="col col--center mt-1--- text-center" style="width: 204px;">
          <div class="text-end small mb-1 text-white">

          '; }

            // Falls nicht eigenloggt, Benutzername und Admin-Level nicht anzeigen

            if(!empty($_SESSION["username"])){

              if($full){

                echo '

                Eingeloggt als <b><a href="edit-password.php" class="text-white">'.$_SESSION["username"].' ('.$_SESSION["id"].')</a></b>

                <a
                  class="bi-box-arrow-left ms-1 button_logout"
                  href="logout.php"
                ></a>

                <br>
                <span class="text-white">
                  <small>'.$admin_header.'</small>
                </span>

                ';

              }
            }


          if($full){

            echo '

          </div>
        </div>
      </div>
      <div style="border-top: 1px solid white;">
        <nav class="nav">
          <a class="nav-link'.$choose_menu_active.'" href="choose-menu.php">Übersicht aller Menüs</a>
          <a class="nav-link'.$create_active.'" href="create.php">Menü erstellen</a>
          <a class="nav-link'.$users_active.'" href="users.php">Benutzerverwaltung</a>
        </nav>
      </div>
    </div>
  </div>


          ';

        }

?>
