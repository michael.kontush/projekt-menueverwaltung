  <!-- Eigenschaften bearbeiten, auf der edit-menu.php Seite -->

  <form name="updateForm" action="" method="post">

    <input type="hidden" name="id" class="txtField">

    <div class="row justify-content-center">

      <!-- Anzeigename -->
      <div class="col-12 col-lg-3 col-sm-6 mb-2">
        <small><label for="name" class="text-secondary">
          Anzeigename*
        </label></small>
        <div class="input-group my-1">
          <input
            type="text"
            class="form-control br"
            :class="{ 'is-invalid': errors && errors.name }"
            placeholder="Anzeigename*"
            required
            @input="clearError('name')"
            v-model="menu.name"
          >
          <!-- @input="st.edited = true" -->
          <span
            class="d-inline-block"
            tabindex="0"
            data-bs-toggle="popover"
            data-bs-trigger="hover focus"
            data-bs-content="z.B: Hauptmenu der Uni"
          >
          <span class="input-group-text ms-1">?</span>
        </div>
      </div>

      <!-- Tag -->
      <div class="col-12 col-lg-3 col-sm-6 mb-2">
        <small><label for="slug" class="text-secondary">Tag*</label></small>
        <div class="input-group my-1">
          <input
            type="text"
            class="form-control br"
            :class="{ 'is-invalid': errors && errors.slug }"
            placeholder="Tag*"
            required
            @input="clearError('slug')"
            v-model="menu.slug"
          >
          <span
            class="d-inline-block"
            tabindex="0"
            data-bs-toggle="popover"
            data-bs-trigger="hover focus"
            data-bs-content="z.B: hauptmenu-2022"
          >
          <span class="input-group-text ms-1">?</span>
        </div>
      </div>
      <div class="col-12 col-lg-3 col-sm-6 mb-2">
        <!-- Placeholder -->
      </div>
      <div class="col-12 col-lg-3 col-sm-6 mb-2">
        <!-- Placeholder -->
      </div>
    </div>

    <!-- Input Felder - Hintergrundfarbe, Textfarbe, Schriftgröße -->

    <div class="row">

      <!-- Hintergrundfarbe 1 -->
      <div class="col-12 col-lg-3 col-sm-6 mb-2">
        <small><label for="bg-colour" class="text-secondary">Hintergrundfarbe (1 Level)</label></small>
        <div class="input-group my-1 fwnw">
          <!-- <input
            type="text"
            class="form-control br"
            :class="{ 'is-invalid': errors && errors.bg_1 }"
            placeholder="BG Colour (1 LVL)*"
            v-model="menu.bg_1"
            @input="clearError('bg_1')"
          > -->
          <input
            id="picker_bg_1"
            class="form-control brr"
            placeholder="BG Colour (1 LVL)*"
            :value="menu.bg_1"
          />
          <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="z.B: blue oder #ffffff"/>
          <span class="input-group-text ms-1">?</span>
        </div>
      </div>

      <!-- Hintergrundfarbe 2 -->
      <div class="col-12 col-lg-3 col-sm-6 mb-2">
        <small><label for="bg-colour" class="text-secondary">Hintergrundfarbe (2 Level)</label></small>
        <div class="input-group my-1 fwnw">
          <!-- <input
            type="text"
            class="form-control br"
            :class="{ 'is-invalid': errors && errors.bg_2 }"
            placeholder="BG Colour (2 LVL)*"
            v-model="menu.bg_2"
            @input="clearError('bg_2')"
          > -->
          <input
            id="picker_bg_2"
            class="form-control brr"
            placeholder="BG Colour (2 LVL)*"
            :value="menu.bg_2"
          />
          <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="z.B: blue oder #ffffff"/>
          <span class="input-group-text ms-1">?</span>
        </div>
      </div>

      <!-- Hintergrundfarbe 3 -->
      <div class="col-12 col-lg-3 col-sm-6 mb-2">
        <small><label for="bg-colour" class="text-secondary">Hintergrundfarbe (3 Level)</label></small>
        <div class="input-group my-1 fwnw">
          <!-- <input
            type="text"
            class="form-control br"
            :class="{ 'is-invalid': errors && errors.bg_3 }"
            placeholder="BG Colour (3 LVL)*"
            v-model="menu.bg_3"
            @input="clearError('bg_3')"
          > -->
          <input
            id="picker_bg_3"
            class="form-control brr"
            placeholder="BG Colour (3 LVL)*"
            :value="menu.bg_3"
          />
          <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="z.B: blue oder #ffffff"/>
          <span class="input-group-text ms-1">?</span>
        </div>
      </div>
      <div class="col-12 col-lg-3 col-sm-6 mb-2">
      </div>
    </div>

    <div class="row">

      <!-- Textfarbe -->
      <div class="col-12 col-lg-3 col-sm-6">
        <small><label for="text-colour" class="text-secondary">Textfarbe</label></small>
        <div class="input-group my-1 fwnw">
          <!-- <input
            type="text"
            class="form-control br"
            :class="{ 'is-invalid': errors && errors.font_color }"
            placeholder="Textfarbe*"
            v-model="menu.font_color"
            @input="clearError('font_color')"
          > -->
          <input
            id="picker_font_color"
            class="form-control brr"
            placeholder="Textfarbe*"
            :value="menu.font_color"
          />
          <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="z.B: blue oder #ffffff"/>
          <span class="input-group-text ms-1">?</span>
        </div>
      </div>

      <!-- Schriftgröße -->
      <div class="col-12 col-lg-3 col-sm-6">
        <small><label for="text-size" class="text-secondary">Schriftgröße</label></small>
        <div class="input-group my-1">
          <input
            type="text"
            class="form-control br"
            :class="{ 'is-invalid': errors && errors.fs }"
            placeholder="Schriftgröße*"
            v-model="menu.font_size"
            @input="clearError('font_size')"
          >
          <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="z. B: 12px" />
          <span class="input-group-text ms-1">?</span>
        </div>
      </div><ol>

      </ol>
    </div>

    <div class="row justify-content-center">
      <div class="col-12 col-sm-12">
        <small><span class="me-2">Freigeben an:</span></small>

        <div class="btn-group">
          <!-- avUsers -->
          <button
            v-if="fUsers.length"
            @click.prevent="st.dropdownActive = !st.dropdownActive"
            class="btn btn-outline-secondary dropdown-toggle"
          >
            Benutzer auswählen
          </button>
          <!-- avUsers -->
          <button
            v-if="!fUsers.length"
            class="btn btn-outline-secondary"
            disabled
          >
            Sie haben alle Benutzer ausgewählt
          </button>

          <div
            v-if="st.dropdownActive"
            class="dropdown-overlay"
            @click="st.dropdownActive = false"
          ></div>
          <ul
            v-if="st.dropdownActive"
            class="dropdown-menu show"
            style="top:40px;
            min-width:260px;"
          >
            <li>
              <div class="px-3">
                <input
                  type="text"
                  placeholder="Nach einem Benutzer suchen..."
                  v-model="st.query"
                  class="ddinput w100p"
                >
              </div>
            </li>
            <li>
              <hr class="dropdown-divider">
            </li>
            <!-- v-for="user in avUsers" -->
            <li
              v-if="user.active"
              v-for="user in fUsers"
              :key="user.id"
              @click="addUser(user)"
            >
              <a
                class="dropdown-item"
                href="#"
                @click.prevent=""
              >
                {{ user.name }}
              </a>
            </li>
            <li v-if="isEmptyFUsers">
              <div
                class="dropdown-item disabled"
              >
                Es wurde kein Benutzer gefunden
              </div>
            </li>
          </ul>
        </div>

      </div>
    </div>

    <!-- Ausgabe - Bereits freigegeben an -->

    <div class="row justify-content-center">
      <div class="col-12 col-sm-12 my-2">
        <small id="selected-users">
          <span class="me-1">Bereits freigegeben an:</span>
          <span
            v-for="(user,userInd) in users.selected"
            :key="user.id"
            class="me-2"
          >
            {{ user.name }}
            <a
              class="bi-x-square ms-1 button_remove"
              @click.prevent="removeUser(userInd)"
            ></a>
          </span>
        </small>
      </div>
    </div>

    <!-- Feld - Spezielle Parameter -->

    <div class="row py-2">
      <div class="col-12 col-sm-12 col-lg-6">
        <div class="input-group">
            <span class="input-group-text">CSS Styles</span>
            <textarea
              class="form-control"
              aria-label="special_param"
              name="special_param"
              v-model="menu.special_param"
              @input="st.edited = true"
              style="border-radius:0 .25rem .25rem 0;"
            >
            </textarea>

            <div style="display:inline-block;">
              <div class="input-group mb-3---" style="height:100%;">
                <input type="hidden">
                <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="CSS Styles, z.B: font-style: italic;"/>
                <span class="input-group-text ms-1" style="height: 100%;">?</span>
              </div>
            </div>
        </div>
      </div>
      <div class="col">
        <!-- Placeholder -->
      </div>
    </div>

  </form>
