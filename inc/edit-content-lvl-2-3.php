<!-- Felder, die bei einem 2 Level Punkt bearbeitet werden -->

<!-- @input="st.edited = true" erlaubt uns eine Meldung anzuzeigen,
die uns sagt, dass Änderungen vorgenommen worden sind -->

<div id="edit-2-3"></div>

<div
  v-if="st.item2edit !== null && st.item3edit === null"
  class="row mt-3"
>
  <!-- Name -->
  <div class="col-12 col-lg-3 col-sm-6 mb-2">
    <small><label for="item-level-2" class="text-secondary">Name des Punktes</label></small>
    <div class="input-group my-1">
      <input
        id="item-level-2"
        type="text"
        class="form-control br"
        name="name"
        v-model="menu.list[st.lang][st.item1edit].list[st.item2edit].name"
        @input="st.edited = true"
      >
      <span
        class="d-inline-block"
        tabindex="0"
        data-bs-toggle="popover"
        data-bs-trigger="hover focus"
        data-bs-content="z.B: Alle Fakultäten"
      >
      <span class="input-group-text ms-1">?</span>
    </div>
  </div>

  <!-- Beschreibung -->
  <div class="col-12 col-lg-3 col-sm-6 mb-2">
    <small><label for="desc" class="text-secondary">Beschreibung (im 2. LVL)</label></small>
    <div class="input-group my-1">
      <input
        type="text"
        class="form-control br"
        name="desc"
        v-model="menu.list[st.lang][st.item1edit].list[st.item2edit].desc"
        @input="st.edited = true"
      >
      <span
        class="d-inline-block"
        tabindex="0"
        data-bs-toggle="popover"
        data-bs-trigger="hover focus"
        data-bs-content="z.B: Hier werden alle Fakultäten angezeigt"
      >
      <span class="input-group-text ms-1">?</span>
    </div>
  </div>

  <!-- 3. Level Überschrift -->
  <div class="col-12 col-lg-3 col-sm-6 mb-2">
    <small><label for="subtitle" class="text-secondary">3. Level Überschrift</label></small>
    <div class="input-group my-1">
      <input
        type="text"
        class="form-control br"
        name="subtitle"
        v-model="menu.list[st.lang][st.item1edit].list[st.item2edit].subtitle"
        @input="st.edited = true"
      >
      <span
        class="d-inline-block"
        tabindex="0"
        data-bs-toggle="popover"
        data-bs-trigger="hover focus"
        data-bs-content="z.B: Die Fakultäten der Universität"
      >
      <span class="input-group-text ms-1">?</span>
    </div>
  </div>

  <!-- Textfarbe -->
  <div class="col-12 col-lg-3 col-sm-6 mb-2">
    <small><label for="color" class="text-secondary">Textfarbe</label></small>
    <div class="input-group my-1">
      <input
        type="text"
        class="form-control br"
        name="color"
        v-model="menu.list[st.lang][st.item1edit].list[st.item2edit].color"
        @input="st.edited = true"
      >
      <span
        class="d-inline-block"
        tabindex="0"
        data-bs-toggle="popover"
        data-bs-trigger="hover focus"
        data-bs-content="z.B: blue oder #000000"
      >
      <span class="input-group-text ms-1">?</span>
    </div>
  </div>

  <!-- Schriftstärke -->
  <div class="col-12 col-lg-3 col-sm-6 mb-2">
    <small><label for="font_weight" class="text-secondary">Schriftstärke</label></small>
    <div class="input-group my-1">
      <input
        type="text"
        class="form-control br"
        name="font_weight"
        v-model="menu.list[st.lang][st.item1edit].list[st.item2edit].font_weight"
        @input="st.edited = true"
      >
      <span
        class="d-inline-block"
        tabindex="0"
        data-bs-toggle="popover"
        data-bs-trigger="hover focus"
        data-bs-content="z.B: 500"
      >
      <span class="input-group-text ms-1">?</span>
    </div>
  </div>

  <!-- Hintergrundfarbe -->
  <div class="col-12 col-lg-3 col-sm-6 mb-2">
    <small><label for="bg" class="text-secondary">Hintergrundfarbe</label></small>
    <div class="input-group my-1">
      <input
        type="text"
        class="form-control br"
        name="bg"
        v-model="menu.list[st.lang][st.item1edit].list[st.item2edit].bg"
        @input="st.edited = true"
      >
      <span
        class="d-inline-block"
        tabindex="0"
        data-bs-toggle="popover"
        data-bs-trigger="hover focus"
        data-bs-content="z.B: blue oder #000000"
      >
      <span class="input-group-text ms-1">?</span>
    </div>
  </div>

  <!-- URL -->
  <div class="col-12 col-lg-3 col-sm-6 mb-2">
    <small><label for="url" class="text-secondary">URL</label></small>
    <div class="input-group my-1">
      <input
        type="text"
        class="form-control br"
        name="url"
        v-model="menu.list[st.lang][st.item1edit].list[st.item2edit].url"
        @input="st.edited = true"
      >
      <span
        class="d-inline-block"
        tabindex="0"
        data-bs-toggle="popover"
        data-bs-trigger="hover focus"
        data-bs-content="z.B: /uni/ oder http://google.com"
      >
      <span class="input-group-text ms-1">?</span>
    </div>
  </div>
</div>

<!-- Felder, die bei einem 3 Level Punkt bearbeitet werden -->
<div
  v-if="st.item2edit !== null && st.item3edit !== null"
  class="row mt-3"
>
  <!-- Name -->
  <div class="col-12 col-lg-3 col-sm-6 mb-2">
    <small><label for="name" class="text-secondary">Name des Punktes</label></small>
    <div class="input-group my-1">
      <input
        id="item-level-3"
        type="text"
        class="form-control br"
        name="name"
        v-model="menu.list[st.lang][st.item1edit].list[st.item2edit].list[st.item3edit].name"
        @input="st.edited = true"
      >
      <span
        class="d-inline-block"
        tabindex="0"
        data-bs-toggle="popover"
        data-bs-trigger="hover focus"
        data-bs-content="z.B: Wirtschaftsinformatik"
      >
      <span class="input-group-text ms-1">?</span>
    </div>
  </div>

  <!-- Textfarbe -->
  <div class="col-12 col-lg-3 col-sm-6 mb-2">
    <small><label for="color" class="text-secondary">Textfarbe</label></small>
    <div class="input-group my-1">
      <input
        type="text"
        class="form-control br"
        name="color"
        v-model="menu.list[st.lang][st.item1edit].list[st.item2edit].list[st.item3edit].color"
        @input="st.edited = true"
      >
      <span
        class="d-inline-block"
        tabindex="0"
        data-bs-toggle="popover"
        data-bs-trigger="hover focus"
        data-bs-content="z.B: blue oder #000000"
      >
      <span class="input-group-text ms-1">?</span>
    </div>
  </div>

  <!-- Schriftstärke -->
  <div class="col-12 col-lg-3 col-sm-6 mb-2">
    <small><label for="font_weight" class="text-secondary">Schriftstärke</label></small>
    <div class="input-group my-1">
      <input
        type="text"
        class="form-control br"
        name="font_weight"
        v-model="menu.list[st.lang][st.item1edit].list[st.item2edit].list[st.item3edit].font_weight"
        @input="st.edited = true"
      >
      <span
        class="d-inline-block"
        tabindex="0"
        data-bs-toggle="popover"
        data-bs-trigger="hover focus"
        data-bs-content="z.B: 500"
      >
      <span class="input-group-text ms-1">?</span>
    </div>
  </div>

  <!-- Hintergrundfarbe -->
  <div class="col-12 col-lg-3 col-sm-6 mb-2">
    <small><label for="bg" class="text-secondary">Hintergrundfarbe</label></small>
    <div class="input-group my-1">
      <input
        type="text"
        class="form-control br"
        name="bg"
        v-model="menu.list[st.lang][st.item1edit].list[st.item2edit].list[st.item3edit].bg"
        @input="st.edited = true"
      >
      <span
        class="d-inline-block"
        tabindex="0"
        data-bs-toggle="popover"
        data-bs-trigger="hover focus"
        data-bs-content="z.B: blue oder #000000"
      >
      <span class="input-group-text ms-1">?</span>
    </div>
  </div>

  <!-- URL -->
  <div class="col-12 col-lg-3 col-sm-6 mb-2">
    <small><label for="url" class="text-secondary">URL</label></small>
    <div class="input-group my-1">
      <input
        type="text"
        class="form-control br"
        name="url"
        v-model="menu.list[st.lang][st.item1edit].list[st.item2edit].list[st.item3edit].url"
        @input="st.edited = true"
      >
      <span
        class="d-inline-block"
        tabindex="0"
        data-bs-toggle="popover"
        data-bs-trigger="hover focus"
        data-bs-content="z.B: /uni/ oder http://google.com"
      >
      <span class="input-group-text ms-1">?</span>
    </div>
  </div>

</div>
