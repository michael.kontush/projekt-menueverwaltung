  <!-- SCRIPTS -->

  <!-- jQuery -->
  <script src="<?php echo $baseUrl; ?>/js/jquery-3.5.1.min.js"></script>

  <!-- Colorpicker -->
  <script src="<?php echo $baseUrl; ?>/js/spectrum.min.js"></script>

  <!-- HTML Code highlighten -->
  <script src="<?php echo $baseUrl; ?>/js/highlight.min.js"></script>

  <!-- Axios -->
  <script src="<?php echo $baseUrl; ?>/js/axios.min.js"></script>

  <!-- Vue.js v2.6.14 -->
  <script src="<?php echo $baseUrl; ?>/js/vue.js"></script>

  <!-- Bootstrap Add-On -->
  <script src="<?php echo $baseUrl; ?>/js/popper.min.js"></script>

  <!-- Bootstrap -->
  <script src="<?php echo $baseUrl; ?>/js/bootstrap.bundle.js"></script>

  <!-- Unser hausgemachter Vue.js code -->
  <script src="<?php echo $baseUrl; ?>/js/app.js"></script>
