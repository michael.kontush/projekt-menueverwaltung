  <!-- Verschiedene generierte Favicons -->

  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $baseUrl; ?>/images/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $baseUrl; ?>/images/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $baseUrl; ?>/images/favicon-16x16.png">
  <link rel="manifest" href="<?php echo $baseUrl; ?>/images/site.webmanifest">
  <link rel="mask-icon" href="<?php echo $baseUrl; ?>/images/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="<?php echo $baseUrl; ?>/images/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="<?php echo $baseUrl; ?>/images/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
