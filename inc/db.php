<?php

// URL der Website

$baseUrl = "http://$_SERVER[HTTP_HOST]";

// Lokale Daten

$user = "root";
$pass = "";
$host = "localhost";
$dbdb = "msis22db";

// Uniserver Daten

// $user = "msis22";
// $pass = "XfQG1oeh4PH7Z8";
// $host = "localhost";
// $dbdb = "msis22db";

// Verbindung mit DB aufbauen

$conn = new mysqli($host, $user, $pass, $dbdb);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Vorsichtshalber UTF-8 benutzen bei der Verbindung

$conn->set_charset('utf8');

?>
