<!-- Das Preview des Menüs. Dieses Include wird im sowohl auf edit-menu.php als auch auf preview.php benutzt -->

<!-- Beginn 1 Level Punkte -->

<div class="row">
  <div class="col mx-1">
    <nav
      class="navbar navbar-expand-lg navbar-dark"
      :style="{ backgroundColor: cssBg1Color }"
    >
      <div class="container-fluid">
        <button
          class="navbar-toggler"
          type="button"
          @click="menuToggle"
        >
          <span class="navbar-toggler-icon"></span>
        </button>
        <div
          class="navbar-collapse"
          :class="st.menuActive ? 'show' : 'collapse'"
        >
          <!-- id="navbarSupportedContent" -->
          <ul class="navbar-nav me-auto mb-2 mb-lg-0 flex-wrap top-space">
            <li
              v-for="(item_1,itemInd) in menu.list[st.lang]"
              class="nav-item level_1 nowrap"
              :class="{ active: st.item1selected === itemInd }"
              :style="{
                backgroundColor: item_1.bg,
                fontWeight: item_1.font_weight
              }"
            >
              <a
                class="nav-link"
                aria-current="page"
                :href="item_1.url"
                :style="{
                  color: item_1.color ? item_1.color : 'white',
                  fontSize: menu.font_size
                }"
                style="
                  display:flex;
                  alignt-items:center;
                "
                @mouseover="selectFirstLevel(itemInd)"
                @click.prevent="selectFirstLevel(itemInd,item_1)"
              >
                {{ item_1.name }}
                <div v-if="item_1.list.length">
                  <svg
                    viewBox="0 0 16 16"
                    height="12"
                    width="12"
                    fill="currentColor"
                    class="ico-exp-mob"
                    :class="{ active: st.item1selected === itemInd }"
                  >
                    <use href="#expand"/>
                  </svg>

                </div>

                <span class="ico-ext-link-mob">
                  <svg
                    v-if="showLinkIco(item_1.url, true)"
                    viewBox="0 0 16 16"
                    height="12"
                    width="12"
                    fill="currentColor"
                  >
                    <use href="#external-link"/>
                  </svg>
                  <template v-if="item_1.list.length">
                    <svg
                      v-if="showLinkIco(item_1.url, false)"
                      viewBox="0 0 16 16"
                      height="15"
                      width="15"
                      fill="currentColor"
                    >
                      <use href="#internal-link"/>
                    </svg>
                  </template>
                </span>
              </a>
              <ul
                v-if="itemInd === st.item1selected"
                class="menu_mob_2"
              >
              <!-- :style="{ borderColor: cssBg1Color }"
              style="
              border-style: solid;
              border-width: 1px 0 0 0;
              /* top right bottom left */
              " -->








              <!-- v-if="row.list.length" -->
                <div
                  class="menu_mob_2_descr"
                  :style="{
                    fontSize: menu.font_size
                  }"
                >
                  <span style="white-space:normal;">
                    {{ selectedMenuItem.subtitle }}
                  </span>
                </div>






                <li v-for="(row,rowInd) in selectedMenuItem.list">
                  <a
                    class="level_2_mob"
                    :href="row.url"
                    @click.prevent.stop="selectSecondLevel(rowInd, false, row)"
                    style="
                      display:flex;
                      white-space:normal;
                      line-height:20px;
                      padding-top:5px;
                      padding-bottom:5px;
                    "
                    :style="{
                      fontSize: menu.font_size
                    }"
                  >
                    <span>
                      {{ row.name }}
                    </span>
                    <span v-if="row.list.length">
                      <svg
                        viewBox="0 0 16 16"
                        height="12"
                        width="12"
                        fill="currentColor"
                        class="ico-exp-mob-2"
                        :class="{ active: st.item2selected === rowInd }"
                      >
                        <use href="#expand"/>
                      </svg>
                    </span>
                    <span
                      @click.stop="goto(row.url)"
                      style="
                        margin-left:auto;
                        padding-left:15px;
                      "
                    >
                      <svg
                        v-if="showLinkIco(row.url, true)"
                        viewBox="0 0 16 16"
                        height="12"
                        width="12"
                        fill="currentColor"
                        class="ico-ext-link-mob-2"
                      >
                        <use href="#external-link"/>
                      </svg>
                      <svg
                        v-if="showLinkIco(row.url, false) && row.list.length"
                        viewBox="0 0 16 16"
                        height="15"
                        width="15"
                        fill="currentColor"
                        class="ico-ext-link-mob-2"
                      >
                        <use href="#internal-link"/>
                      </svg>
                    </span>
                  </a>

                  <div
                    class="mob-bg"
                    v-if="rowInd === st.item2selected"
                    data-type="mobile 2nd level"
                  >
                    <!-- style="
                    border-style: solid;
                    border-width: 1px 0 0 0;
                    " -->
                    <!-- :style="{
                      borderColor: row.list.length ? menu.bg_1 : 'transparent'
                    }" -->
                    <div
                      v-if="row.list.length"
                      class="menu_mob_3_descr"
                      :style="{
                        fontSize: menu.font_size
                      }"
                    >
                      <span style="white-space:normal;">
                        {{ row.subtitle }}
                      </span>
                    </div>
                    <ul
                      class="menu_mob_3"
                    >
                      <li v-for="item in row.list">
                        <a
                          class="level_3_mob"
                          :href="item.url"
                          style="
                            display:flex;
                            line-height:20px;
                            padding-top:5px;
                            padding-bottom:5px;
                          "
                          :style="{
                            fontSize: menu.font_size
                          }"
                        >
                          <span style="white-space:normal;">
                            {{ item.name }}
                          </span>
                          <span
                            style="
                              margin-left:auto;
                              padding-left:15px;
                            "
                          >
                            <svg
                              v-if="showLinkIco(item.url, true)"
                              viewBox="0 0 16 16"
                              height="12"
                              width="12"
                              fill="currentColor"
                              class="ico-ext-link-mob-3"
                            >
                              <use href="#external-link"/>
                            </svg>
                          </span>
                        </a>
                      </li>
                    </ul>
                  </div>

                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </div>
</div>

<!-- Ende 1 Level Punkte -->

<!-- selectedMenuItem -->
<div
  v-if="secondLevel && secondLevel.length"
  class="row menu_border mx-1 justify-content-between submenu"
  :style="{ backgroundColor: menu.bg_2 }"
>

  <!-- Beginn 2 Level Punkte -->
  <div class="col-12 col-lg-8 col-md-12 col-sm-12 my-2 mb-4">
    <div class="level_2_big ms-3">
       <a
        :href="selectedMenuItem.url ? selectedMenuItem.url : '#'"
        :class="{
          'unu cursor-default': !selectedMenuItem.url,
        }"
        :style="{
          color: menu.font_color,
          fontSize: menu.font_size
        }"
      >{{ selectedMenuItem.subtitle }}</a>
      <a
       :href="selectedMenuItem.url"
       class="subtitle_link unu"
       :style="{
         color: menu.font_color,
         fontSize: menu.font_size
       }"
     >
       <svg
         v-if="showLinkIco(selectedMenuItem.url, true)"
         viewBox="0 0 16 16"
         height="12"
         width="12"
         fill="currentColor"
         class="ico-ext-link ms-1"
       >
         <use href="#external-link"/>
       </svg>
       <svg
         v-if="showLinkIco(selectedMenuItem.url, false)"
         viewBox="0 0 16 16"
         height="15"
         width="15"
         fill="currentColor"
         class="ico-int-link"
         @click.stop="goto(selectedMenuItem.url)"
       >
         <use href="#internal-link"/>
       </svg>
     </a>
    </div>

    <div
      v-for="(row,rowInd) in secondLevel"
      class="row mb-1"
    >

      <div
        v-for="(item_2,itemInd) in row"
        class="col ms-3 mt-3"
      >
        <div
          v-if="item_2"
          class="vl"
          :class="{ active: st.item2selected === rowInd * 2 + itemInd }"
        >
          <a
            :href="item_2.url"
            class="level_2_link"
            :class="{ 'cursor-default': !item_2.url }"
            :style="{ fontSize: menu.font_size }"
            @click.prevent="selectSecondLevel(rowInd * 2 + itemInd)"
          >
            <!-- @mouseover="selectSecondLevel(rowInd * 2 + itemInd, true)" -->
            <!-- @mouseleave="unselectSecondLevel(rowInd * 2 + itemInd)" -->
            <div
              class="level_2_title ms-2"
              :style="{
                color: item_2.color || menu.font_color,
                backgroundColor: item_2.bg,
                fontWeight: item_2.font_weight
              }"
            >
              {{ item_2.name }}

              <svg
                v-if="showLinkIco(item_2.url, true)"
                viewBox="0 0 16 16"
                height="12"
                width="12"
                fill="currentColor"
                class="ico-ext-link ms-1"
                @click.stop="goto(item_2.url)"
              >
                <use href="#external-link"/>
              </svg>

              <svg
                v-if="showLinkIco(item_2.url, false)"
                viewBox="0 0 16 16"
                height="15"
                width="15"
                fill="currentColor"
                class="ico-int-link"
                @click.stop="goto(item_2.url)"
              >
                <use href="#internal-link"/>
              </svg>

            </div>
            <div
              class="level_2_desc ms-2"
              :style="{ color: item_2.color || menu.font_color }"
            >
              {{ item_2.desc }}
            </div>
          </a>
        </div>
      </div>
    </div>

  </div>
  <!-- Ende 2 Level Punkte -->

  <!-- Beginn 3 Level Punkte -->
  <div
    v-if="selectedSubmenuItem"
    class="col-12 col-lg-4 col-md-12 col-sm-12"
    :style="{
      backgroundColor: menu.bg_3,
      fontSize: menu.font_size
    }"
  >
    <div class="mt-2">
      <div
        class="level_3_big ms-1"
      >
        <a
          :href="selectedSubmenuItem.url ? selectedSubmenuItem.url : '#'"
          :class="{
            'unu cursor-default': !selectedSubmenuItem.url,
          }"
          :style="{ color: menu.font_color }"
          style="font-size:1em;"
        >{{ selectedSubmenuItem.subtitle }}</a>
        <a
          :href="selectedSubmenuItem.url"
          :style="{ color: menu.font_color }"
          class="unu"
        >
          <svg
            v-if="showLinkIco(selectedSubmenuItem.url, true)"
            viewBox="0 0 16 16"
            height="12"
            width="12"
            fill="currentColor"
            class="ico-ext-link ms-1"
            @click.stop="goto(selectedSubmenuItem.url)"
          >
            <use href="#external-link"/>
          </svg>

          <svg
            v-if="showLinkIco(selectedSubmenuItem.url, false)"
            viewBox="0 0 16 16"
            height="15"
            width="15"
            fill="currentColor"
            class="ico-int-link"
            @click.stop="goto(selectedSubmenuItem.url)"
          >
            <use href="#internal-link"/>
          </svg>
        </a>
      </div>
    </div>
    <div
      v-for="item_3 in selectedSubmenuItem.list"
      class="ms-3 mt-2"
    >
      <a
        class="level_3_title"
        :href="item_3.url"
        :style="{
          color: item_3.color || menu.font_color,
          backgroundColor: item_3.bg,
          fontWeight: item_3.font_weight
        }"
        style="font-size:.92em;"
      >
        {{ item_3.name }}
        <svg
          v-if="showLinkIco(item_3.url, true)"
          viewBox="0 0 16 16"
          height="12"
          width="12"
          fill="currentColor"
          class="ico-ext-link ms-1"
          @click.stop="goto(item_3.url)"
        >
          <use href="#external-link"/>
        </svg>
      </a>
    </div>
    <br>
    <!-- <div class="ms-3 mt-2"></div> -->
  </div>
  <!-- Ende 3 Level Punkte -->

</div>


<?php

if($full AND $show_btns) {
  echo '
    <div class="row">
      <div class="col my-3 ms-1">
        <button
          v-if="!st.showMenuModel"
          class="btn btn-outline-primary mb-2"
          @click="showModel(true)"
        >
          Modell anzeigen
        </button>
        <button
          v-if="st.showMenuModel"
          class="btn btn-outline-primary mb-2"
          @click="showModel(false)"
        >
          Modell verstecken
        </button>
      </div>

      <div class="col my-3 ms-1">
        <button
          v-if="!st.showHTMLCode"
          class="btn btn-outline-primary mb-2"
          @click="toggleHTMLCode(true)"
        >
          HTML Code anzeigen
        </button>
        <button
          v-if="st.showHTMLCode"
          class="btn btn-outline-primary mb-2"
          @click="toggleHTMLCode(false)"
        >
          HTML Code verstecken
        </button>
      </div>
    </div>

    <div
      v-if="st.showHTMLCode"
      style="position:relative;"
    >
      <button
        v-if="!st.copied"
        class="btn btn-outline-primary"
        style="
          position:absolute;
          top:15px;left:15px;
        "
        @click.prevent="copyCode"
      >
        Code kopieren
      </button>
      <button
        v-if="st.copied"
        class="btn btn-outline-primary"
        disabled
        style="
          position:absolute;
          top:15px;left:15px;
        "
      >
        Code kopiert
      </button>

      <button
        @click="downloadHTML"
        class="btn btn-outline-primary"
        style="
          position:absolute;
          top:15px;left:200px;
        "
      >
        Code runterladen
      </button>

      <pre><code style="padding-top:40px;" class="language-html">{{ st.HTML }}</code></pre>
    </div>


    <pre v-if="st.showMenuModel"><code class="language-json">{{ jsonMenuSettings }}</code></pre>
    <pre v-if="st.showMenuModel"><code class="language-json">{{ jsonMenu }}</code></pre>
  ';
}

?>

<div class="row">
  <div class="col mb-4">

  </div>
</div>
