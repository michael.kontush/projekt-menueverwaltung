<?php

session_start();

if(empty($_SESSION["id"])){
  header("location: login.php");
}

include('inc/db.php');

// Fehler in einem Array speichern
$errors = array();

// Falls der Benutzer eingeloggt ist
if (!empty($_SESSION["id"])){

  $result = mysqli_query($conn,"SELECT username, id FROM admins WHERE id = '".$_SESSION["id"]."'");
  $row = mysqli_fetch_array($result);
  $username = $row["username"];

}

// Falls wir eine Post-Anfrage bekommen
if(count($_POST)>0) {

$password_1 = mysqli_real_escape_string($conn, $_POST['password_1']);
$password_2 = mysqli_real_escape_string($conn, $_POST['password_2']);

// Passwort lönge ermitteln
$passwordlength= strlen($password_1);

// Passwort durch MD5 encoden
$final_password = md5($password_1);

// Fehler prüfen
if (empty($password_1)) { array_push($errors, "Sie haben das Passwort nicht eingegeben.");

  } elseif ($password_1 != $password_2) {

    array_push($errors, "Die Passwörter stimmen nicht überein.");

  } elseif ($passwordlength < 6) {

    array_push($errors, "Die minimale Länge des Passworts muss 6 Zeichen sein.");

  } elseif ($passwordlength > 20){

    array_push($errors, "Die maximale Länge des Passworts muss 20 Zeichen sein.");

  } else {

    // Wenn alles okay, dann neues Passwort in der DB speichern
    mysqli_query($conn,"UPDATE admins SET password = '$final_password' WHERE id = '".$row['id']."'");
    $okay = 'Ihr Passwort wurde geändert.';
  }

}

?>

<!DOCTYPE html>
<html lang="de">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href="/css/bootstrap.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="/css/bootstrap-icons.css" rel="stylesheet">
  <title>Eigenes Passwort ändern</title>
  <?php include 'inc/favicons.php'; ?>
</head>
<body>

  <div id="app">

    <?php $title="Eigenes Passwort ändern"; ?>

    <?php include 'inc/header.php'; ?>

    <div class="content">
      <div class="container">

        <!-- Felder - Benutzername, Passwort, Passwort wiederholen -->

        <form method="post" action="edit-password.php">

          <div class="row justify-content-center mt-3">
            <div class="col-12 col-lg-3 col-sm-6 mb-2">
              <div class="input-group mb-3">
                <input type="text" class="form-control br" aria-label="name" value="<?php if (!empty($_SESSION["id"])){echo $username;}?>" readonly>
              </div>
            </div>
            <div class="col-12 col-lg-3 col-sm-6 mb-2">
              <div class="input-group mb-3">
                <input type="password" class="form-control br" aria-label="tag" name="password_1" placeholder="Neues Passwort">
                <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="von 6 bis 20 Zeichen"/>
                <span class="input-group-text ms-1">?</span>
              </div>
            </div>
            <div class="col-12 col-lg-3 col-sm-6 mb-2">
              <div class="input-group mb-3">
                <input type="password" class="form-control br" aria-label="tag" name="password_2" placeholder="Passwort wiederholen">
                <span class="d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="von 6 bis 20 Zeichen"/>
                <span class="input-group-text ms-1">?</span>
              </div>
            </div>
            <div class="col-12 col-lg-3 col-sm-6 mb-2">
              <!-- Placeholder -->
            </div>
          </div>

          <!-- Button - Passwort ändern -->
          <div class="row">
            <div class="col mb-3">
                <button type="submit" class="btn btn-outline-primary">Passwort ändern</button>
            </div>
          </div>

          <!-- Falls alles Okay: Text okay anzeigen, falls nicht, Fehler anzeigen -->
          <div class="row">
            <div class="col mb-3">
              <span class="text-success">
                <?php
                if((count($_POST)>0) && (count($errors)==0)) {echo $okay;}
                ?>
              </span>
              <span class="text-danger">
                <?php
                  if (count($errors) > 0){
                    foreach ($errors as $error) echo $error;
                  }
                  ?>
              </span>
            </div>
          </div>

        </form>

      </div>
    </div>

  <?php include 'inc/footer.php';?>

  </div> <!-- #app -->

<?php include 'inc/scripts.php';?>

</body>
</html>
