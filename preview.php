<?php

session_start();

include('inc/db.php');

// Slug des ausgewählten Menüs aus der URL bekommen
$chosen_menu = $_GET['slug'];

// Abfrage aller Daten des ausgewählten Menüs
$sql = "SELECT id, model, name, slug FROM menus WHERE slug = '$chosen_menu'";
$result = mysqli_query($conn,$sql);
$row = mysqli_fetch_array($result);

// Modell vorbereiten, " mit ' ersetzen
$final_model = str_replace("\"","'", $row['model']);

// Als default ist die Deutsche Sprache ausgewählt
// Falls wir "en" im GET bekommen, dann die Englische Version anzeigen
$lang = 'de';
if(isset($_GET['lang'])){
  if($_GET['lang'] === 'en'){
    $lang = 'en';
  }
}

// Variable, die die Anzeige der Buttons "HTML Code anzeigen" und "Modell anzeigen" regelt
$show_btns = true;

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href="<?php echo $baseUrl; ?>/css/default.min.css" rel="stylesheet">
  <link href="<?php echo $baseUrl; ?>/css/bootstrap.css" rel="stylesheet">
  <link href="<?php echo $baseUrl; ?>/css/bootstrap-icons.css" rel="stylesheet">
  <link href="<?php echo $baseUrl; ?>/css/style.css" rel="stylesheet">
  <title>Menü-Vorschau - <?php echo $row['slug'];?></title>
  <style>[v-cloak]{display:none!important;}</style>
  <?php include 'inc/favicons.php'; ?>
</head>
<body>


  <div id="app">

    <?php $title="Test des Menüs \"$chosen_menu\""; ?>

    <?php include 'inc/header.php'; ?>

    <div class="content">
      <div class="container">

        <?php

          if (!empty($_SESSION["id"])){ // Falls eine Session existiert

            // Prüfen, ob der Admin die nötigen Admin Rechte hat
            $query_check = "SELECT id, admin_id, menu_id FROM admins_menus WHERE menu_id = '".$row["id"]."' AND admin_id = '".$_SESSION["id"]."'";
            $result_check = mysqli_query($conn,$query_check);
            $row_check = mysqli_fetch_array($result_check);

            if (!empty($row_check['admin_id'])){

              $found = $row_check['admin_id'];

            }

            // Falls der Admin die nötigen Rechte nicht hat, Text anzeigen
            if($_SESSION["admin_level"] == '1' && empty($found)){

              echo '

              <div class="row py-2">
                <div class="col">
                  <div class="p-3 text-center">Sie haben keine Rechte diese Seite anzusehen.</div>
                </div>
              </div>';

              echo '</div>
              </div>'; // #content und #container

              include 'inc/footer.php';

              echo '</div>'; // #app

              include 'inc/scripts.php';

              die;

              }

          }else{ // Falls der Benutzer nicht eingeloggt ist

            echo '

            <div class="row py-2">
              <div class="col">
                <div class="p-3 text-center">Diese Seite steht ihnen nicht zur Verfügung. Bitte <a href="login.php" class="text-dark">loggen sie sich ein</a>.</div>
              </div>
            </div>';

            echo '</div>
            </div>'; // #content und #container

            include 'inc/footer.php';

            echo '</div>'; // #app

            include 'inc/scripts.php';

            die;

          }

         ?>

        <!-- SVGs im Menü -->
        <svg xmlns="http://www.w3.org/2000/svg" style="display:none;">

          <!-- External Link -->
          <symbol id="external-link" viewBox="0 0 16 16">
            <path fill-rule="evenodd" d="M8.636 3.5a.5.5 0 0 0-.5-.5H1.5A1.5 1.5 0 0 0 0 4.5v10A1.5 1.5 0 0 0 1.5 16h10a1.5 1.5 0 0 0 1.5-1.5V7.864a.5.5 0 0 0-1 0V14.5a.5.5 0 0 1-.5.5h-10a.5.5 0 0 1-.5-.5v-10a.5.5 0 0 1 .5-.5h6.636a.5.5 0 0 0 .5-.5z"/>
            <path fill-rule="evenodd" d="M16 .5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h3.793L6.146 9.146a.5.5 0 1 0 .708.708L15 1.707V5.5a.5.5 0 0 0 1 0v-5z"/>
          </symbol>

          <!-- Level ausklappen/verstecken -->
          <symbol id="expand" viewBox="0 0 16 16">
            <path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
          </symbol>

          <!-- Internal Link -->
          <symbol id="internal-link" viewBox="0 0 16 16">
            <path d="M4.715 6.542 3.343 7.914a3 3 0 1 0 4.243 4.243l1.828-1.829A3 3 0 0 0 8.586 5.5L8 6.086a1.002 1.002 0 0 0-.154.199 2 2 0 0 1 .861 3.337L6.88 11.45a2 2 0 1 1-2.83-2.83l.793-.792a4.018 4.018 0 0 1-.128-1.287z"/>
            <path d="M6.586 4.672A3 3 0 0 0 7.414 9.5l.775-.776a2 2 0 0 1-.896-3.346L9.12 3.55a2 2 0 1 1 2.83 2.83l-.793.792c.112.42.155.855.128 1.287l1.372-1.372a3 3 0 1 0-4.243-4.243L6.586 4.672z"/>
          </symbol>
        </svg>

        <!-- Falls wir uns nicht in dem "HTML Code anzeigen" befinden -->
        <?php if($full){

        echo '

        <div class="col mt-2 mb-3 ms-1">
          <a
            href="edit-menu.php?slug='.$row['slug'].'&content=true"
            class="btn btn-outline-primary mt-2"
          >
            Zurück zum Bearbeiten
          </a>
        </div>

        ';
        } ?>

        <!--

        p-state:
          menuActive: Menü öffnen / verstecken, in der mobilen Version
          lang: Sprache
          item1selected: Ausgewählter 1. Level Punkt
          item2selected: Ausgewählter 2. Level Punkt
          timeoutId: wurde benutzt, um bei einem Mouseover über die rechte Seite des 2. Level, nicht direkt den 3. Level anzuzeigen.
                     wir haben uns mit den Betreuern entschieden, den mouseover mit einem click zu ersetzten
          delay: zusammen mit timeoutID, wird nicht mehr benutzt
          copied: HTML Code kopiert
          showMenuModel: Menü Modell anzeigen
          showHTMLCode: HTML Code anzeigen
          menus: Sprachen
        p-menu: Modell

        -->

        <menu-block
          inline-template
          :p-state="{
            menuActive: false,
            lang: '<?php echo $lang; ?>',
            item1selected: 0,
            item2selected: null,
            timeoutId: null,
            delay: 500,
            copied: false,
            showMenuModel: false,
            showHTMLCode: false,
            menus: [
              {
                key: 'de',
                name: 'Deutsches Menu',
              },
              {
                key: 'en',
                name: 'Englisches Menu',
              },
            ],
          }"
          :p-menu="<?php echo $final_model; ?>"
        >

          <div
            v-cloak
            id="test"
            :style="menu.special_param"
          >

            <!-- Schaltung zwischen den Sprachen Deutsch/Englisch -->
            <div class="row">

              <?php if ($full) {

                echo '

                <div class="col mb-3 ms-1">
                  <div class="btn-group" role="group" aria-label="Basic outlined example">
                    <button
                      v-for="item in st.menus"
                      type="button"
                      class="btn"
                      :class="st.lang === item.key ? \'btn-dark\' : \'btn-outline-dark\'"
                      @click="changeLang(item.key)"
                    >
                      {{ item.name }}
                    </button>
                  </div>
                </div>

                ';

              }

            ?>

            </div>

            <!-- Das eigentliche Menü -->

            <?php include('inc/preview-menu-block.php'); ?>

          </div>

        </menu-block>

      </div>
    </div>

    <?php include 'inc/footer.php';?>

  </div>

  <?php include 'inc/scripts.php';?>

</body>
</html>
